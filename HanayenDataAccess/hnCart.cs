﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HanayenEntity;
using System.Data;
using HanayenEntity.SupportingModel;

namespace HanayenDataAccess
{
   public class hnCart
    {
      public string AddCartItems(CartItems items)
       {
           string result = "";
           try
           {
               if (items != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_COOKIEID,items.CookieId),
                   new SqlParameter(HanayenConstants.COL_QUATITY,items.Quantity),
                   new SqlParameter(HanayenConstants.COL_PRODUCTID,items.ProductId),
                   new SqlParameter(HanayenConstants.COL_CARTDATE,items.CartDate),
                   new SqlParameter(HanayenConstants.COL_ISGUEST,items.IsGuest),
                   new SqlParameter(HanayenConstants.COL_CARTSTATUS,items.CartStatus),
                   new SqlParameter(HanayenConstants.COL_CUSTOMERID,items.CustomerId),
                   new SqlParameter(HanayenConstants.PARAM_OPERATION,"I"),            
               };
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_CART, CommandType.StoredProcedure, param);
                   result = "success";
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;
       }
      public List<CartItems> GetCookie(string cookieId)
       {
           List<CartItems> cartList = new List<CartItems>();
           IDataReader rd;
           SqlConnection con = new SqlConnection();
           SqlParameter[] param = new SqlParameter[]
           {
                       new SqlParameter(HanayenConstants.PARAM_CookieId,cookieId),
                      new SqlParameter(HanayenConstants.PARAM_OPERATION,"S")
           };
           rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_CART, CommandType.StoredProcedure, param, con);
           while (rd.Read())
           {
                CartItems Cartitems = new CartItems();
                Cartitems.CookieId= rd[HanayenConstants.COL_COOKIEID].ToString();
                Cartitems.CartStatus= int.Parse(rd[HanayenConstants.COL_CARTSTATUS].ToString());
                Cartitems.IsGuest= rd[HanayenConstants.COL_ISGUEST].ToString();
                Cartitems.ProductId=long.Parse( rd[HanayenConstants.COL_PRODUCTID].ToString());
                Cartitems.Quantity= int.Parse(rd[HanayenConstants.COL_QUANTITY].ToString());             
                cartList.Add(Cartitems);
           }
           rd.Close();
           con.Close();
           return cartList;
       }
      public string DeleteCartItems(CartItems items)
       {
           string result = "";
           try
           {
               if (items != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_CARTID,items.CartId),
                   new SqlParameter(HanayenConstants.PARAM_OPERATION,"D"),            
               };
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_CART, CommandType.StoredProcedure, param);
                   result = "success";
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;
       }
      public string EditCartItems(CartItems items)
       {
           string result = "";
           try
           {
               if (items != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_CARTID,items.CartId),
                   new SqlParameter(HanayenConstants.COL_QUANTITY,items.Quantity),
                   new SqlParameter(HanayenConstants.PARAM_OPERATION,"U"),            
               };
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_CART, CommandType.StoredProcedure, param);
                   result = "success";
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;
       }
      public List<CartItemList> GetCartItems(string cookieId)
      {
          List<CartItemList> cartIt = new List<CartItemList>();
          IDataReader rd;
          SqlConnection con = new SqlConnection();
          SqlParameter[] param = new SqlParameter[]
           {
                       new SqlParameter(HanayenConstants.PARAM_CookieId,cookieId),
                      new SqlParameter(HanayenConstants.PARAM_OPERATION,"V")
           };
          rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_CART, CommandType.StoredProcedure, param, con);
          while (rd.Read())
          {
              CartItemList Cartitems = new CartItemList(); 
              //Cartitems.CookieId = rd[HanayenConstants.COL_COOKIEID].ToString();
              Cartitems.CartId = int.Parse(rd[HanayenConstants.COL_CARTID].ToString());
              Cartitems.Description = rd[HanayenConstants.COL_DESCRIPTION].ToString();
              Cartitems.Color = rd[HanayenConstants.COL_COLOR].ToString();
              //Cartitems.ProductId = long.Parse(rd[HanayenConstants.COL_PRODUCTID].ToString());
              Cartitems.Quantity = int.Parse(rd[HanayenConstants.COL_QUANTITY].ToString());
              Cartitems.Discount= int.Parse(rd[HanayenConstants.COL_DISCOUNT].ToString());
              Cartitems.Image = rd[HanayenConstants.COL_IMAGE].ToString();
              Cartitems.Price = int.Parse(rd[HanayenConstants.COL_PRICE].ToString());
              Cartitems.ProductCode = rd[HanayenConstants.COL_PRODUCTCODE].ToString();
              Cartitems.ProductName = rd[HanayenConstants.COL_PRODUCTNAME].ToString();
              cartIt.Add(Cartitems);
          }
          rd.Close();
          con.Close();
          return cartIt;
      }

      public long CheckProductId(long ProductId,string CookieId )
      {
          //long ProId=0;
          int ProId= 0;
          IDataReader rd;
          SqlConnection con = new SqlConnection();
          SqlParameter[] param = new SqlParameter[]
           {
                      new SqlParameter(HanayenConstants.PARAM_PRODUCTID,ProductId),
                      new SqlParameter(HanayenConstants.PARAM_CookieId,CookieId),
                      new SqlParameter(HanayenConstants.PARAM_OPERATION,"C")
           };
          rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_CART, CommandType.StoredProcedure, param, con);
          if (rd.Read())
          {
             // CartItemList Cartitems = new CartItemList();

              ProId = int.Parse(rd[HanayenConstants.COL_PRODUCTID].ToString());
             
          }
          rd.Close();
          con.Close();
          return ProId;
      }
      public CartItemList GetProductDetailsByCartId(string cartId)
      {
          CartItemList Cartitems = new CartItemList();
          IDataReader rd;
          SqlConnection con = new SqlConnection();
          SqlParameter[] param = new SqlParameter[]
           {
                       new SqlParameter(HanayenConstants.PARAM_CARTID,cartId),
                      new SqlParameter(HanayenConstants.PARAM_OPERATION,"CP")
           };
          rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_CART, CommandType.StoredProcedure, param, con);
          while (rd.Read())
          {
            
              //Cartitems.CookieId = rd[HanayenConstants.COL_COOKIEID].ToString();
              Cartitems.CartId = int.Parse(rd[HanayenConstants.COL_CARTID].ToString());
              Cartitems.Description = rd[HanayenConstants.COL_DESCRIPTION].ToString();
              Cartitems.Color = rd[HanayenConstants.COL_COLOR].ToString();
              //Cartitems.ProductId = long.Parse(rd[HanayenConstants.COL_PRODUCTID].ToString());
              Cartitems.Quantity = int.Parse(rd[HanayenConstants.COL_QUANTITY].ToString());
              Cartitems.Discount = int.Parse(rd[HanayenConstants.COL_DISCOUNT].ToString());
              Cartitems.Image = rd[HanayenConstants.COL_IMAGE].ToString();
              Cartitems.Price = int.Parse(rd[HanayenConstants.COL_PRICE].ToString());
              Cartitems.ProductCode = rd[HanayenConstants.COL_PRODUCTCODE].ToString();
              Cartitems.ProductName = rd[HanayenConstants.COL_PRODUCTNAME].ToString();
         
          }
          rd.Close();
          con.Close();
          return Cartitems;
      }
      public string UpdateQuantity(CartItems items)
      {
          string result = "";
          try
          {
              if (items != null)
              {
                  SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_PRODUCTID,items.ProductId),
                   new SqlParameter(HanayenConstants.COL_COOKIEID,items.CookieId),
                   new SqlParameter(HanayenConstants.COL_QUANTITY,items.Quantity),
                   new SqlParameter(HanayenConstants.PARAM_OPERATION,"UQ"),            
               };
                  DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_CART, CommandType.StoredProcedure, param);
                  result = "success";
              }
          }
          catch (Exception ex)
          {
              throw ex;
          }
          return result;
      }

    }
}
