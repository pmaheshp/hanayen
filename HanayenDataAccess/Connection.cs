﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
namespace HanayenDataAccess
{
    public class Connection
    {
        static string connectionString = ConfigurationSettings.AppSettings["HanayenConnection"].ToString();
        public  SqlConnection con = null;
        public SqlConnection  Open()
        {
            con = new SqlConnection(connectionString);
            if (con != null)
            {
               
                if (con.State != System.Data.ConnectionState.Open)
                {
                    con.Open();
                }
            }
            return con;
           
        }
        public SqlConnection Open(SqlConnection con1)
        {
            con1 = new SqlConnection(connectionString);
            if (con1 != null)
            {

                if (con1.State != System.Data.ConnectionState.Open)
                {
                    con1.Open();
                }
            }
            return con1;

        }
        
    }
}
