﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HanayenEntity;
using System.Data.SqlClient;
using System.Data;
using HanayenEntity.SupportingModel;

namespace HanayenDataAccess
{
   public class hnCountry
    {
       public string Insert(Country country)
       {
           string result = "";
           try
           {
               if (country != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_COUNTRYNAME,country.CountryName),
                               
               };
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_AddCountry, CommandType.StoredProcedure, param);
                   result = "success";
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;
       }
    }
}
