﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HanayenEntity;
using System.Data.SqlClient;
using System.Data;
using HanayenEntity.SupportingModel;

namespace HanayenDataAccess
{
  public  class HnBanner
    {
        public string AddBannerImages(Banner banneritems)
        {
            string result = "";
            try
            {
                if (banneritems != null)
                {
                    SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_BANNERIMAGE,banneritems.BannerImage),
                   new SqlParameter(HanayenConstants.COL_BANNERISLINK,banneritems.BannerIsLink),
                   new SqlParameter(HanayenConstants.COL_BANNERLINK,banneritems.BannerLink),
                   new SqlParameter(HanayenConstants.COL_BANNERNAME,banneritems.BannerName),
                   new SqlParameter(HanayenConstants.COL_BANNERTYPE,banneritems.BannerType),                  
                   new SqlParameter(HanayenConstants.PARAM_OPERATION,"I"),            
               };
                    DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_Banner, CommandType.StoredProcedure, param);
                    result = "success";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public string UpdateBannerImages(Banner banneritems)
        {
            string result = "";
            try
            {
                if (banneritems != null)
                {
                    SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_BANNERIMAGE,banneritems.BannerImage),
                   new SqlParameter(HanayenConstants.COL_BANNERISLINK,banneritems.BannerIsLink),
                   new SqlParameter(HanayenConstants.COL_BANNERLINK,banneritems.BannerLink),
                   new SqlParameter(HanayenConstants.COL_BANNERNAME,banneritems.BannerName),
                   new SqlParameter(HanayenConstants.COL_BANNERTYPE,banneritems.BannerType),
                   new SqlParameter(HanayenConstants.COL_BANNERID,banneritems.BannerId),                  
                   new SqlParameter(HanayenConstants.PARAM_OPERATION,"U"),            
               };
                    DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_Banner, CommandType.StoredProcedure, param);
                    result = "success";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
       
        public string DeleteBannerImages(Banner banneritems)
        {
            string result = "";
            try
            {
                if (banneritems != null)
                {
                    SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_BANNERID,banneritems.BannerId),
                               
               };
                    DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_DeleteBanner, CommandType.StoredProcedure, param);
                    result = "success";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public List<Banner> GetBanners(string bannertype)
        {
            List<Banner> banner = new List<Banner>();
            SqlConnection con = new SqlConnection();
            SqlParameter[] param = new SqlParameter[] { 
            new SqlParameter(HanayenConstants.PARAM_BANNERTYPE,bannertype),
            new SqlParameter(HanayenConstants.PARAM_OPERATION,"BN"),
           
            
         };
         IDataReader rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_Banner, CommandType.StoredProcedure, param, con);
         while (rd.Read())
         {
                Banner banners = new Banner();
                banners.BannerId = int.Parse(rd[HanayenConstants.COL_BANNERID].ToString());
                banners.BannerImage=rd[HanayenConstants.COL_BANNERIMAGE].ToString();
                banners.BannerName= rd[HanayenConstants.COL_BANNERNAME].ToString();
                banners.BannerLink =rd[HanayenConstants.COL_BANNERLINK].ToString();
                banners.BannerIsLink =rd[HanayenConstants.COL_BANNERISLINK].ToString();
                banners.BannerImage=rd[HanayenConstants.COL_BANNERIMAGE].ToString();
                banner.Add(banners);
          }
            rd.Close();
            con.Close();
            return banner;
        }
        public List<Banner> GetOfferBanners(string bannertype)
        {
            List<Banner> banner = new List<Banner>();
            SqlConnection con = new SqlConnection();
            SqlParameter[] param = new SqlParameter[] { 
            new SqlParameter(HanayenConstants.PARAM_BANNERTYPE,bannertype),
            new SqlParameter(HanayenConstants.PARAM_OPERATION,"OB"),   
            
           };
            IDataReader rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_Banner, CommandType.StoredProcedure, param, con);
            while (rd.Read())
            {
                Banner banners = new Banner();
                banners.BannerImage = rd[HanayenConstants.COL_BANNERIMAGE].ToString();
                banners.BannerName = rd[HanayenConstants.COL_BANNERNAME].ToString();
                banners.BannerLink = rd[HanayenConstants.COL_BANNERLINK].ToString();
                banners.BannerIsLink =rd[HanayenConstants.COL_BANNERISLINK].ToString();
                banner.Add(banners);
            }
            rd.Close();
            con.Close();
            return banner;
        }
        public List<Banner> SelectBanner(string bannerid)
        {
            List<Banner> banner = new List<Banner>();
            SqlConnection con = new SqlConnection();
            SqlParameter[] param = new SqlParameter[] { 
            new SqlParameter(HanayenConstants.PARAM_BANNERID,bannerid),
            new SqlParameter(HanayenConstants.PARAM_OPERATION,"V"),
           
            
         };
            IDataReader rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_Banner, CommandType.StoredProcedure, param, con);
            while (rd.Read())
            {
                Banner banners = new Banner();
                banners.BannerId = int.Parse(rd[HanayenConstants.COL_BANNERID].ToString());
                banners.BannerImage = rd[HanayenConstants.COL_BANNERIMAGE].ToString();
                banners.BannerName = rd[HanayenConstants.COL_BANNERNAME].ToString();
                banners.BannerLink = rd[HanayenConstants.COL_BANNERLINK].ToString();
                banners.BannerIsLink = rd[HanayenConstants.COL_BANNERISLINK].ToString();
                banner.Add(banners);
            }
            rd.Close();
            con.Close();
            return banner;
        }
        public List<Banner> GetBannerList()
        {
            List<Banner> viewbannerlist = new List<Banner>();
            SqlConnection con = new SqlConnection();
            //SqlParameter[] param = new SqlParameter[] { 
            // new SqlParameter(HanayenConstants.PARAM_OPERATION,"J"),
            // };
            IDataReader rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_ViewBanner, CommandType.StoredProcedure, null, con);
            while (rd.Read())
            {
                Banner banner = new Banner();
                banner.BannerId = Convert.ToInt32(rd[HanayenConstants.COL_BANNERID].ToString());
                banner.BannerName = rd[HanayenConstants.COL_BANNERNAME].ToString();
                banner.BannerType = rd[HanayenConstants.COL_BANNERTYPE].ToString();
                banner.BannerImage = rd[HanayenConstants.COL_BANNERIMAGE].ToString();
                banner.BannerIsLink = rd[HanayenConstants.COL_BANNERISLINK].ToString();


                viewbannerlist.Add(banner);
            }
            rd.Close();
            con.Close();
            return viewbannerlist;
        }
    }
}
