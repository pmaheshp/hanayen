﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenDataAccess
{
    internal class HanayenConstants
    {
        //Parameters
        public const string PARAM_CATEGORY = "Category";
        public const string PARAM_CATEGORYID = "CategoryId";
        public const string PARAM_SIZEVALUE = "SizeValue";
        public const string PARAM_SIZEID = "SizeId";
        public const string PARAM_DESIGN = "Design";
        public const string PARAM_PRODUCTNAME = "ProductName";
        public const string PARAM_PRODUCTCODE = "ProductCode";
        public const string PARAM_SKU = "SKU";
        public const string PARAM_DESIGNID = "DesignId";
        public const string PARAM_PRICE = "Price";
        public const string PARAM_DISCOUNT = "Discount";
        public const string PARAM_IMAGEID = "ImageId";
        public const string PARAM_QUANTITY = "Quantity";
        public const string PARAM_SIZEDETID = "SizeDetId";
        public const string PARAM_MODELHEIGHT = "ModelHeight";
        public const string PARAM_COLOR = "Color";
        public const string PARAM_NECKTYPE = "NeckType";
        public const string PARAM_TITLE = "Title";
        public const string PARAM_PRODUCTMATERIAL = "ProductMaterial";
        public const string PARAM_INSTRUCTION = "Instruction";
        public const string PARAM_MODELMEASUREMENT = "ModelMeasurement";
        public const string PARAM_DESCRIPTION = "Description";
        public const string PARAM_PRODUCTID = "ProductId";
        public const string PARAM_IMAGEPATH = "Image";
        public const string PARAM_SUBIMAGEID = "SubImageId";
        public const string PARAM_DATE = "Date";
        public const string PARAM_TYPE = "Type";
        public const string PARAM_START = "Start";
        public const string PARAM_END = "End";
        public const string PARAM_PRODUCTDATE = "ProductDate";

        //COUNTRY
        public const string PARAM_COUNTRYID = "CountryId";
        public const string PARAM_COUNTRYNAME = "CountryName";

       // STATE
        public const string PARAM_STATEID = "StateId";
        public const string PARAM_STATENAME = "StateName";
        //CART ITEMS
        public const string PARAM_CARTID = "CartId";
        public const string PARAM_CookieId = "CookieId";
        public const string PARAM_QUADITY = "Quandity";              
        public const string PARAM_CARTDATE = "CartDate";
        public const string PARAM_ISGUEST = "IsGuest";
        public const string PARAM_CARTSTATUS = "CartStatus";
        public const string PARAM_OPERATION = "Operation";

        //LOGIN
        public const string PARAM_LOGINID = "LoginId";
        public const string PARAM_EMAIL = "Email";
        public const string PARAM_PASSWORD = "Password";
        public const string PARAM_ACTIVATIONKEY = "ActivationKey";
        public const string PARAM_STATUS = "Status";
        public const string PARAM_ISACTIVATE = "IsActivate";

        //Banner

        public const string PARAM_BANNERID = "BannerId";
        public const string PARAM_BANNERNAME = "BannerName";
        public const string PARAM_BANNERIMAGE = "BannerImage";
        public const string PARAM_BANNERLINK = "BannerLink";
        public const string PARAM_BANNERTYPE = "BannerType";
        public const string PARAM_BANNERISLINK = "BannerIsLink";

        
        //HotSale
        public const string PARAM_HOTSALEID = "HotSaleId";
        public const string PARAM_HOTSALEDATE = "HotSaleDate";
        public const string PARAM_ISHOTSALE = "IsHotSale";
     
        
        //Stored Procedure
        public const string HSP_NEWCATEGORY = "HSP_NewCategory";
        public const string HSP_GETCATEGORY = "HSP_GetCategory";
        public const string HSP_NEWSIZE = "HSP_NewSize";
        public const string HSP_GETSIZE = "HSP_GetSize";
        public const string HSP_GETSIZEDESC = "HSP_GetSizeDesc";
        public const string HSP_NEWDESIGN = "HSP_NewDesign";
        public const string HSP_SIZESTANDERD = "HSP_GetSizeStanderd";
        public const string HSP_GETDESIGNMASTER = "HSP_GetDesignMaster";
        public const string HSP_NEWPRODUCT = "HSP_NewProduct";
        public const string HSP_XREFNEWIMAGE = "HSP_NewXrefImage";
        public const string HSP_NEWSUBIMAGE = "HSP_NewSubImage";
        public const string HSP_NEWIMAGE = "HSP_NewImage";
        public const string HSP_NEWPRODUTSIZE = "HSP_NewSizeDetails";
        public const string HSP_GETLASTPRODUCTCODE = "HSP_GetLastProductId";
        public const string HSP_GETGALLERYPRODUCTS = "HSP_GetGalleryProducts";
        public const string HSP_GETPRODUCTDETAILS = "HSP_GetProductDetails";
        public const string HSP_GETPRODUCTSIZE="HSP_GetProductSize";
        public const string HSP_GETPRODUCTIMAGES = "HSP_GetProductImages";
        public const string HSP_GETTOTALITEMCATEGORY = "HSP_GetTotalItemByCategory";
        public const string HSP_CART = "HSP_CART";
        public const string HSP_LOGIN = "HSP_LOGIN";
        public const string HSP_CUSTOMER = "HSP_CUSTOMER";
        public const string HSP_Banner = "HSP_Banner";
        public const string HSP_HotSale = "HSP_HotSale";
        public const string HSP_GetProduct = "HSP_GetProduct";
        public const string HSP_ViewProduct = "HSP_ViewProduct";
        public const string HSP_DeleteProduct = "HSP_DeleteProduct";
        public const string HSP_ViewCustomer = "HSP_ViewCustomer";
        public const string HSP_DeleteCustomer = "HSP_DeleteCustomer";
        public const string HSP_DeleteHotSale = "HSP_DeleteHotSale";
        public const string HSP_ViewBanner = "HSP_ViewBanner";
        public const string HSP_DeleteBanner = "HSP_DeleteBanner";
    
        ///// /////////////////////
        
        public const string HSP_EditCustomer = "HSP_EditCustomer";
        public const string HSP_AddCountry = "HSP_AddCountry";
        public const string HSP_AddState = "HSP_AddState";
        public const string HSP_GetCountryName = "HSP_GetCountryName";

        //Column Name
        public const string COL_SIZEDESCID = "SizeDetId";
        public const string COL_SIZEVALUE = "SizeValue";
        public const string COL_SIZEID = "SizeId";
        public const string COL_CATEGORY = "Category";
        public const string COL_CATEGORYID = "CategoryId";
        public const string COL_SIZENAME = "SizeName";
        public const string COL_DESIGNID = "DesignId";
        public const string COL_DESIGNNAME = "DesignName";
        public const string COL_PRODUCTID = "ProductId";
        public const string COL_TITLE = "Title";
        public const string COL_PRODUCTCODE = "ProductCode";
        public const string COL_IMAGE = "Image";
        public const string COL_COLOR = "Color";
        public const string COL_DESCRIPTION = "Description";
        public const string COL_DISCOUNT = "Discount";
        public const string COL_INSTRUCTION = "Instruction";
        public const string COL_MATERIAL = "ProductMaterial";
        public const string COL_MEASUREMENT = "ModelMeasurement";
        public const string COL_MODELHEIGHT = "ModelHeight";
        public const string COL_NECKTYPE = "NeckType";
        public const string COL_PRICE = "Price";
        public const string COL_PRODUCTNAME = "ProductName";
        public const string COL_PRODUCTDATE = "ProductDate";

        public const string COL_QUANTITY = "Quantity";
        public const string COL_SKU = "SKU";
        public const string COL_SIZEDETID = "SizeDetId";

        public const string COL_CARTID = "CartId";
        public const string COL_COOKIEID = "CookieId";
        public const string COL_QUATITY = "Quantity";        
        public const string COL_CARTDATE = "CartDate";
        public const string COL_ISGUEST = "IsGuest";
        public const string COL_CARTSTATUS = "CartStatus";
       

        //LOGIN COL NAME
        public const string COL_PASSWORD = "Password";
        public const string COL_ACTIVATIONKEY = "ActivationKey";
        public const string COL_STATUS = "Status";
        public const string COL_ISACTIVATE = "IsActivate";
        public const string COL_LOGINID = "LoginId";

        //CUSTOMER COL NAME
        public const string COL_CUSTOMERID = "CustomerId";
        public const string COL_FIRSTNAME = "FirstName";
        public const string COL_LASTNAME = "LastName";
        //public const string COL_COUNTRY = "Country";
        public const string COL_STATE = "State";
        public const string COL_CITY = "City";
        public const string COL_ADDRESS1 = "Address1";
        public const string COL_ADDRESS2 = "Address2";
        public const string COL_POSTALCODE = "PostalCode";
        public const string COL_PHONE = "Phone";
        public const string COL_EMAIL = "Email";
        public const string COL_REFERAL = "Referal";
        public const string COL_CARDNUMBER = "CardNumber";

        //BANNER COL NAME
        public const string COL_BANNERID ="BannerId";
        public const string COL_BANNERNAME ="BannerName";
        public const string COL_BANNERIMAGE ="BannerImage";
        public const string COL_BANNERLINK= "BannerLink";
        public const string COL_BANNERTYPE = "BannerType";
        public const string COL_BANNERISLINK = "BannerIsLink";

        //HotSale
        public const string COL_HOTSALEID = "HotSaleId";
        public const string COL_HOTSALEDATE = "HotSaleDate";
        public const string COL_ISHOTSALE = "IsHotSale";

        //COUNTRY COL NAME
        public const string COL_COUNTRYID = "CountryId";
        public const string COL_COUNTRYNAME = "CountryName";

        //STATE COL NAME
        public const string COL_STATEID = "StateId";
        public const string COL_STATENAME = "StateName";
    }
}
