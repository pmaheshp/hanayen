﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HanayenEntity;
using System.Data.SqlClient;
using System.Data;
using HanayenEntity.SupportingModel;

namespace HanayenDataAccess
{
   public class hnCustomer
    {
       public string Insert(Customers customers)
       {
           string result = "";
           try
           {
               if (customers != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_EMAIL,customers.Email),
                   new SqlParameter(HanayenConstants.COL_LOGINID,customers.LoginId),
                   new SqlParameter(HanayenConstants.PARAM_OPERATION,"I"),            
               };
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_CUSTOMER, CommandType.StoredProcedure, param);
                   result = "success";
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;
       }

       public List<CustomerViewJoin> GetCustomerList()
       {
           List<CustomerViewJoin> viewcustomerlist = new List<CustomerViewJoin>();
           SqlConnection con = new SqlConnection();
           //SqlParameter[] param = new SqlParameter[] { 
           // new SqlParameter(HanayenConstants.PARAM_OPERATION,"J"),
           // };
           IDataReader rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_ViewCustomer, CommandType.StoredProcedure, null, con);
           while (rd.Read())
           {
               CustomerViewJoin customer = new CustomerViewJoin();
               customer.CustomerId = Convert.ToInt32(rd[HanayenConstants.COL_CUSTOMERID].ToString());
               customer.FirstName= rd[HanayenConstants.COL_FIRSTNAME].ToString();
               customer.LastName = rd[HanayenConstants.COL_LASTNAME].ToString();
               customer.CountryName= rd[HanayenConstants.COL_COUNTRYNAME].ToString();
               customer.StateName= rd[HanayenConstants.COL_STATENAME].ToString();
               customer.City = rd[HanayenConstants.COL_CITY].ToString();
               customer.Address1 =rd[HanayenConstants.COL_ADDRESS1].ToString();
               customer.Address2 = rd[HanayenConstants.COL_ADDRESS2].ToString();
               customer.PostalCode = Convert.ToInt32(rd[HanayenConstants.COL_POSTALCODE].ToString());
               customer.Phone = rd[HanayenConstants.COL_PHONE].ToString();
               customer.Email = rd[HanayenConstants.COL_EMAIL].ToString();
               customer.Referal =Convert.ToInt64(rd[HanayenConstants.COL_REFERAL].ToString());
               customer.CardNumber= rd[HanayenConstants.COL_CARDNUMBER].ToString();

               viewcustomerlist.Add(customer);
           }
           rd.Close();
           con.Close();
           return viewcustomerlist;
       }
       public string DeleteCustomerById(Customers customeritems)
       {
           string result = "";
           try
           {
               if (customeritems != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_CUSTOMERID,customeritems.CustomerId)
                              
               };
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_DeleteCustomer, CommandType.StoredProcedure, param);
                   result = "success";
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;
       }
       public string UpdateCustomerDetails(Customers customeritems)
       {
           string result = "";
           try
           {
               if (customeritems != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_FIRSTNAME,customeritems.FirstName),
                   new SqlParameter(HanayenConstants.COL_LASTNAME,customeritems.LastName),
                   new SqlParameter(HanayenConstants.COL_CITY,customeritems.City),
                   new SqlParameter(HanayenConstants.COL_ADDRESS1,customeritems.Address1), 
                   new SqlParameter(HanayenConstants.COL_ADDRESS2,customeritems.Address2), 
                   new SqlParameter(HanayenConstants.COL_POSTALCODE,customeritems.PostalCode), 
                   new SqlParameter(HanayenConstants.COL_PHONE,customeritems.Phone), 
                   new SqlParameter(HanayenConstants.COL_EMAIL,customeritems.Email),
                   new SqlParameter(HanayenConstants.COL_REFERAL,customeritems.Referal),
                   new SqlParameter(HanayenConstants.COL_CARDNUMBER,customeritems.CardNumber)
            };
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_EditCustomer, CommandType.StoredProcedure, param);
                   result = "success";
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;
       }
    }
}
