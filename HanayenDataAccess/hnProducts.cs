﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HanayenEntity;
using HanayenEntity.SupportingModel;
namespace HanayenDataAccess
{
   public  class hnProducts
    { 
       public string AddCategory(Category category)
       {
           string result="";
           try
           {
               SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.PARAM_CATEGORY,category.CategoryName)
               };
               DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_NEWCATEGORY, CommandType.StoredProcedure, param);
              
                  result="Success";
              
           }
           catch(Exception ex)
           {
               result= ex.Message;
           }
           return result;
       }
       public string AddSize(Size size)
       {
           string result = "";
           try
           {
               SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.PARAM_SIZEID,size.SizID),
                   new SqlParameter(HanayenConstants.PARAM_SIZEVALUE,size.SizeDesc[0].SizeValue)
               };
               DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_NEWSIZE, CommandType.StoredProcedure, param);
               
                   result = "Success";
               
           }
           catch (Exception ex)
           {
               result = ex.Message;
           }
           return result;
       }
       public string AddDesign(Design design)
       {
           string result = "";
           try
           {
               SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.PARAM_DESIGN,design.DesignName)
               };
               DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_NEWDESIGN, CommandType.StoredProcedure, param);
               result = "success";
           }
           catch(Exception ex)
           {
               result=ex.Message;
           } return result;
       }
       public string AddProducts(Products products)
       {
           string result = "";
           try
           {
               if (products != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.PARAM_PRODUCTID,products.ProductId),
                   new SqlParameter(HanayenConstants.PARAM_PRODUCTNAME,products.ProductName),
                   new SqlParameter(HanayenConstants.PARAM_PRODUCTCODE,products.ProductCode),
                   new SqlParameter(HanayenConstants.PARAM_SKU,products.SKU),
                   new SqlParameter(HanayenConstants.COL_DESIGNID,products.DesignId.DesignId),
                   new SqlParameter(HanayenConstants.COL_CATEGORYID,products.CategoryId.CategoryId),
                   new SqlParameter(HanayenConstants.PARAM_PRICE,products.Price),
                   new SqlParameter(HanayenConstants.PARAM_DISCOUNT,products.Discount),
                   new SqlParameter(HanayenConstants.PARAM_QUANTITY,products.Quanitity),
                   new SqlParameter(HanayenConstants.PARAM_MODELHEIGHT,products.ModelHeight),
                   new SqlParameter(HanayenConstants.PARAM_COLOR,products.Color),
                   new SqlParameter(HanayenConstants.PARAM_NECKTYPE,products.NeckType),
                   new SqlParameter(HanayenConstants.PARAM_TITLE,products.Title),
                   new SqlParameter(HanayenConstants.PARAM_PRODUCTMATERIAL,products.Material),
                   new SqlParameter(HanayenConstants.PARAM_INSTRUCTION,products.Instruction),
                   new SqlParameter(HanayenConstants.PARAM_MODELMEASUREMENT,products.Measurement),
                   new SqlParameter(HanayenConstants.PARAM_DESCRIPTION,products.Description),
                   new SqlParameter(HanayenConstants.PARAM_PRODUCTDATE,products.ProductDate),
                    new SqlParameter(HanayenConstants.PARAM_OPERATION,"I")

                
               };
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_NEWPRODUCT, CommandType.StoredProcedure, param);
                   
                   result = "success";
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;
       }
       public ErrorCode AddProductSize(xRefProductSize productSize)
       {
           ErrorCode result;
           try
           {

               SqlParameter[] param=new SqlParameter[2];
               SqlParameter param1,param2;
               if (productSize != null && productSize.ProductId > 0)
               {
                   param1 = new SqlParameter(HanayenConstants.PARAM_PRODUCTID, productSize.ProductId);
                   param[0] = param1;
               }
               else { return ErrorCode.EMPTY; }
               if (productSize != null && productSize.SizeDescId > 0)
               {
                   param2 = new SqlParameter(HanayenConstants.PARAM_SIZEDETID, productSize.SizeDescId);
                   param[1] = param2;
               }
               else { return ErrorCode.EMPTY; }
               DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_NEWPRODUTSIZE, CommandType.StoredProcedure, param);
               result = ErrorCode.SUCCESS;
               
           }
           catch(Exception ex)
           {
               result = ErrorCode.FAILED;
               throw ex;
           }
           return result;
       }
       public ErrorCode  AddImage(Images images)
       {ErrorCode errorCode=ErrorCode.EMPTY;
           long result=0;
           try
           {

               if (images!=null && images.File!="")
               {
                   SqlParameter ProductID = new SqlParameter(HanayenConstants.PARAM_PRODUCTID,images.ProductID);
                  
                   SqlParameter[] param = new SqlParameter[2];
                   param[0] = ProductID;
                   SqlParameter fileName = new SqlParameter(HanayenConstants.PARAM_IMAGEPATH, images.File);
                   param[1] = fileName;
                 
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_NEWIMAGE, CommandType.StoredProcedure, param);
                   errorCode = ErrorCode.SUCCESS;
                   
               }
               else
               {
                   errorCode = ErrorCode.EMPTY;
               }
           }
           catch(Exception ex)
           {
               errorCode = ErrorCode.FAILED;
               throw ex;
           }
           return errorCode;
       }
       public List<Category> GetCategory()
       {
           List<Category> categoryList=new List<Category>();
           try
           {
               IDataReader rd;
               SqlConnection con = new SqlConnection(); ;
               rd=DataHelper.hnExcuteReader(HanayenConstants.HSP_GETCATEGORY, CommandType.StoredProcedure, null,con);
               while (rd.Read())
               {
                   Category category = new Category();
                   category.CategoryName = rd[HanayenConstants.COL_CATEGORY].ToString();
                   category.CategoryId = int.Parse(rd[HanayenConstants.COL_CATEGORYID].ToString());
                   categoryList.Add(category);
               }
               rd.Close();
               con.Close();
               
           }
           catch
           {
               throw;
           }
           return categoryList;
       }
       //Check Product Code Availability

       //public List<Products> CheckProductCode( string productcode)
       //{
       //    List<Products> productList = new List<Products>();
       //    try
       //    {
       //        IDataReader rd;
       //        SqlConnection con = new SqlConnection();
       //        Products product = new Products();
       //        SqlParameter[] param = new SqlParameter[]
       //        {
       //            new SqlParameter(HanayenConstants.PARAM_PRODUCTCODE,product.ProductCode)
       //        };
       //        rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_GetProduct, CommandType.StoredProcedure, null, con);
       //        while (rd.Read())
       //        {
       //            Products products = new Products();
       //            products.ProductName = rd[HanayenConstants.COL_PRODUCTNAME].ToString();
       //            products.ProductId= int.Parse(rd[HanayenConstants.COL_PRODUCTID].ToString());
       //            products.ProductCode = rd[HanayenConstants.COL_PRODUCTCODE].ToString();
       //            productList.Add(products);
       //        }
       //        rd.Close();
       //        con.Close();

       //    }
       //    catch
       //    {
       //        throw;
       //    }
       //    return productList;
       //}

       public Products CheckProductCode(string productcode)
       {
           Products product = null;
           IDataReader rd;
           SqlConnection con = new SqlConnection();
           SqlParameter[] param = new SqlParameter[]
           {
                       new SqlParameter(HanayenConstants.PARAM_PRODUCTCODE,productcode),
                       new SqlParameter(HanayenConstants.PARAM_OPERATION,"S")
                
                       
           };
           rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_GetProduct, CommandType.StoredProcedure, param, con);
           if (rd.Read())
           {
               product = new Products();
               product.ProductCode = rd[HanayenConstants.COL_PRODUCTCODE].ToString();
               product.ProductId=int.Parse(rd[HanayenConstants.COL_PRODUCTID].ToString());



           }
           rd.Close();
           con.Close();
           return product;
       }
       public List<Size> GetSize()
       {
           List<Size> sizeList = new List<Size>();
           try
           {
               IDataReader rd;
               SqlConnection con = new SqlConnection();
               rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_GETSIZE, CommandType.StoredProcedure, null,con);
               
               while (rd.Read())
               {

                   Size size = new Size();
                   size.SizID = (SIZE)Enum.Parse(typeof(SIZE), rd[HanayenConstants.COL_SIZEID].ToString());
                   size.SizeName = rd[HanayenConstants.COL_SIZENAME].ToString();
                   size.SizeDesc = new List<SizeDesc>();
                   sizeList.Add(size);
               }
               rd.Close();
               con.Close();
           }
           catch
           {
               throw;
           }
           return sizeList;
       }
       public List<SizeDesc> GetSizeDesc(int sizeId)
       {
                List<SizeDesc> descList = new List<SizeDesc>();
                   IDataReader rd;
                   SqlConnection con = new SqlConnection();
                   SqlParameter[] param=new SqlParameter[]{
                       new SqlParameter(HanayenConstants.PARAM_SIZEID,sizeId)
                   };
                   rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_GETSIZEDESC, CommandType.StoredProcedure, param,con);
                   while (rd.Read())
                   {
                       SizeDesc sizeDesc=new SizeDesc();
                       sizeDesc.SizeDetId=int.Parse(rd[HanayenConstants.COL_SIZEDESCID].ToString());
                       sizeDesc.SizeValue=rd[HanayenConstants.COL_SIZEVALUE].ToString();
                       descList.Add(sizeDesc);
                   }
           rd.Close();
           con.Close();
           return descList;
       }
       public List<Design> GetDesign()
       {
           List<Design> designList = new List<Design>();
           IDataReader rd;
           SqlConnection con = new SqlConnection();
           rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_GETDESIGNMASTER, CommandType.StoredProcedure, null,con);
           while (rd.Read())
           {
               Design design = new Design();
               design.DesignId = int.Parse(rd[HanayenConstants.COL_DESIGNID].ToString());
               design.DesignName = rd[HanayenConstants.COL_DESIGNNAME].ToString();
               designList.Add(design);
           }
           rd.Close();
           con.Close();
           return designList;

       }
       public string GetLastProductCode()
       {
           string productCode = "";
           IDataReader rd;
           SqlConnection con = new SqlConnection();
           rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_GETLASTPRODUCTCODE, CommandType.StoredProcedure, null,con);
           if (rd.Read())
           {
               productCode = rd[HanayenConstants.COL_PRODUCTID].ToString();
           }
           else
           {
             productCode= "0";
           }
           rd.Close();
           con.Close();
           return productCode;
       }
       public List<Products> GetGalleryProducts(int category,int start,int end)
       {
           List<Products> products = new List<Products>();
           SqlConnection con = new SqlConnection();
           SqlParameter[] param = new SqlParameter[] { 
            new SqlParameter(HanayenConstants.PARAM_CATEGORY,category),
            new SqlParameter(HanayenConstants.PARAM_START,start),
            new SqlParameter(HanayenConstants.PARAM_END,end)
           };
           IDataReader rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_GETGALLERYPRODUCTS, CommandType.StoredProcedure, param,con);
           while (rd.Read())
           {
               Products product = new Products();
               product.CategoryId = new Category();
               product.ProductId = long.Parse(rd[HanayenConstants.COL_PRODUCTID].ToString());
               product.CategoryId.CategoryId = int.Parse(rd[HanayenConstants.COL_CATEGORYID].ToString());
               product.Title = rd[HanayenConstants.COL_TITLE].ToString();
               product.Price = double.Parse(rd[HanayenConstants.COL_PRICE].ToString());
               double discount=double.Parse(rd[HanayenConstants.COL_DISCOUNT].ToString());
               product.Discount =  product.Price*(1-discount/100);
               product.ImageId = new Images();
               product.ImageId.File = rd[HanayenConstants.COL_IMAGE].ToString();
               products.Add(product);

           }
           rd.Close();
           con.Close();
           return products;
       }
       public List<Products> GetProductDescription(long productId)
       {
           List<Products> products = new List<Products>();
           SqlParameter[] param = new SqlParameter[]
           {
               new SqlParameter(HanayenConstants.PARAM_PRODUCTID,productId)
           };
           SqlConnection con = new SqlConnection();
           IDataReader rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_GETPRODUCTDETAILS,CommandType.StoredProcedure,param,con);
           while (rd.Read())
           {
               Products product = new Products();
               product.CategoryId=new Category();
               product.CategoryId.CategoryId= int.Parse(rd[HanayenConstants.COL_CATEGORYID].ToString());
               product.CategoryId.CategoryName = rd[HanayenConstants.COL_CATEGORY].ToString();
               product.Color = rd[HanayenConstants.COL_COLOR].ToString();
               product.Description = rd[HanayenConstants.COL_DESCRIPTION].ToString();
               product.DesignId = new Design();
               product.DesignId.DesignId = int.Parse(rd[HanayenConstants.COL_DESIGNID].ToString());
               product.DesignId.DesignName = rd[HanayenConstants.COL_DESIGNNAME].ToString();
               product.Discount = Double.Parse(rd[HanayenConstants.COL_DISCOUNT].ToString());
             //Get Imaage From Another Function
               product.imageList = GetImages(productId);
               product.Instruction = rd[HanayenConstants.COL_INSTRUCTION].ToString();
               product.Material = rd[HanayenConstants.COL_MATERIAL].ToString();
               product.Measurement = rd[HanayenConstants.COL_MEASUREMENT].ToString();
               product.ModelHeight = rd[HanayenConstants.COL_MODELHEIGHT].ToString();
               product.NeckType = rd[HanayenConstants.COL_NECKTYPE].ToString();
               product.Price = Double.Parse(rd[HanayenConstants.COL_PRICE].ToString());
               product.ProductCode = rd[HanayenConstants.COL_PRODUCTCODE].ToString();
               product.ProductId = long.Parse( rd[HanayenConstants.COL_PRODUCTID].ToString());
               product.ProductName = rd[HanayenConstants.COL_PRODUCTNAME].ToString();
               product.Quanitity = int.Parse(rd[HanayenConstants.COL_QUANTITY].ToString());
               product.SKU = rd[HanayenConstants.COL_SKU].ToString();
               product.Title = rd[HanayenConstants.COL_TITLE].ToString();
               product.ProductSize = GetProductSize(productId);
               products.Add(product);
               
           }
           return products;
           
       }
       public List<xRefProductSize> GetProductSize(long productId)
       {
            List<xRefProductSize> productSizes = new List<xRefProductSize>();
           SqlParameter[] param = new SqlParameter[]
           {
               new SqlParameter(HanayenConstants.PARAM_PRODUCTID,productId)
           };
           SqlConnection con = new SqlConnection();
           IDataReader rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_GETPRODUCTSIZE,CommandType.StoredProcedure,param,con);
           while (rd.Read())
           {
               xRefProductSize productSize = new xRefProductSize();
               productSize.SizeDet = new xRefSize();
               productSize.SizeDet.SizeDesc = new SizeDesc();
               productSize.SizeDet.SizeDesc.SizeDetId = long.Parse(rd[HanayenConstants.COL_SIZEDETID].ToString());
               productSize.SizeDet.SizeDesc.SizeValue = rd[HanayenConstants.COL_SIZEVALUE].ToString();
               productSize.ProductId = long.Parse(rd[HanayenConstants.COL_PRODUCTID].ToString());
               productSizes.Add(productSize);
           }
           return productSizes;
       }
       public List<Images> GetImages(long ProductId)
       {
           List<Images> images = new List<Images>();
           SqlParameter[] param = new SqlParameter[]
           {
               new SqlParameter(HanayenConstants.PARAM_PRODUCTID,ProductId)
           };
           SqlConnection con = new SqlConnection();
           IDataReader rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_GETPRODUCTIMAGES, CommandType.StoredProcedure, param, con);
           while (rd.Read())
           {
               Images  image = new Images();
               image.File = rd[HanayenConstants.COL_IMAGE].ToString();
               images.Add(image);
           }
           return images;
       }
       public string GetTotalProductsCount(int category)
       {    string result="0";
           SqlParameter[] param = new SqlParameter[]
           {
               new SqlParameter(HanayenConstants.PARAM_CATEGORYID,category)
               
           };
           result = DataHelper.hnExecuteScalar(HanayenConstants.HSP_GETTOTALITEMCATEGORY, CommandType.StoredProcedure, param);
           return result;
       }
       //Get New Arrivals
       public List<Products> GetNewArrivals()
       {
           List<Products> products = new List<Products>();
           SqlParameter[] param = new SqlParameter[]
           {
               new SqlParameter(HanayenConstants.PARAM_OPERATION,"S")
           };
           SqlConnection con = new SqlConnection();
           IDataReader rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_NEWPRODUCT, CommandType.StoredProcedure, param, con);
           while (rd.Read())
           {
               Products product = new Products();
               product.ProductId = int.Parse(rd[HanayenConstants.COL_PRODUCTID].ToString());
               product.ImageId = new Images();
               product.ImageId.File = rd[HanayenConstants.COL_IMAGE].ToString();
               product.Price = Double.Parse(rd[HanayenConstants.COL_PRICE].ToString());  
               product.ProductName = rd[HanayenConstants.COL_PRODUCTNAME].ToString();
               product.Quanitity = int.Parse(rd[HanayenConstants.COL_QUANTITY].ToString());      
               products.Add(product);

           }
           return products;
       }
       //Get HotSales
       public List<Products> GetHotsaleProduct()
       {
           List<Products> prdct = new List<Products>();
           IDataReader rd;
           SqlConnection con = new SqlConnection();
           SqlParameter[] param = new SqlParameter[]
           {
             new SqlParameter(HanayenConstants.PARAM_OPERATION,"FV")
           };
           rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_HotSale, CommandType.StoredProcedure, param, con);
           while (rd.Read())
           {
               Products product = new Products();
               product.ProductId = int.Parse(rd[HanayenConstants.COL_PRODUCTID].ToString());
               product.ImageId = new Images();
               product.ImageId.File = rd[HanayenConstants.COL_IMAGE].ToString();
               product.ProductName = rd[HanayenConstants.COL_PRODUCTNAME].ToString();
               product.Price = int.Parse(rd[HanayenConstants.COL_PRICE].ToString());              
               prdct.Add(product);
           }
           rd.Close();
           con.Close();
           return prdct;
       }

       //Get Product List
       public List<ProductViewJoin> GetProductList()
       {
           List<ProductViewJoin> viewproductlist = new List<ProductViewJoin>();
           SqlConnection con = new SqlConnection();
           //SqlParameter[] param = new SqlParameter[] { 
           // new SqlParameter(HanayenConstants.PARAM_OPERATION,"J"),
           // };
           IDataReader rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_ViewProduct, CommandType.StoredProcedure, null, con);
           while (rd.Read())
           {
               ProductViewJoin infoProductView = new ProductViewJoin();
               infoProductView.ProductId= Convert.ToInt32(rd[HanayenConstants.COL_PRODUCTID].ToString());
               infoProductView.ProductName =rd[HanayenConstants.COL_PRODUCTNAME].ToString();
               infoProductView.ProductCode = rd[HanayenConstants.COL_PRODUCTCODE].ToString();
               infoProductView.CategoryName = rd[HanayenConstants.COL_CATEGORY].ToString();
               infoProductView.SKU= rd[HanayenConstants.COL_SKU].ToString();
               infoProductView.DesignName = rd[HanayenConstants.COL_DESIGNNAME].ToString();
               infoProductView.Price = Convert.ToDouble(rd[HanayenConstants.COL_PRICE].ToString());
               infoProductView.Discount = Convert.ToDouble(rd[HanayenConstants.COL_DISCOUNT].ToString());
               infoProductView.Quanitity =Convert.ToInt32(rd[HanayenConstants.COL_QUANTITY].ToString());
               infoProductView.ModelHeight = rd[HanayenConstants.COL_MODELHEIGHT].ToString();
               infoProductView.Color = rd[HanayenConstants.COL_COLOR].ToString();
               infoProductView.NeckType = rd[HanayenConstants.COL_NECKTYPE].ToString();
               infoProductView.Title = rd[HanayenConstants.COL_TITLE].ToString();
               infoProductView.Material = rd[HanayenConstants.COL_MATERIAL].ToString();
               infoProductView.Instruction = rd[HanayenConstants.COL_INSTRUCTION].ToString();
               infoProductView.Measurement = rd[HanayenConstants.COL_MEASUREMENT].ToString();
               infoProductView.Description = rd[HanayenConstants.COL_DESCRIPTION].ToString();
               infoProductView.ProductDate = rd[HanayenConstants.COL_PRODUCTDATE].ToString();
               viewproductlist.Add(infoProductView);
           }
           rd.Close();
           con.Close();
           return viewproductlist;
       }
       //Delete Product
       public string DeleteProduct(Products productitems)
       {
           string result = "";
           try
           {
               if (productitems != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_PRODUCTID,productitems.ProductId),
                               
               };
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_DeleteProduct, CommandType.StoredProcedure, param);
                   result = "success";
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;
       }
    
    }
}
