﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
namespace HanayenDataAccess
{
   public  class DataHelper:Connection
    {
      
       public static int hnExecuteNonQuery(string command, CommandType commandType, SqlParameter[] param)
       {
           int res = 0;
           try
           {
               Connection cont = new Connection();
               SqlConnection con = cont.Open();
               using (SqlCommand cmd = con.CreateCommand())
               {
                   cmd.CommandText = command;
                   if (param.Length > 0)
                   {
                       cmd.Parameters.AddRange(param);
                   }
                   cmd.CommandType = commandType;
                   res=cmd.ExecuteNonQuery();
                   con.Close();
               }
           }
           catch
           {

               throw;
           }
           return res;
       }
       public static IDataReader hnExcuteReader(string command, CommandType commandType, SqlParameter[] param,SqlConnection con)
       {
           SqlDataReader rd = null;
           try
           {
               Connection cont = new Connection();
              con = cont.Open();
               using (SqlCommand cmd = con.CreateCommand())
               {
                   cmd.CommandText = command;
                   cmd.CommandType = commandType;
                   if (param!=null && param.Length>0)
                   {
                       cmd.Parameters.AddRange(param);
                   }
                    rd = cmd.ExecuteReader();
                   
                    return rd;
               }
           }
           catch
           {
               throw;
           }
       }
       public static string hnExecuteScalar(string command, CommandType commandType, SqlParameter[] param)
       {
           object result = new object();
           try
           {
               if (command.Length > 1)
               {
                   Connection cont = new Connection();
                   SqlConnection con = cont.Open();
                   using (SqlCommand cmd = con.CreateCommand())
                   {
                       cmd.CommandType = commandType;
                       cmd.CommandText = command;
                       cmd.Parameters.AddRange(param);
                       result=cmd.ExecuteScalar();

                   }
               }
           }
           catch
           {
               throw;
           }
           return result.ToString();
       }
       public static int hnExecuteScalarForInt(string command, CommandType commandType, SqlParameter[] param)
       {
           object result = new object();
           try
           {
               if (command.Length > 1)
               {
                   Connection cont = new Connection();
                   SqlConnection con = cont.Open();
                   using (SqlCommand cmd = con.CreateCommand())
                   {
                       cmd.CommandType = commandType;
                       cmd.CommandText = command;
                       cmd.Parameters.AddRange(param);
                       result = cmd.ExecuteScalar();

                   }
               }
           }
           catch
           {
               throw;
           }
           return Convert.ToInt32(result);
       }
       
    }
}
