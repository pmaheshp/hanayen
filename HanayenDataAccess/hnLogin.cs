﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HanayenEntity;
using System.Data;

namespace HanayenDataAccess
{
   public class hnLogin
   {
       public int Insert(Login login)
       {
           int result = 0;
           try
           {
               if (login != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_EMAIL,login.Email),
                   new SqlParameter(HanayenConstants.COL_PASSWORD,login.Password),
                   new SqlParameter(HanayenConstants.COL_ACTIVATIONKEY,login.ActivationKey),
                   new SqlParameter(HanayenConstants.COL_STATUS,login.Status),
                   new SqlParameter(HanayenConstants.COL_ISACTIVATE,login.IsActivate),
                   new SqlParameter(HanayenConstants.PARAM_OPERATION,"I"),            
               };
              result=DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_LOGIN, CommandType.StoredProcedure, param);
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;
       }
       public string EditPassword(Login login)
       {
           string result = "";
           try
           {
               if (login != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_LOGINID,login.LoginId),
                   new SqlParameter(HanayenConstants.COL_PASSWORD,login.Password),
                   new SqlParameter(HanayenConstants.PARAM_OPERATION,"U"),            
               };
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_LOGIN, CommandType.StoredProcedure, param);
                   result = "success";
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;
       }
       public string EditActivationKey(Login login)
       {
           string result = "";
           try
           {
               if (login != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_LOGINID,login.LoginId),
                   new SqlParameter(HanayenConstants.COL_ACTIVATIONKEY,login.ActivationKey),
                   new SqlParameter(HanayenConstants.PARAM_OPERATION,"AC"),            
               };
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_LOGIN, CommandType.StoredProcedure, param);
                   result = "success";
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;
       }
       public Login CheckUser(string email,string password)
       {
           Login login = null;
           IDataReader rd;
           SqlConnection con = new SqlConnection();
           SqlParameter[] param = new SqlParameter[]
           {
                       new SqlParameter(HanayenConstants.PARAM_EMAIL,email),
                       new SqlParameter(HanayenConstants.PARAM_PASSWORD,password),
                       new SqlParameter(HanayenConstants.PARAM_OPERATION,"CH")
           };
           rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_LOGIN, CommandType.StoredProcedure, param, con);
           if (rd.Read())
           {
                login = new Login();
               login.Email = rd[HanayenConstants.COL_EMAIL].ToString();
               login.Password = rd[HanayenConstants.COL_PASSWORD].ToString();
               
           }
           rd.Close();
           con.Close();
           return login;
       }
       public int GetMaxLoginId()
       {
           int result = 0;
           SqlParameter[] param = new SqlParameter[]
           {
               new SqlParameter(HanayenConstants.PARAM_OPERATION,'M')
               
           };
           result = DataHelper.hnExecuteScalarForInt(HanayenConstants.HSP_LOGIN, CommandType.StoredProcedure, param);
           return result;
       }
       public Login CheckEmail(string email)
       {
           Login login = null;
           IDataReader rd;
           SqlConnection con = new SqlConnection();
           SqlParameter[] param = new SqlParameter[]
           {
                       new SqlParameter(HanayenConstants.PARAM_EMAIL,email),
                
                       new SqlParameter(HanayenConstants.PARAM_OPERATION,"EMAIL")
           };
           rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_LOGIN, CommandType.StoredProcedure, param, con);
           if (rd.Read())
           {
               login = new Login();
               login.Email = rd[HanayenConstants.COL_EMAIL].ToString();
               

           }
           rd.Close();
           con.Close();
           return login;
       }
       //Activate user
       public string ActivateUser(Login login)
       {
           string result = "";
           try
           {
               if (login != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_EMAIL,login.Email),
                   new SqlParameter(HanayenConstants.COL_STATUS,login.Status),
                    new SqlParameter(HanayenConstants.COL_ISACTIVATE,login.IsActivate),
                   new SqlParameter(HanayenConstants.PARAM_OPERATION,"ACTIVATE"),            
               };
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_LOGIN, CommandType.StoredProcedure, param);
                   result = "success";
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;
       }
   }
}
