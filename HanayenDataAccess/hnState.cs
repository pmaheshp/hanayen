﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HanayenEntity;
using System.Data.SqlClient;
using System.Data;
using HanayenEntity.SupportingModel;


namespace HanayenDataAccess
{
    public class hnState
    {
        public string Insert(State state)
        {
            string result = "";
            try
            {
                if (state != null)
                {
                    SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_STATENAME,state.StateName),
                   new SqlParameter(HanayenConstants.COL_COUNTRYID,state.CountryId),
                               
               };
                    DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_AddState, CommandType.StoredProcedure, param);
                    result = "success";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public List<Country> GetCountry()
        {
            List<Country> CountryNameList = new List<Country>();
            try
            {
                IDataReader rd;
                SqlConnection con = new SqlConnection(); ;
                rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_GetCountryName, CommandType.StoredProcedure, null, con);
                while (rd.Read())
                {
                    Country country = new Country();
                    country.CountryName = rd[HanayenConstants.COL_COUNTRYNAME].ToString();

                    CountryNameList.Add(country);
                }
                rd.Close();
                con.Close();

            }
            catch
            {
                throw;
            }
            return CountryNameList;
        }
    }
}
