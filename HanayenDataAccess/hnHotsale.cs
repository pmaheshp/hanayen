﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HanayenEntity;
using System.Data.SqlClient;
using System.Data;
using HanayenEntity.SupportingModel;

namespace HanayenDataAccess
{
   public class hnHotsale
    {
       public string AddHotsale(HotSale hotsale)
       {
           string result = "";
           try
           {
               SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.PARAM_PRODUCTID,hotsale.ProductId),
                   new SqlParameter(HanayenConstants.PARAM_HOTSALEDATE,hotsale.HotSaleDate),
                   new SqlParameter(HanayenConstants.PARAM_ISHOTSALE,hotsale.IsHotSale),
                   new SqlParameter(HanayenConstants.PARAM_OPERATION,"I") 
                };
               DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_HotSale, CommandType.StoredProcedure, param);

               result = "Success";

           }
           catch (Exception ex)
           {
               result = ex.Message;
           }
           return result;
       }

       public List<HotSaleJoin> GetHotSaleisBy()
       {
            List<HotSaleJoin> lstHotSale = new List<HotSaleJoin>();
            SqlConnection con = new SqlConnection();
            SqlParameter[] param = new SqlParameter[] { 
            new SqlParameter(HanayenConstants.PARAM_OPERATION,"J"),
            };
            IDataReader rd = DataHelper.hnExcuteReader(HanayenConstants.HSP_HotSale, CommandType.StoredProcedure, param, con);
            while (rd.Read())
            {
                HotSaleJoin infoHotSale = new HotSaleJoin();
                infoHotSale.HotSaleId = Convert.ToInt32(rd[HanayenConstants.COL_HOTSALEID].ToString());
                infoHotSale.ProductId = Convert.ToInt32(rd[HanayenConstants.COL_PRODUCTID].ToString());
                infoHotSale.ProductName = rd[HanayenConstants.COL_PRODUCTNAME].ToString();
                infoHotSale.ProductCode = rd[HanayenConstants.COL_PRODUCTCODE].ToString();
                infoHotSale.HotSaleDate = Convert.ToDateTime(rd[HanayenConstants.COL_HOTSALEDATE].ToString());
                lstHotSale.Add(infoHotSale);
            }
            rd.Close();
            con.Close();
            return lstHotSale;
       }
       public string DeleteHotsaleById(HotSale hotsaleitems)
       {
           string result = "";
           try
           {
               if (hotsaleitems != null)
               {
                   SqlParameter[] param = new SqlParameter[]
               {
                   new SqlParameter(HanayenConstants.COL_HOTSALEID,hotsaleitems.HotSaleId)
                              
               };
                   DataHelper.hnExecuteNonQuery(HanayenConstants.HSP_DeleteHotSale, CommandType.StoredProcedure, param);
                   result = "success";
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return result;

       }
    }
}
