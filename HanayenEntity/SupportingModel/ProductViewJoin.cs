﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity.SupportingModel
{
    public class ProductViewJoin
    {
        public Int64 ProductId { get; set; }
        public string ProductName { set; get; }
        public string ProductCode { set; get; }
        public string CategoryName { set; get; }
        public string SKU { set; get; }
        public string DesignName { set; get; }
        public double Price { set; get; }
        public double Discount { set; get; }
        public int Quanitity { set; get; }
        public string ModelHeight { set; get; }
        public string Color { set; get; }
        public string NeckType { set; get; }
        public string Title { set; get; }
        public string Material { set; get; }
        public string Instruction { set; get; }
        public string Measurement { set; get; }
        public string Description { set; get; }
        public string ProductDate { set; get; }


       
    }
}
