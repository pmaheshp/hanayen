﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity.SupportingModel
{
    public class CustomerViewJoin
    {
        public long CustomerId { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string CountryName { set; get; }
        public string StateName { set; get; }
        public string City { set; get; }
        public string Address1 { set; get; }
        public string Address2 { set; get; }
        public int PostalCode { set; get; }
        public string Phone { set; get; }
        public string Email { set; get; }
        public long Referal { get; set; }
        public string CardNumber { set; get; }
      

    }
}
