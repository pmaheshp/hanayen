﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity.SupportingModel
{
    public class HotSaleJoin
    {
        public Int64 HotSaleId { get; set; }
        public Int64 ProductId { set; get; }
        public string ProductName { set; get; }
        public string ProductCode { set; get; }
        public DateTime HotSaleDate { set; get; }
    }
}
