﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity.SupportingModel
{
  public class CartItemList
    {
        public int CartId { get; set; }
        public int Quantity { get; set; }
        public string CookieId { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public float Discount { get; set; }
        public string Image { get; set; }
        public string Color { get; set; }

    }
}
