﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity
{
    public class HotSale
    {
        public int HotSaleId { get; set; }
        public Int64 ProductId { set; get; }
        public DateTime HotSaleDate { get; set;}
        public string IsHotSale { get; set; }
    }
}
