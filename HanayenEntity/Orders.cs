﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity
{
    public class Orders
    {
        public long OrderId { set; get; }
        public Customers Customer { set; get; }
        public DateTime Date { set; get; }
        public List<OrderItems> OrderItems{set;get;}
    }
    public class OrderItems
    {
        public long OrderItemId { set; get; }
        public Products Product { set; get; }
        
        public Size Size { set; get; }
        public int Qty { set; get; }
    }
}
