﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity
{
    public class Images
    {
        public string File { set; get; }
       
        public int Type { set; get; }
        public long ProductID { set; get; }
        public long ImageId { set; get; }
        public IMAGETYPE ImageType { set; get; }
    }
    public enum IMAGETYPE
    {
        LARGE = 1,
        SMALL = 2,
        THUMB = 3
    }
}
