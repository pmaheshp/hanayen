﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity
{
    public class Size
    {
        public SIZE SizID = new SIZE();
        public string SizeName { set; get; }
        public List<SizeDesc> SizeDesc { set; get; }

    }
    public class SizeDesc
    {
        public long SizeDetId { set; get; }
       
        public string SizeValue { set; get; }
    }
    public class xRefSize
    {
        public Size Size { set; get; }
        public SizeDesc SizeDesc { set; get; }
    }
    public class xRefProductSize
    {
        public long SizeDescId { set; get; }
        public long ProductId { set; get; }
        public xRefSize SizeDet { set; get; }
    }
    public enum SIZE
    {
        UK=1,
        US=2,
        UNIVERSAL
    }

}
