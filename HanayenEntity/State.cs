﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity
{
   public class State
    {
        public long StateId { get; set; }
        public string StateName { get; set; }
        public int CountryId { set; get; }
    }
}
