﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity
{
    public class Category
    {
            
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
    public class Design
    {
        public int DesignId { set; get; }
        public string DesignName { set; get; }
    }
}
