﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity
{
  public  class CartItems
    {

        public int CartId { get; set; }
        public string CookieId { get; set; }
        public int Quantity { get; set; }
        public long ProductId { get; set; }
        public DateTime CartDate { get; set; }
        public string IsGuest { get; set; }
        public int CartStatus { get; set; }
        public long CustomerId { get; set; }
    
    }


    }
