﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity
{
  public  class Banner
    {
        
        public int BannerId { get; set; }
        public string BannerName { get; set; }
        public string BannerImage { get; set; }
        public string BannerLink { get; set; }        
        public string BannerIsLink { get; set; }
        public string BannerType { get; set; }
        
       
    }
}
