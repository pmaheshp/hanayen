﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity
{
   public class Login
   {
       public int LoginId { get; set; }
       public string Email { get; set; }
       public string Password { get; set; }
       public string ActivationKey { get; set; }
       public string Status { get; set; }
       public string IsActivate{ get; set; }

    }
}
