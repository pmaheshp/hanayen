﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity
{
    public class Country
    {
        public int CountryId { set; get; }
        public string CountryName { set; get; }
    }
}
