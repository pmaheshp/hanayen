﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HanayenEntity
{
    public class Customers
    {
        public long CustomerId { set; get; }
        public long LoginId { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public int CountryId { set; get; }
        public long StateId { set; get; }
        public string City { set; get; }
        public string Address1 { set; get; }
        public string Address2 { set; get; }
        public int PostalCode { set; get; }
        public string Phone { set; get; }
        public string Email { set; get; }
        public long Referal { get; set; }//If Customer Billing Address is Diferent}
        public string CardNumber { set; get; }
      
    }
}
