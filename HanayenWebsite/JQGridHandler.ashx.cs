﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Collections.Specialized;
using HanayenEntity.SupportingModel;
using HanayenBussiness;

namespace HanayenWebsite
{
    /// <summary>
    /// Summary description for JQGridHandler
    /// </summary>
    public class JQGridHandler : IHttpHandler
    {


        public void ProcessRequest(HttpContext context)
        {
            System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
            string strOperation = forms.Get("oper");
            HanayenEntity.HotSale hotsale = new HanayenEntity.HotSale();
            hnHotsaleHandler hotSaleHandler = new hnHotsaleHandler();
            List<HotSaleJoin> collectionEmployee = hotSaleHandler.hnGetHotSaleJoin();
            string strResponse = string.Empty;

            if (strOperation == null)
            {
                //oper = null which means its first load.
                var jsonSerializer = new JavaScriptSerializer();
                context.Response.Write(jsonSerializer.Serialize(collectionEmployee.AsQueryable<HotSaleJoin>().ToList<HotSaleJoin>()));
            }
            else if (strOperation == "del")
            {
                //var query = Query.EQ("_id", forms.Get("EmpId").ToString());
                //collectionEmployee.Remove(query);
                //strResponse = "Employee record successfully removed";
                //context.Response.Write(strResponse);
                hotsale.HotSaleId = Convert.ToInt32(forms.Get("HotSaleId").ToString());
                var result = hotSaleHandler.hnDeleteHotsale(hotsale);
                if (result == "success")
                {
                    strResponse = "Customer Deleted Sucessfully";
                    context.Response.Write(strResponse);
                }
                else
                {
                    strResponse = "Customer Deletion Failed";
                    context.Response.Write(strResponse);
                }
            }
            else
            {
                //string strOut=string.Empty;
                //AddEdit(forms, collectionEmployee, out strOut);
                //context.Response.Write(strOut);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        //private void AddEdit(NameValueCollection forms, MongoCollection collectionEmployee, out string strResponse)
        //{
        //    string strOperation = forms.Get("oper");
        //    int strEmpId = 0;
        //    if (strOperation == "add")
        //    {
        //        strEmpId = Convert.ToInt32(forms.Get("EmpId").ToString());
        //    }
        //    else if (strOperation == "edit")
        //    {
        //        var result = collectionEmployee.AsQueryable<Employee>().Select(c => c._id).Max();
        //        strEmpId = (Convert.ToInt32(result) + 1);
        //    }

        //    string strFirstName = forms.Get("FirstName").ToString();
        //    string strLastName = forms.Get("LastName").ToString();
        //    string strLastSSN = forms.Get("LastSSN").ToString();
        //    string strDepartment = forms.Get("Department").ToString();
        //    string strAge = forms.Get("Age").ToString();
        //    int strSalary = Convert.ToInt32(forms.Get("Salary").ToString());
        //    string strAddress = forms.Get("Address").ToString();
        //    string strMaritalStatus = forms.Get("MaritalStatus").ToString();
        //    Employee objEmp = new Employee();
        //    objEmp._id = strEmpId;
        //    objEmp.FirstName = strFirstName;
        //    objEmp.LastName = strLastName;
        //    objEmp.LastSSN = strLastSSN;
        //    objEmp.Department = strDepartment;
        //    objEmp.Age = Convert.ToInt32(strAge);
        //    objEmp.Address = strAddress;
        //    objEmp.MaritalStatus = strMaritalStatus;
        //    objEmp.Salary = strSalary;
        //    collectionEmployee.Save(objEmp);
        //    strResponse = "Employee record successfully updated";
        //}

    }
}