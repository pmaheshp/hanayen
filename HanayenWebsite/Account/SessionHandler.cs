﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HanayenWebsite.Account
{
    public class SessionHandler
    {
        private static string _loginId="LoginId";
        private static string _email = "Email";
        private static string _password = "Password";
        private static string _activationkey = "ActivationKey";



        public static int LoginId
        {
            get
            {
                if(HttpContext.Current.Session[SessionHandler._loginId]==null)
                {return 0;}
                else
                {return Convert.ToInt32(HttpContext.Current.Session[SessionHandler._loginId]);}
            }
            set
            { HttpContext.Current.Session[SessionHandler._loginId] = value; }

       }
        public static string Email
        {
            get
            {
                if (HttpContext.Current.Session[SessionHandler._email] == null)
                { return string.Empty; }
                else
                { return HttpContext.Current.Session[SessionHandler._email].ToString(); }
            }
            set
            { HttpContext.Current.Session[SessionHandler._email] = value; }
        }
        public static string Password
        {
            get
            {
                if (HttpContext.Current.Session[SessionHandler._password] == null)
                { return string.Empty; }
                else
                { return HttpContext.Current.Session[SessionHandler._password].ToString(); }
            }
            set
            { HttpContext.Current.Session[SessionHandler._password] = value; }
        }
        public static string Activationkey
        {
            get
            {
                if (HttpContext.Current.Session[SessionHandler._activationkey] == null)
                { return string.Empty; }
                else
                { return HttpContext.Current.Session[SessionHandler._activationkey].ToString(); }
            }
            set
            { HttpContext.Current.Session[SessionHandler._activationkey] = value; }
        }

        public static void RemoveSession()
        {
            if (HttpContext.Current.Session[SessionHandler._loginId] != null) HttpContext.Current.Session.Remove(SessionHandler._loginId);
            if (HttpContext.Current.Session[SessionHandler._email] != null) HttpContext.Current.Session.Remove(SessionHandler._email);
        }
    }
}