﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Collections.Specialized;
using HanayenEntity.SupportingModel;
using HanayenBussiness;
using HanayenEntity;

namespace HanayenWebsite
{
    /// <summary>
    /// Summary description for JQGridHandler
    /// </summary>
    public class JQCustomerHandler : IHttpHandler
    {
        
        public void ProcessRequest(HttpContext context)
        {
            System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
            string strOperation = forms.Get("oper");
            HanayenEntity.Customers customer = new HanayenEntity.Customers();
            hnCustomerHandler customerHandler = new hnCustomerHandler();
            List<CustomerViewJoin> collectionEmployee = customerHandler.hnGetCustomerView();
            string updatecustomer = customerHandler.hnUpdateCustomer(customer);
            string strResponse = string.Empty;

            if (strOperation == null)
            {
                //oper = null which means its first load.
                var jsonSerializer = new JavaScriptSerializer();
                context.Response.Write(jsonSerializer.Serialize(collectionEmployee.AsQueryable<CustomerViewJoin>().ToList<CustomerViewJoin>()));
            }
            else if (strOperation == "del")
            {
                //var query = Query.EQ("_id", forms.Get("EmpId").ToString());
                //collectionEmployee.Remove(query);
                //strResponse = "Employee record successfully removed";
                //context.Response.Write(strResponse);

                customer.CustomerId = Convert.ToInt64(forms.Get("CustomerId").ToString());
                var result = customerHandler.hnDeleteCustomer(customer);
                if (result == "success")
                {
                    strResponse = "Customer Deleted Sucessfully";
                    context.Response.Write(strResponse);
                }
                else
                {
                    strResponse = "Customer Deletion Failed";
                    context.Response.Write(strResponse);
                }
            }
            else
            {
                string strOut = string.Empty;
                AddEdit(forms, updatecustomer, out strOut);
                context.Response.Write(strOut);

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        //private void AddEdit(NameValueCollection forms, CustomerViewJoin collectionEmployee, out string strResponse)
        private void AddEdit(NameValueCollection forms, string updatecustomer, out string strResponse)
        {
            string strOperation = forms.Get("oper");
            int strCustId = 0;
            if (strOperation == "edit")
            {
                strCustId = Convert.ToInt32(forms.Get("CustomerId").ToString());

            }
                //else if (strOperation == "add")
                //{
                //    // var result = collectionEmployee.AsQueryable<CustomerViewJoin>().Select(c => c.CustomerId).Max();
                //    // strCustId = (Convert.ToInt32(result) + 1);
                //}
                string strFirstName = forms.Get("FirstName").ToString();
                string strLastName = forms.Get("LastName").ToString();
                //int strCountry = Convert.ToInt16(forms.Get("Country").ToString());
                //int strState = Convert.ToInt32(forms.Get("State").ToString());
                string strCity = forms.Get("City").ToString();
                string strAddress1 = forms.Get("Address1").ToString();
                string strAddress2 = forms.Get("Address2").ToString();
                int strPostalCode = Convert.ToInt32(forms.Get("PostalCode").ToString());
                string strphone = forms.Get("Phone").ToString();
                string stremail = forms.Get("Email").ToString();
                long strReferal = Convert.ToInt64(forms.Get("Referal").ToString());
                string strCardNumber = forms.Get("CardNumber").ToString();

                HanayenEntity.Customers objcust = new HanayenEntity.Customers();
                hnCustomerHandler custhandler = new hnCustomerHandler();
                objcust.CustomerId = strCustId;
                objcust.FirstName = strFirstName;
                objcust.LastName = strLastName;
                //objcust.CountryId = strCountry;
                //objcust.StateId = strState;
                objcust.City = strCity;
                objcust.Address1 = strAddress1;
                objcust.Address2 = strAddress2;
                objcust.PostalCode = strPostalCode;
                objcust.Phone = strphone;


                objcust.Email = stremail;
                objcust.Referal = strReferal;
                objcust.CardNumber = strCardNumber;
                //collectionEmployee.save(objEmp);
                custhandler.hnUpdateCustomer(objcust);
                strResponse = "Employee record successfully updated";
            
        }

    }
}