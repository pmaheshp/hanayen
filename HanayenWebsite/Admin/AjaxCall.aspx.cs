﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HanayenBussiness;

using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using HanayenEntity;
namespace HanayenWebsite.Admin
{
    public partial class AjaxCall : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetCategory()
        {
            hnProductHandler productHandler = new hnProductHandler();
            JavaScriptSerializer jscript = new JavaScriptSerializer();
            
          return jscript.Serialize( productHandler.hnGetCategory());
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetSizeStanderd()
        {
            hnProductHandler productHandler = new hnProductHandler();
            JavaScriptSerializer jscript = new JavaScriptSerializer();
            return jscript.Serialize(GetSizeData());
           
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetSize()
        {
            hnProductHandler productHandler = new hnProductHandler();
            JavaScriptSerializer jscript = new JavaScriptSerializer();
            return jscript.Serialize(productHandler.hnGetSize());

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetDesign()
        {
            hnProductHandler productHandler = new hnProductHandler();
            JavaScriptSerializer jscript = new JavaScriptSerializer();
            return jscript.Serialize(productHandler.hnGetDesign());
        }
        [WebMethod]
        public static string GetLastProductCode()
        {
            hnProductHandler productHandler = new hnProductHandler();
            string str = productHandler.hnGetLastProductCode();
            
            return str;
        }
        //........................Private Methods..................
        private static List<SizeStanderd> GetSizeData()
        {
            hnProductHandler productHandler = new hnProductHandler();
            SizeStanderd size = new SizeStanderd();
            //List<Size> standerd = productHandler.hnGetSize();
            //List<SizeStanderd> stndList = new List<SizeStanderd>();
            //foreach (Size st in standerd)
            //{
            //    SizeStanderd sizeStanderd = new SizeStanderd();
            //    sizeStanderd.SizeId = (int)st.SizID;
            //    sizeStanderd.SizeName = st.SizeName;
            //    stndList.Add(sizeStanderd);
            //}
            return null;
        }
        

    }
    public class SizeStanderd
    {
        public int SizeId { set; get; }
        public string SizeName { set; get; }
        public string SizeValue { set; get; }
        public int SizeDetId { set; get; }
    }
}