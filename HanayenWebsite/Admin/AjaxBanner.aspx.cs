﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using HanayenEntity;
using HanayenBussiness;
using HanayenWebsite.Admin;
using HanayenWebsite.Account;

namespace HanayenWebsite.Admin
{
    public partial class AjaxBanner : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string AddBanner(String banneritems)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            JBanner ban = new JBanner();
            ban = jScript.Deserialize<JBanner>(banneritems);
            hnBannerHandler bannerHandler = new hnBannerHandler();
            Banner banner = new Banner();
            banner.BannerLink = ban.Link;
            banner.BannerImage = ban.Image;
            banner.BannerIsLink = ban.islink;
            banner.BannerType = ban.Type;
            banner.BannerName= ban.Name;
            return jScript.Serialize(bannerHandler.hnInsertBanner(banner));
        }

    }
    class JBanner
    {
        public string Name { get; set; }
        public string Link { get; set; }
        public string islink { get; set; }
        public string Type { get; set; }
        public string Image { get; set; }
    
    }
    }
