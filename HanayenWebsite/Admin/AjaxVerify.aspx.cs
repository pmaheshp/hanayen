﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using HanayenBussiness;
using HanayenEntity;

namespace HanayenWebsite.Admin
{
    public partial class AjaxVerify : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string UpdateIsActivate(String loginItem)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            hnLoginHandler loginitemHandler = new hnLoginHandler();
            HanayenEntity.Login login = new HanayenEntity.Login();
            StatusUpdation statustUp = new StatusUpdation();
            statustUp = jScript.Deserialize<StatusUpdation>(loginItem);
            login.Email =statustUp.email;
            login.Status = statustUp.status;
            login.IsActivate = statustUp.isactivate;
            return jScript.Serialize(loginitemHandler.hnActivateUser(login));
        }
        public class StatusUpdation
        {
            public string email { get; set; }
            public string status { get; set; }
            public string isactivate { get; set; }

        }
    }
}