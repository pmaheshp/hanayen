﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="HotSale.aspx.cs" Inherits="HanayenWebsite.Admin.HotSale" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../JQGridReq/jquery-1.9.0.min.js" type="text/javascript"></script>
    <link href="../JQGridReq/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="../JQGridReq/jquery.jqGrid.js" type="text/javascript"></script>
    <link href="../JQGridReq/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="../JQGridReq/grid.locale-en.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
  
    <script type="text/javascript">
        jQuery("#jQGridDemo").jqGrid({
            url: 'http://localhost:18994/JQGridHandler.ashx',
            datatype: "json",
            colNames: ['HotSaleId', 'ProductName', 'ProductCode', 'HotSaleDate'],
            colModel: [
                        { name: 'HotSaleId', index: 'HotSaleId', width: 80 },
                        { name: 'ProductName', index: 'ProductName', width: 150, stype: 'text', editable: true},
   		                { name: 'ProductCode', index: 'ProductCode', width: 150, stype: 'text', sortable: true, editable: true },
   		                { name: 'HotSaleDate', index: 'HotSaleDate', width: 150, editable: true, edittype: 'text'}
   		                
   	                  ],
            rowNum: 30,
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'HotSaleId',
            viewrecords: true,
            autowidth: true,
            width: '100%',
            height: '100%',
            sortorder: 'desc',
            caption: "Hot Sale Details",

            editurl: 'http://localhost:18994/JQGridHandler.ashx'
        });
        
        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       edit: true,
                       add: true,
                       del: true,
                       search: true,
                       refresh: true,
                       searchtext: "Search",
                       addtext: "Add",
                       edittext: "Edit",
                       deltext: "Delete",
                       refreshtext: "Refresh"
                   },
                   {   //EDIT
                       //                       height: 300,
                       //                       width: 400,
                       //                       top: 50,
                       //                       left: 100,
                       //                       dataheight: 280,
                       closeOnEscape: true, //Closes the popup on pressing escape key
                       reloadAfterSubmit: true,
                       drag: true,
                       afterSubmit: function (response, postdata) {
                           if (response.responseText == "") {

                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid'); //Reloads the grid after edit
                               return [true, '']
                           }
                           else {
                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid'); //Reloads the grid after edit
                               return [false, response.responseText]//Captures and displays the response text on th Edit window
                           }
                       },
                       editData: {
                           EmpId: function () {
                               var sel_id = $('#jQGridDemo').jqGrid('getGridParam', 'selrow');
                               var value = $('#jQGridDemo').jqGrid('getCell', sel_id, 'HotSaleId');
                               return value;
                           }
                       }
                   },
                   {
                       closeAfterAdd: true, //Closes the add window after add
                       afterSubmit: function (response, postdata) {
                           if (response.responseText == "") {

                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')//Reloads the grid after Add
                               return [true, '']
                           }
                           else {
                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')//Reloads the grid after Add
                               return [false, response.responseText]
                           }
                       }
                   },
                   {   //DELETE
                       closeOnEscape: true,
                       closeAfterDelete: true,
                       reloadAfterSubmit: true,
                       closeOnEscape: true,
                       drag: true,
                       afterSubmit: function (response, postdata) {
                           if (response.responseText == "") {

                               $("#jQGridDemo").trigger("reloadGrid", [{ current: true}]);
                               return [false, response.responseText]
                           }
                           else {
                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                               return [true, response.responseText]
                           }
                       },
                       delData: {
                           HotSaleId: function () {
                               var sel_id = $('#jQGridDemo').jqGrid('getGridParam', 'selrow');
                               var value = $('#jQGridDemo').jqGrid('getCell', sel_id, 'HotSaleId');
                               HotSaleId = value;
                               return HotSaleId;
                           }
                       }
                   },
                   {//SEARCH
                       closeOnEscape: true

                   }
            );
              
    </script>



</asp:Content>
