﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using HanayenBussiness;
using System.Web.Script.Serialization;
using HanayenEntity;

namespace HanayenWebsite.Admin
{
    public partial class AjaxHotsale : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //[WebMethod]
        //[ScriptMethod(UseHttpGet = true)]
        //public static string GetProduct()
        //{
        //    hnProductHandler productHandler = new hnProductHandler();
        //    JavaScriptSerializer jscript = new JavaScriptSerializer();
        //    return jscript.Serialize(productHandler.hnGetProduct());
        //}
        // Check product code is valid or not

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string CheckProductCodeValid(string productcode)
        {
             JavaScriptSerializer jScript = new JavaScriptSerializer();
             hnProductHandler productHandler = new hnProductHandler();
             HanayenEntity.Products product = new HanayenEntity.Products();
             product= productHandler.CheckProductCode(productcode);
             if (product != null)
             {
                 string sucess = product.ProductId.ToString();
                 return sucess;
                //return "false";
             }
             return "false";
             
             
        }
    
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static void InsertHotsale(string hotsaleData)
        {

            JavaScriptSerializer jScript = new JavaScriptSerializer();
            Hotsaleinsert sale = new Hotsaleinsert();
            HotSale hotsale = new HotSale();
            sale = jScript.Deserialize<Hotsaleinsert>(hotsaleData);
            HanayenEntity.HotSale hot = new HanayenEntity.HotSale();
            hot.ProductId= long.Parse(sale.productid);
            hot.IsHotSale = sale.ishotsale;
            hot.HotSaleDate = System.DateTime.Now;
            hnHotsaleHandler hotsaleHandler = new hnHotsaleHandler();
            string success = hotsaleHandler.hnAddHotsale(hot);
        }
        
        class Hotsaleinsert
        {
            public string productid { get; set; }
            public string ishotsale { get; set; }

        }
    }
}