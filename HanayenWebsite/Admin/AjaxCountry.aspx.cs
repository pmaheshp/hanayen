﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using HanayenBussiness;
using HanayenEntity;

namespace HanayenWebsite.Admin
{
    public partial class AjaxCountry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
         [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string AddCountry(String countryitems)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            JCountry country = new JCountry();
            country = jScript.Deserialize<JCountry>(countryitems);
            hnCountryHandler countryhandler = new hnCountryHandler();
            Country cntry = new Country();
            cntry.CountryName = country.CountryName;

            return jScript.Serialize(countryhandler.hnInsertCountry(cntry));
        }
      class JCountry
        {
            public string CountryName { get; set; }
        }
    }
    }
