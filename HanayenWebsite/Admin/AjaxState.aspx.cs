﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using HanayenBussiness;
using System.Web.Script.Serialization;
using HanayenEntity;

namespace HanayenWebsite.Admin
{
    public partial class AjaxState : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetCountry()
        {
            hnStateHandler stateHandler = new hnStateHandler();
            JavaScriptSerializer jscript = new JavaScriptSerializer();
            return jscript.Serialize(stateHandler.hnGetCountry());
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string AddState(String stateitems)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            JState state = new JState();
            state = jScript.Deserialize<JState>(stateitems);
            hnStateHandler statehandler = new hnStateHandler();
            State st = new State();
            st.CountryId = int.Parse(state.CountryId);
            st.StateName = state.StateName;
            return jScript.Serialize(statehandler.hnInsertState(st));
        }
        class JState
        {
            public string CountryId { get; set; }
            public string StateName { get; set; }
        }
    }
}