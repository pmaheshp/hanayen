﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Admin.Master" CodeBehind="AddHotSale.aspx.cs" Inherits="HanayenWebsite.Admin.AddHotSale" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../Shopping/Style/Customer.css" rel="stylesheet" type="text/css" />
    <script src="js/Banners.js" type="text/javascript"></script>
    <link href="Styles/form.css" rel="Stylesheet" type="text/css" />
    <link rel="Stylesheet" href="Styles/Category.css" type="text/css" />
    <script src="js/JSHotSale.js" type="text/javascript"></script>
    <script type="text/javascript">

function selectHotsaleproduct() {
    
     $.ajax({
        type:"GET",
        url:"AjaxHotsale.aspx/GetHotsaleProduct",
        data: "{}",
        dataType:"JSON",
        async: false,
        contentType:"application/json;charset=utf-8",
        success:function(data){
        var jData=JSON.parse(data.d);

        },
    });
}
 function addHotsale() {
// var dsData = $('#ContentPlaceHolder1_DDLProduct option:selected').val();
 var dsData=$('#txtproduct').val(); 
 var hotsale="";
 if ($('#chkishotsale').is(":checked"))
 {
    hotsale = "true";
 }
 else
 {
    hotsale="false";
 }
 var str = '{productid:"' + dsData + '",ishotsale:"' + hotsale + '"}';
 $.ajax({
 type: "GET",
 url: "AjaxHotsale.aspx/InsertHotsale",
 data: { hotsaleData: JSON.stringify(str) },
 dataType: "JSON",
 async: false,
 contentType: "application/json;charset=utf-8",
 success: function (data) {
 var jData = JSON.parse(data.d);
 var divtable = document.getElementById('lblmsg');
 divtable.innerHTML ='Sucessfully Inserted';
 }
 });
 }
function ProductCodeChange() {
var productcode = $("#txtProductCode").val();
var str = "{productcode:"+productcode+"}";
 $.ajax
 ({
     type: "GET",
     url: "AjaxHotsale.aspx/CheckProductCodeValid",
     data: { ProductCode: JSON.stringify(productcode)},
     dataType: "JSON",
     async: false,
     contentType: "application/json;charset=utf-8",
     success: function (data) {
     var jData = JSON.parse(data.d);
     
if(jData!==false)
{
var divtable = document.getElementById('txtproductid');
divtable.innerHTML=divtable.innerHTML+'<div class="productid"><input type="text" class="txtprdct" id="txtproduct" value="'+jData+'"></div>'

var divtable = document.getElementById('productcodemsg');
divtable.innerHTML ='Product Exit';
}
else if(jData==false)
{
  var divtable = document.getElementById('productcodemsg');
  divtable.innerHTML = 'Product Not Exit';
} 
}
});
}
</script>
</asp:Content> 
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="sidebar">
        <ul class="sideNav">
            <li>
                <asp:LinkButton ID="lbViewBanners" runat="server" Width="94px" 
                    >View HotSale</asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="lbAddBanners" runat="server" Width="94px" Height="35px" 
                    >Add HotSale</asp:LinkButton></li>
        </ul>
    </div>
    
            
    <div class="register">
    <form id="AddHotSale">
        
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
        <fieldset class="row1">
         <legend>Add HotSale Details</legend>
        <p>
                <%--<label>
                    Product Name</label>
                <select id="DDLProduct" runat="server" class="select" onclick="return DDLProduct_onclick()">
                </select>--%>
                 <label>
                    Product Code</label>
                <input type="text" id="txtProductCode" onchange="ProductCodeChange()"/>
        </p>
        <div class="productcodemsg">
        
        <div id="productcodemsg" class="productcodemsg"></div>
        
        </div>
        </fieldset>
        <div class="productid">
        
        <div id="txtproductid" class="productid" style="display: none;"></div>
        
        </div>
        <fieldset>
        <p>
        <input type="checkbox" id="chkishotsale" name="ishotsale" value="IsHotSale" style="font-size:15px; margin-left:16%">IsHotSale<br>
        </p>        
        </fieldset>
        <p>
        <div class="btnSubmit" ID="btnhotsale" onclick="addHotsale()">SUBMIT</div>
        </p>
        <p>
        <div class="insertmsg">
        <div id="lblmsg" class="insertmsg"></div>
        </div>
        </p>
        <%--
        <p >
        <asp:Button class="button" runat="server" Text="Save HotSale" ID="btnhotsale" OnClick="btnhotsale_Click" OnClientClick="btnhotsale_OnClientClick()()"></asp:Button>
           <%-- <asp:Button  runat="server" ID="btnhotsale"  Text="Button"  OnClientClick="btnhotsale_OnClientClick()" />--%>
        <%--</p>  --%>    
        
      

        </asp:View>
</asp:MultiView>
            </form>
               
           
        </div>
        
        <asp:HiddenField ID="HFProduct" runat="server" />
            
   </asp:Content>

