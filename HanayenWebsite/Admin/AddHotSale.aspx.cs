﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HanayenEntity;
using HanayenBussiness;


namespace HanayenWebsite.Admin
{
    public partial class AddHotSale : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MultiView1.SetActiveView(View1);
            }
            
        }
        protected void btnhotsale_Click(object sender, EventArgs e)
        {
            HanayenEntity.HotSale hotsale = new HanayenEntity.HotSale();
            hnHotsaleHandler hotsaleHandler = new hnHotsaleHandler();
            HanayenEntity.Products product=new HanayenEntity.Products();
            
            hnProductHandler producthandler=new hnProductHandler();
            product.ProductCode=HFProduct.Value;
            string productcode=product.ProductCode;
            hotsale.HotSaleDate= System.DateTime.Now;
            //hotsale.ProductId = long.Parse(HFProduct.Value);
            hotsale.ProductId =long.Parse(producthandler.CheckProductCode(productcode).ToString());
            string result=hotsaleHandler.hnAddHotsale(hotsale);
            

            
       }
    }
}