﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HanayenEntity;
using HanayenBussiness;
using System.IO;
using System.Security.AccessControl;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
namespace HanayenWebsite.Admin
{
    public partial class Products : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //hnProductHandler productHandler = new hnProductHandler();
            //cblSize.DataValueField = "SizeDetId";
            //cblSize.DataTextField = "SizeValue";
            //List<SizeDesc> descList = productHandler.hnGetSize()[0].SizeDesc;
            //cblSize.DataSource = descList;
            //cblSize.DataBind();
            if (!IsPostBack)
            {
              
               
            }
            
        }
        protected void iBtnCategory_Click(object sender, EventArgs e)
        {
           // Multiview1.SetActiveView(categoryView);
            hnProductHandler productHandler = new hnProductHandler();
            gvCategory.DataSource = productHandler.hnGetCategory();
            gvCategory.DataBind();
        }
        protected void btnNewCategory_Click(object sender, EventArgs e)
        {
            Category categoryEntity = new Category();
            categoryEntity.CategoryName = txtNewCategory.Value;
            hnProductHandler ProductHandler = new hnProductHandler();
            string result=ProductHandler.hnAddCategory(categoryEntity);
            if (result == "Success")
            {

            }
        }
        protected void btnNewSize_Click(object sender, EventArgs e)
        {
            try
            {
                HanayenEntity.Size sizeEntity = new HanayenEntity.Size();
                sizeEntity.SizeDesc= new List<SizeDesc>() ;
                SizeDesc desc = new SizeDesc();
                desc.SizeValue = txtNewSize.Value;
                sizeEntity.SizeDesc.Add(desc);
                sizeEntity.SizID =(SIZE) Enum.Parse(typeof(SIZE),HFStanderd.Value);
                hnProductHandler productHandler = new hnProductHandler();
                productHandler.hnAddSize(sizeEntity);
            }
            catch (Exception ex)
            {

            }
        }
        protected void btnDesign_Click(object sender, EventArgs e)
        {
            try
            {
                Design designEntity = new Design();
                designEntity.DesignName = txtNweDesign.Value;
                hnProductHandler productHandler = new hnProductHandler();
                productHandler.hnAddDesign(designEntity);
            }
            catch (Exception ex)
            {
            }

        }
        protected void btnProduct_Click(object sender, EventArgs e)
        {
            try
            {
                HanayenEntity.Products prodEntity = new HanayenEntity.Products();
                hnProductHandler productHandler = new hnProductHandler();
                Category category = new Category();
                Design design = new Design();
               
                List<SizeDesc> sizeDesc = new List<SizeDesc>();
                if (HFCategory.Value != "" && HFDesign.Value != "" && HFSize.Value != "" && txtProductCode.Value != "")
                {
                    long productID =long.Parse( productHandler.hnGetLastProductCode())+1;
                        prodEntity.ProductId = productID;
                        category.CategoryId = int.Parse(HFCategory.Value);
                        design.DesignId = int.Parse(HFDesign.Value);
                        prodEntity.ProductName = txtProduct.Value;
                        prodEntity.ProductCode = txtProductCode.Value;
                        prodEntity.SKU = txtsku.Value;
                        prodEntity.Price = Double.Parse(txtPrice.Value);
                        prodEntity.Discount = Double.Parse(txtDiscount.Value);
                        prodEntity.Quanitity = int.Parse(txtDiscount.Value);
                        prodEntity.ModelHeight = txtHeight.Value;
                        prodEntity.Color = txtColor.Value;
                        prodEntity.NeckType = txtNeck.Value;
                        prodEntity.Title = txtTitle.Value;
                        prodEntity.Material = txtMaterial.Value;
                        prodEntity.Instruction = txtInstruction.Value;
                        prodEntity.Measurement = txtMeasurement.Value;
                        prodEntity.Description = txtDetails.Value;
                        prodEntity.ProductDate = System.DateTime.Now.ToString();
                        prodEntity.CategoryId = category;
                        prodEntity.ProductSize = GetSize(productID);
                        prodEntity.DesignId = design;
                        productHandler.hnAddProduct(prodEntity);
                        if (UploadFiles(productID))
                        {
                        }
                    
                }
}
            catch (Exception ex)
            {
            }
        }
        private void CreateFolder(string path)
        {
            Directory.CreateDirectory(path);
            FileSystemAccessRule everyOne = new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, AccessControlType.Allow);
            DirectorySecurity dirSecurity = new DirectorySecurity(path, AccessControlSections.Group);
            dirSecurity.AddAccessRule(everyOne);
            Directory.SetAccessControl(path, dirSecurity);
        }
        //private long UploadFiles()
        //{
        //    //long imgId = 0;
            //string path = System.Configuration.ConfigurationManager.AppSettings["ImagePath"].ToString();
            //string productCode = txtProductCode.Value;
            //string filePath = Request.MapPath(path);
            
            //List<SubImages> subList = new List<SubImages>();
            //if (Directory.Exists(filePath))
            //{
            //    string ext1 = Path.GetExtension(fuFrontView.PostedFile.FileName);
            //    string ext2 = Path.GetExtension(fuBackView.PostedFile.FileName);
            //    string ext3 = Path.GetExtension(fuSideView.PostedFile.FileName);
            //    string fullPath = filePath +"\\"+ productCode;
            //    CreateFolder(fullPath);
            //    int iCount = 0;
            //    if (fuFrontView.HasFile)
            //    {
            //        fuFrontView.SaveAs(fullPath);
            //        SubImages sub = new SubImages();
            //        sub.Path = fullPath;
            //        subList.Add(sub); 
                    
            //    }
            //    if (fuBackView.HasFile)
            //    {
            //        fuBackView.SaveAs(fullPath);
            //        SubImages sub = new SubImages();
            //        sub.Path = fullPath ;
            //        subList.Add(sub);
            //    }
            //    if (fuSideView.HasFile)
            //    {
            //        fuSideView.SaveAs(fullPath);
            //        SubImages sub = new SubImages();
            //        sub.Path = fullPath;
            //        subList.Add(sub);
            //    }
            //    if (iCount > 0)
            //    {
            //        hnProductHandler productHandler = new hnProductHandler();
            //        Images img=new Images();
            //        img.SubImages=subList;
            //        imgId= productHandler.hnAddImage(img);
            //    }
              
                
            //}
            //else
            //{
            //    return 0;
            //}
            //return imgId;
            
        //}
        private List<xRefProductSize>  GetSize(long productId)
        {
            List<xRefProductSize> sizeDesc = new List<xRefProductSize>();
                string[] size = HFSize.Value.Split(",".ToCharArray());
                if (size != null)
                {
                    foreach (string s in size)
                    {
                        xRefProductSize desc = new xRefProductSize();
                        desc.SizeDescId = long.Parse(s);
                        desc.ProductId = productId;
                        sizeDesc.Add(desc);
                    }
                }
                else
                {
                    return null;
                }
                return sizeDesc;
        
        }



        private bool UploadFiles(long productId)
        {
            //"ctl00$MainContent$file_upload"
            bool flag = false;
            HttpFileCollection fileCollection = Request.Files;
            string[] str = fileCollection.AllKeys;
            string fileName = "", file = "";
            int type = 0;
            string Largepath = System.Configuration.ConfigurationManager.AppSettings["LargeImage"].ToString();
            string Smallpath = System.Configuration.ConfigurationManager.AppSettings["SmallImage"].ToString();
            string Thumbpath = System.Configuration.ConfigurationManager.AppSettings["ThumbImage"].ToString();
            int j = 0;
            int s = 0, l = 0, t = 0;
            
            for (int i = 0; i < fileCollection.Count; i++)
            {
                HttpPostedFile uploadfile = fileCollection[i];
                string ext = Path.GetExtension(uploadfile.FileName);
                if (fileCollection.Keys[i] == "ctl00$ContentPlaceHolder1$file_upload")
                {
                    l++;
                    fileName = "Pic" + l + "_" + productId + ext;
                    file = Largepath + "/" + fileName;
                    type = 1;

                    if (File.Exists(file))
                    {
                        File.Delete(file);
                    }
                    
                        if (uploadfile.ContentLength > 0)
                        {
                            uploadfile.SaveAs(Server.MapPath(file));
                            string fileNam = Path.GetFileName(uploadfile.FileName);
                            string filePath = Server.MapPath(file);
                            try
                            {
                                s++;
                                t++;
                                hnProductHandler productSp = new hnProductHandler();
                                HanayenEntity.Images imageFiles = new Images();
                                imageFiles.File = fileName;

                                imageFiles.ProductID = productId;

                                productSp.hnAddImage(imageFiles);
                                getResizedImage(filePath, 277, 400, Smallpath + "/" + fileName);
                                getResizedImage(filePath, 76, 109, Thumbpath + "/" + fileName);
                                flag = true;

                            }
                            catch (Exception ex)
                            {
                                flag =false;
                            }


                            //lblMessage.Text += file + "Saved Successfully<br>";


                        }
                    }
                
                
               
            }
            return flag;
        }
        string getResizedImage(String path, int width, int height, string destinationFile)
        {
            Bitmap imgIn = new Bitmap(path);
            double y = imgIn.Height;
            double x = imgIn.Width;

            double factor = 1;
            if (width > 0)
            {
                factor = width / x;
            }
            else if (height > 0)
            {
                factor = height / y;
            }
            System.IO.MemoryStream outStream = new System.IO.MemoryStream();
            Bitmap imgOut = new Bitmap((int)(x * factor), (int)(y * factor));

            // Set DPI of image (xDpi, yDpi)
            imgOut.SetResolution(72, 72);

            Graphics g = Graphics.FromImage(imgOut);
            g.Clear(Color.White);
            g.DrawImage(imgIn, new Rectangle(0, 0, (int)(factor * x), (int)(factor * y)),
              new Rectangle(0, 0, (int)x, (int)y), GraphicsUnit.Pixel);

            imgOut.Save(Server.MapPath(destinationFile), getImageFormat(path));
            return destinationFile;
        }
        string getContentType(String path)
        {
            switch (Path.GetExtension(path))
            {
                case ".bmp": return "Image/bmp";
                case ".gif": return "Image/gif";
                case ".jpg": return "Image/jpeg";
                case ".png": return "Image/png";
                default: break;
            }
            return "";
        }
        ImageFormat getImageFormat(String path)
        {
            switch (Path.GetExtension(path))
            {
                case ".bmp": return ImageFormat.Bmp;
                case ".gif": return ImageFormat.Gif;
                case ".jpg": return ImageFormat.Jpeg;
                case ".png": return ImageFormat.Png;
                default: break;
            }
            return ImageFormat.Jpeg;
        }
        }

    }