﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AddState.aspx.cs" Inherits="HanayenWebsite.Admin.AddState" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Styles/State.css" rel="stylesheet" type="text/css" />
    <script src="js/JSState.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="sidebar" style="top:110px;">
        <ul class="sideNav">
            <li>
                <asp:LinkButton ID="lbViewBanners" runat="server" Width="94px" 
                    >View Country</asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="lbAddBanners" runat="server" Width="94px" Height="35px" 
                    >Add Country</asp:LinkButton></li>
        </ul>
    </div>
    
    <div class="register" style="height:200px">
    <form id="AddHotSale">
        
    
        <fieldset class="row2">
         <legend>Add State</legend>
         <label>
                    Country Name</label>
                <select id="DDLCountry" runat="server" class="select">
                </select>
        <p>
                
                <label>
                    State Name</label>
                <input type="text" id="txtStatename"/>
        </p>
        <div class="btnSubmit" id="btnhotsale" onclick="insertStateDetails()">SUBMIT</div>
        </fieldset>
        
        <div class="insertmsg">
        <div id="lblmsg" class="insertmsg"></div>
        </div>
       
       
        
        
      


            </form>
               
           
        </div>     
</asp:Content>
