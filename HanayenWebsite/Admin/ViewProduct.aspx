﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ViewProduct.aspx.cs" Inherits="HanayenWebsite.Admin.ViewProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <script src="../JQGridReq/jquery-1.9.0.min.js" type="text/javascript"></script>
    <link href="../JQGridReq/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="../JQGridReq/jquery.jqGrid.js" type="text/javascript"></script>
    <link href="../JQGridReq/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="../JQGridReq/grid.locale-en.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
 
    <script type="text/javascript">
        jQuery("#jQGridDemo").jqGrid({
            url: 'http://localhost:18994/JQViewProductHandler.ashx',
            datatype: "json",
            colNames: ['ProductId', 'ProductName', 'ProductCode', 'Title', 'Price', 'Discount', 'Description', 'Quantity', 'SKU', 'Color', 'Instruction', 'NeckType', 'ModelMeasurement', 'ModelHeight', 'ProductMaterial', 'ProductDate', 'Category', 'DesignName'],
            colModel: [
                        { name: 'ProductId', index: 'ProductId', width: 80 },
                        { name: 'ProductName', index: 'ProductName', width: 150, stype: 'text' },
   		                { name: 'ProductCode', index: 'ProductCode', width: 150, stype: 'text', sortable: true, editable: true },
   		                { name: 'Title', index: 'Title', width: 150,stype: 'text',editable: true },
                        { name: 'Price', index: 'Price', width: 150,stype: 'text',editable: true },
                        { name: 'Discount', index: 'Discount', width: 150,stype: 'text',editable: true },
                        { name: 'Description', index:'Description', width: 150,stype: 'text',editable: true },
                        { name: 'Quantity', index: 'Quantity', width: 150, stype: 'text', sortable: true, editable: true },
                        { name: 'SKU', index: 'SKU', width: 150, editable: true },
                        { name: 'Color', index: 'Color', width: 150, editable: true },
                        { name: 'Instruction', index: 'Instruction', width: 150, editable: true },
                        { name: 'NeckType', index: 'NeckType', width: 150, editable: true },
                        { name: 'ModelMeasurement', index: 'ModelMeasurement', width: 150, stype: 'text', editable: true },
                        { name: 'ModelHeight', index: 'ModelHeight', width: 150,stype: 'text', editable: true },
                        { name: 'ProductMaterial', index: 'ProductMaterial', width: 150,stype: 'text', editable: true },
                        { name: 'ProductDate', index: 'ProductDate', width: 150,stype: 'text', editable: true },
                        { name: 'Category', index: 'Category', width: 150,stype: 'text',editable: true },
                        { name: 'DesignName', index: 'DesignName', width: 150,stype: 'text', editable: true }

              ],
            rowNum: 10,
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'ProductId',
            autowidth: true,
            width: '100%',
            height: '100%',
            viewrecords: true,
            sortorder: 'desc',
            caption: "Product Details",

            editurl: 'http://localhost:18994/JQViewProductHandler.ashx'
        });

        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       edit: true,
                       add: true,
                       del: true,
                       search: true,
                       searchtext: "Search",
                       addtext: "Add",
                       edittext: "Edit",
                       deltext: "Delete"
                   },
                   {   //EDIT
                       height: 300,
                       width: 400,
                       top: 50,
                       left: 100,
                       dataheight: 280,
                       closeOnEscape: true, //Closes the popup on pressing escape key
                       reloadAfterSubmit: true,
                       drag: true,
                       afterSubmit: function (response, postdata) {
                           if (response.responseText == "") {

                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid'); //Reloads the grid after edit
                               return [true, '']
                           }
                           else {
                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid'); //Reloads the grid after edit
                               return [false, response.responseText]//Captures and displays the response text on th Edit window
                           }
                       },
                       editData: {
                           EmpId: function () {
                               var sel_id = $('#jQGridDemo').jqGrid('getGridParam', 'selrow');
                               var value = $('#jQGridDemo').jqGrid('getCell', sel_id, 'ProductId');
                               return value;
                           }
                       }
                   },
                   {
                       closeAfterAdd: true, //Closes the add window after add
                       afterSubmit: function (response, postdata) {
                           if (response.responseText == "") {

                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')//Reloads the grid after Add
                               return [true, '']
                           }
                           else {
                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')//Reloads the grid after Add
                               return [false, response.responseText]
                           }
                       }
                   },
                   {   //DELETE
                       closeOnEscape: true,
                       closeAfterDelete: true,
                       reloadAfterSubmit: true,
                       closeOnEscape: true,
                       drag: true,
                       afterSubmit: function (response, postdata) {
                           if (response.responseText == "") {

                               $("#jQGridDemo").trigger("reloadGrid", [{ current: true}]);
                               return [false, response.responseText]
                           }
                           else {
                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                               return [true, response.responseText]
                           }
                       },
                       delData: {
                           ProductId: function () {
                               var sel_id = $('#jQGridDemo').jqGrid('getGridParam', 'selrow');
                               var value = $('#jQGridDemo').jqGrid('getCell', sel_id, 'ProductId');
                               ProductId = value;
                               return ProductId;
                           }
                       }
                   },
                   {//SEARCH
                       closeOnEscape: true

                   }
            );
              
    </script>
</asp:Content>
