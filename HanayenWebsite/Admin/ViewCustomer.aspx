﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ViewCustomer.aspx.cs" Inherits="HanayenWebsite.ViewCustomer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../JQGridReq/jquery-1.9.0.min.js" type="text/javascript"></script>
    <link href="../JQGridReq/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="../JQGridReq/jquery.jqGrid.js" type="text/javascript"></script>
    <link href="../JQGridReq/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="../JQGridReq/grid.locale-en.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
 
   <script type="text/javascript">
        jQuery("#jQGridDemo").jqGrid({
            url: 'http://localhost:18994/JQCustomerHandler.ashx',
            datatype: "json",
            colNames: ['CustomerId', 'FirstName', 'LastName', 'CountryName', 'StateName', 'City', 'Address1', 'Address2', 'PostalCode', 'Phone', 'Email', 'Referal', 'CardNumber'],
            colModel: [
                        { name: 'CustomerId', index: 'CustomerId', width: 80 },
                        { name: 'FirstName', index: 'FirstName', width: 150, stype: 'text', editable: true },
   		                { name: 'LastName', index: 'LastName', width: 150, stype: 'text', sortable: true, editable: true },
               		    { name: 'CountryName', index: 'CountryName', width: 150, stype: 'text'},
                        { name: 'StateName', index: 'StateName', width: 150, stype: 'text'},
                        
                        { name: 'City', index: 'City', width: 150, stype: 'text', editable: true },
                        { name: 'Address1', index: 'Address1', width: 150, stype: 'text', editable: true },
                        { name: 'Address2', index: 'Address2', width: 150, stype: 'text', sortable: true, editable: true },
                        { name: 'PostalCode', index: 'PostalCode', width: 150, editable: true },
                        { name: 'Phone', index: 'Phone', width: 150, editable: true },
                        { name: 'Email', index: 'Email', width: 150, editable: true },
                        { name: 'Referal', index: 'Referal', width: 150, editable: true },
                        { name: 'CardNumber', index: 'CardNumber', width: 150, stype: 'text', editable: true },
            ],
            rowNum: 10,
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'CustomerId',
            viewrecords: true,
            autowidth: true,
            width: '100%',
            height: '100%',
            sortorder: 'desc',
            caption: "Customer Details",

            editurl: 'http://localhost:18994/JQCustomerHandler.ashx'
        });

        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       edit: true,
                       add: true,
                       del: true,
                       search: true,
                       refresh: true,
                       searchtext: "Search",
                       refreshtext: "Refresh",
                       addtext: "Add",
                       edittext: "Edit",
                       deltext: "Delete"
                   },
                   {   //EDIT
//                       height: 300,
//                       width: 400,
//                       top: 50,
//                       left: 100,
//                       dataheight: 280,
                       closeOnEscape: true, //Closes the popup on pressing escape key
                       reloadAfterSubmit: true,
                       drag: true,
                       afterSubmit: function (response, postdata) {
                           if (response.responseText == "") {

                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid'); //Reloads the grid after edit
                               return [true, '']
                           }
                           else {
                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid'); //Reloads the grid after edit
                               return [false, response.responseText]//Captures and displays the response text on th Edit window
                           }
                       },
                       editData: {
                           CustomerId: function () {
                               var sel_id = $('#jQGridDemo').jqGrid('getGridParam', 'selrow');
                               var value = $('#jQGridDemo').jqGrid('getCell', sel_id, 'CustomerId');
                               alert(value);
                               return value;
                           }
                       }
                   },
                   {//ADD
                       closeAfterAdd: true, //Closes the add window after add
                       afterSubmit: function (response, postdata) {
                           if (response.responseText == "") {

                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')//Reloads the grid after Add
                               return [true, '']
                           }
                           else {
                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')//Reloads the grid after Add
                               return [false, response.responseText]
                           }
                       }
                   },
                   {   //DELETE
                       closeOnEscape: true,
                       closeAfterDelete: true,
                       reloadAfterSubmit: true,
                       closeOnEscape: true,
                       drag: true,
                       afterSubmit: function (response, postdata) {
                           if (response.responseText == "") {

                               $("#jQGridDemo").trigger("reloadGrid", [{ current: true}]);
                               return [false, response.responseText]
                           }
                           else {
                               $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                               return [true, response.responseText]
                           }
                       },
                       delData: {
                           CustomerId: function () {
                               var sel_id = $('#jQGridDemo').jqGrid('getGridParam', 'selrow');
                               var value = $('#jQGridDemo').jqGrid('getCell', sel_id, 'CustomerId');
                               CustomerId = value;
                               return CustomerId;

                           }
                       }
                   },
                   {//SEARCH
                       closeOnEscape: true

                   }
            );
              
    </script>

</asp:Content>
