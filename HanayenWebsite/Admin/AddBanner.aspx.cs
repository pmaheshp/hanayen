﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Security.AccessControl;
using HanayenEntity;
using HanayenBussiness;
using System.Drawing;
using System.Drawing.Imaging;


namespace HanayenWebsite.Admin
{
    public partial class AddBanner : System.Web.UI.Page
    {
       public int bannerid = 2;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MultiView1.SetActiveView(View1);
            }
        }
        private void CreateFolder(string path)
        {
            Directory.CreateDirectory(path);
            FileSystemAccessRule everyOne = new FileSystemAccessRule("BannerImages", FileSystemRights.FullControl, AccessControlType.Allow);
            DirectorySecurity dirSecurity = new DirectorySecurity(path, AccessControlSections.Group);
            dirSecurity.AddAccessRule(everyOne);
            Directory.SetAccessControl(path, dirSecurity);
            
        }

        protected void btnbannersubmit_Click(object sender, EventArgs e)
        {
            string Success = null;
          

            hnBannerHandler hnbanner = new hnBannerHandler();
            Banner banner = new Banner();
            banner.BannerLink = txtbannerlink.Text;
            banner.BannerName = txtbannername.Text;
            banner.BannerType = drpdownbannertype.SelectedItem.ToString();
            string Bannerpath = System.Configuration.ConfigurationManager.AppSettings["BannerImage"].ToString();
            //banner.BannerId = int.Parse(ViewState["BannerId"].ToString());            
            if (chkboxislink.Checked == true)
            {
                banner.BannerIsLink = "true";
            }
            else
            {
                banner.BannerIsLink = "false";

            }
            string banner_image = FileUpload1.FileName;
            if (banner_image == "")
            {
                banner.BannerImage = Image3.ImageUrl;
            }
            else
            {
                string path1 = Server.MapPath("~/") + "Admin\\BannerImages\\" + banner_image;
                FileUpload1.SaveAs(path1);
                int bannerid = 2;
                UploadFiles(bannerid);
                //getResizedImage(path1, 960, 430, Bannerpath + "/" + banner_image);
                banner.BannerImage = Bannerpath + "/" + banner_image;


            }
            if (btnbannersubmit.Text == "Update")
            {
                Success = hnbanner.hnUpdateBanner(banner);
                if (Success == "success")
                {
                    LblMessage.Text = "Succesfully Updated";
                    Image3.ImageUrl = banner.BannerImage;
                }
                else
                {
                    LblMessage.Text = "Not Updated";
                }
            }
            else
            {

                Success = hnbanner.hnInsertBanner(banner);
                if (Success == "success")
                {
                    LblMessage.Text = "Succesfully Inserted";

                }
                else
                {
                    LblMessage.Text = "Not Inserted";
                }
            }
            Grdbanner.DataBind();

        }
        protected void drpdwnbannerview_SelectedIndexChanged(object sender, EventArgs e)
        {
            Banner banner = new Banner();
            banner.BannerType = drpdwnbannerview.SelectedItem.ToString();
            string bannertype = banner.BannerType;
            hnBannerHandler hnbanner = new hnBannerHandler();
            List<Banner> bannerlist = new List<Banner>();
            bannerlist = hnbanner.GetBannerlist(bannertype);
            Grdbanner.DataSource = bannerlist;
            Grdbanner.DataBind();

        }
        private bool UploadFiles(int bannerid)
        {
            //"ctl00$MainContent$file_upload"
            bool flag = false;
            HttpFileCollection fileCollection = Request.Files;
            string[] str = fileCollection.AllKeys;
            string fileName = "", file = "";
            int type = 0;
            

            string Largepath = System.Configuration.ConfigurationManager.AppSettings["LargeImage"].ToString();
            //string Smallpath = System.Configuration.ConfigurationManager.AppSettings["SmallImage"].ToString();
            //string Thumbpath = System.Configuration.ConfigurationManager.AppSettings["ThumbImage"].ToString();
            int j = 0;
            int s = 0, l = 0, t = 0;

            for (int i = 0; i < fileCollection.Count; i++)
            {
                HttpPostedFile uploadfile = fileCollection[i];
                string ext = Path.GetExtension(uploadfile.FileName);


                //if (fileCollection.Keys[i] == "ctl00$ContentPlaceHolder1$file_upload")
                //{
                    l++;
                    fileName = "Pic" + l + "_" + bannerid + ext;
                    file = Largepath + "/" + fileName;
                    type = 1;

                    if (File.Exists(file))
                    {
                        File.Delete(file);
                    }

                    if (uploadfile.ContentLength > 0)
                    {
                        uploadfile.SaveAs(Server.MapPath(file));
                        string fileNam = Path.GetFileName(uploadfile.FileName);
                        string filePath = Server.MapPath(file);
                        try
                        {
                            s++;
                            t++;
                            hnProductHandler productSp = new hnProductHandler();
                            HanayenEntity.Banner bannerimage = new Banner();
                            bannerimage.BannerImage = fileName;
                            bannerimage.BannerId = bannerid;

                           // productSp.hnAddImage(imageFiles);
                            getResizedImage(filePath, 960, 430, Largepath + "/" + fileName);
                           // getResizedImage(filePath, 76, 109, Thumbpath + "/" + fileName);
                            flag = true;

                        }
                        catch (Exception ex)
                        {
                            flag = false;
                        }


                    }
                }



            //}
            return flag;
        }

        //private bool UploadFiles()
        //{
        //    //"ctl00$MainContent$file_upload"
        //    bool flag = false;
        //    HttpFileCollection fileCollection = Request.Files;
        //    string[] str = fileCollection.AllKeys;
        //    string fileName = "", file = "";
        //    int type = 0;
        //    string Largepath = System.Configuration.ConfigurationManager.AppSettings["LargeImage"].ToString();
        //    //string Smallpath = System.Configuration.ConfigurationManager.AppSettings["SmallImage"].ToString();
        //    //string Thumbpath = System.Configuration.ConfigurationManager.AppSettings["ThumbImage"].ToString();
        //    int j = 0;
        //    int s = 0, l = 0, t = 0;

        //    for (int i = 0; i < fileCollection.Count; i++)
        //    {
        //        HttpPostedFile uploadfile = fileCollection[i];
        //        string ext = Path.GetExtension(uploadfile.FileName);


        //        //if (fileCollection.Keys[i] == "ctl00$ContentPlaceHolder1$file_upload")
        //        //{
        //            l++;
        //          fileName = "Pic" + l + "_" + ext;
        //          file = Largepath + "/" + fileName;
        //            type = 1;

        //            if (File.Exists(file))
        //            {
        //                File.Delete(file);
        //            }

        //            if (uploadfile.ContentLength > 0)
        //            {
        //                uploadfile.SaveAs(Server.MapPath(file));
        //                string fileNam = Path.GetFileName(uploadfile.FileName);
        //                string filePath = Server.MapPath(file);
        //                try
        //                {
        //                    s++;
        //                    t++;
        //                    hnProductHandler productSp = new hnProductHandler();
        //                    HanayenEntity.Images imageFiles = new Images();
        //                    imageFiles.File = fileName;

        //                    //imageFiles.ProductID = productId;

        //                    //productSp.hnAddImage(imageFiles);
        //                    getResizedImage(filePath, 960, 430, Largepath + "/" + fileName);

        //                    flag = true;

        //                }
        //                catch (Exception ex)
        //                {
        //                    flag = false;
        //                }

        //            }
        //        }

        ////}
        //    return flag;
        //}


        protected void lbViewBanners_Click(object sender, EventArgs e)
        {
            MultiView1.SetActiveView(View2);
        }

        protected void lbAddBanners_Click(object sender, EventArgs e)
        {
            MultiView1.SetActiveView(View1);
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            Banner banner = new Banner();
            banner.BannerId = int.Parse(e.CommandArgument.ToString());
            hnBannerHandler hnbanner = new hnBannerHandler();
            hnbanner.hnDeleteBanner(banner);
            Grdbanner.DataBind();
        }
        string getResizedImage(String path, int width, int height, string destinationFile)
        {
            Bitmap imgIn = new Bitmap(path);
            double y = imgIn.Height;
            double x = imgIn.Width;

            double factor = 1;
            if (width > 0)
            {
                factor = width / x;
            }
            else if (height > 0)
            {
                factor = height / y;
            }
            System.IO.MemoryStream outStream = new System.IO.MemoryStream();
            Bitmap imgOut = new Bitmap((int)(x * factor), (int)(y * factor));

            // Set DPI of image (xDpi, yDpi)
            imgOut.SetResolution(72, 72);

            Graphics g = Graphics.FromImage(imgOut);
            g.Clear(Color.White);
            g.DrawImage(imgIn, new Rectangle(0, 0, (int)(factor * x), (int)(factor * y)),
              new Rectangle(0, 0, (int)x, (int)y), GraphicsUnit.Pixel);

            imgOut.Save(Server.MapPath(destinationFile), getImageFormat(path));
            return destinationFile;
        }
        ImageFormat getImageFormat(String path)
        {
            switch (Path.GetExtension(path))
            {
                case ".bmp": return ImageFormat.Bmp;
                case ".gif": return ImageFormat.Gif;
                case ".jpg": return ImageFormat.Jpeg;
                case ".png": return ImageFormat.Png;
                default: break;
            }
            return ImageFormat.Jpeg;
        }
        protected void LinkButton2_Command(object sender, CommandEventArgs e)
        {
            lblbannerimage.Text = "Change BannerImage";
            btnbannersubmit.Text = "Update";
            Image3.Visible = true;
            Banner banner = new Banner();
            banner.BannerId = int.Parse(e.CommandArgument.ToString());
            ViewState["BannerId"] = banner.BannerId;
            string Bannerid = banner.BannerId.ToString();
            hnBannerHandler hnbanner = new hnBannerHandler();
            List<Banner> bannerlist = new List<Banner>();
            bannerlist = hnbanner.SelectBannerlist(Bannerid);
            if (bannerlist.Count > 0)
            {
                MultiView1.SetActiveView(View1);
                foreach (Banner bannervalue in bannerlist)
                {
                    txtbannername.Text = bannervalue.BannerName;
                    txtbannerlink.Text = bannervalue.BannerLink;
                    Image3.ImageUrl = bannervalue.BannerImage;
                    drpdownbannertype.Text = bannervalue.BannerType;
                    if (bannervalue.BannerIsLink == "true")
                    {
                        chkboxislink.Checked = true;
                    }
                    else
                    {
                        chkboxislink.Checked = false;

                    }

                }

            }


        }
    }
}