﻿$(document).ready(function () {
    getCategory();
    getSize();
    getDesign();

  

});

function getCategory() {

    $.ajax({
       
        url: "AjaxCall.aspx/GetCategory",
        data: "{}",
        dataType: "json",
        contentType: "application/json;charset=utf-8",

        success: function (data) {
          
            var jsData = JSON.parse(data.d);
            $.each(jsData, function (key, value) {
               
                
                $('#ContentPlaceHolder1_DDLCategory')
    .append($("<option></option>").val(value.CategoryId).html(value.CategoryName));
            });
        }
    });

}
function getSizeStanderd() {
    $.ajax({
        type: "GET",
        url: "AjaxCall.aspx/GetSizeStanderd",
        data: "{}",
        dataType: "json",
        contentType: "application/json;charset=utf-8",

        success: function (data) {

            var jsData = JSON.parse(data.d);
            $.each(jsData, function (key, value) {


                $('#ContentPlaceHolder1_dbStanderd')
    .append($("<option></option>").val(value.SizeId).html(value.SizeName));
            });
        }
    });
}
function getSize() {
    $.ajax({
        type:"GET",
        url:"AjaxCall.aspx/GetSize",
        data:"{}",
                dataType:"json",
                contentType:"application/json;charset=utf-8",
        success:getSizeData,
        error:ajaxFailed,
        });

}
function getDesign()
{
    $.ajax({
        
        url:"AjaxCall.aspx/GetDesign",
        data:"{}",
        dataType:"json",
        contentType:"application/json;charset=utf-8",
        success:appendDesign,
        error:ajaxFailed,
        });
}
function getLastProductCode()
{
    $.ajax({
      
        url:"AjaxCall.aspx/GetLastProductCode",
        data:"{}",
        dataType:"text",
        
        success:function(data){
        
            var jsData=JSON.stringify(data.d);
            if(jsData='undefined'){jsData="1";}
            var dsData=$('#ContentPlaceHolder1_txtDesignId option:selected').text();
            var code=dsData+"_"+jsData;
            $('#ContentPlaceHolder1_txtProductCode').val(code);
          }
        });
}
/*.......Events...........*/
function dbStanderd_SelectedIndexChanged()
{
    alert("hi");
}

/*....Private Methods.......................*/
function getSizeData(result)
{
    var items = JSON.parse(result.d);
    var table = $('<table></table>');
    var count=0;
    var value=0;
    var a={};
    $(items).each(function () {
    if(value!=this.SizID)
    {
        value=this.SizID;
        table.append($('<tr></tr>').append($('<td style="font-size:15px; font-weight:bold"></td>').append(this.SizeName)));
       
    }
   var desc=this.SizeDesc;
  
   $(desc).each(function(){
 
        table.append(
            $('<tr></tr>').append($('<td  style="padding-left:40px;padding-bottom:2px;"></td>').append($('<input>').attr
            ({
                type: 'checkbox',
                name: 'sizelistbox',
                value: this.SizeDetId,
                id: 'sizelistbox'+ this.SizeDetId ,
               
                
            }))
            .append($('<label>').attr({
                for: 'sizelistbox' + this.SizeDetId, style:'float:none'
            }).text(this.SizeValue))));
            });
           
    });
    $('#tblSize').append(table);
}
function appendDesign(data)
{
    var items=JSON.parse(data.d);
    $.each(items,function(index,element){
        $('#ContentPlaceHolder1_txtDesignId').append($("<option></option>").val(element.DesignId).html(element.DesignName)).delay(1000);
         
        });
      //  getLastProductCode();


}
function ajaxFailed()
{
    alert("Error");
}
 function btnProduct_OnClientClick() {
 var size=new Array();
 var count=0;
           $('input[name="sizelistbox"]:checked').each(function(){

                size[count]=this.value;
                count++;
           });
           document.getElementById('ContentPlaceHolder1_HFSize').value=size.join(',');
           $('#ContentPlaceHolder1_HFCategory').val($('#ContentPlaceHolder1_DDLCategory option:selected').val());
           $('#ContentPlaceHolder1_HFDesign').val($('#ContentPlaceHolder1_txtDesignId option:selected').val());
           createSKU();

           
        }
function createSKU()
{
    var c= $('#ContentPlaceHolder1_HFCategory').val();
    var d=  $('#ContentPlaceHolder1_HFDesign').val();
    var r= $('#ContentPlaceHolder1_txtColor').val();
    var sku="C"+c+"D"+d+r;
    $('#ContentPlaceHolder1_txtsku').val(sku).wait(2000);
    }
    
