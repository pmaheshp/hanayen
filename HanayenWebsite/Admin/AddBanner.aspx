﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Admin.Master" CodeBehind="AddBanner.aspx.cs" Inherits="HanayenWebsite.Admin.AddBanner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Shopping/Style/Customer.css" rel="stylesheet" type="text/css" />
    <script src="js/Banners.js" type="text/javascript"></script>

    <link href="Styles/form.css" rel="Stylesheet" type="text/css" />
    <link rel="Stylesheet" href="Styles/Category.css" type="text/css" />
    <script type="text/javascript" src="js/ProductJson.js"></script>
    <script src="js/jquery.MultiFile.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {         
        });
        $(document).on('focus', '#ContentPlaceHolder1_txtNewSize', function () {
            var id = $("#ContentPlaceHolder1_dbStanderd option:selected").val();
            $("#ContentPlaceHolder1_HFStanderd").val(id);
        });
        $(document).on('change', '#ContentPlaceHolder1_txtDesignId', function () {
            //getLastProductCode();
        });
        $(document).on('click', '#imgCategory', function () {
            $("#dvCategory").css("display", "block");
            $("#productView").css("display", "none");
            $("#dvSize").css("display", "none");


        });
        $(document).on('click', '#iBtnSize', function () {
            $("#dvCategory").css("display", "none");
            $("#productView").css("display", "none");
            $("#dvSize").css("display", "block");
            getSizeStanderd();

        });
        $(document).on('click', '#imgDesign', function () {

            $("#dvDesign").css("display", "block");
            $("#productView").css("display", "none");
        });
        $(document).on('click', ".back", function () {
            $("#dvCategory").css("display", "none");
            $("#productView").css("display", "block");
            $("#dvSize").css("display", "none");
            $("#dvDesign").css("display", "none");
        });

            function insertBannerDetails() {
            var name = $("#txtBannerName").val();
            var Link = $("#txtBannerLink").val();
            var Type = $("#drpBannerType option:selected").text();
            var islink = document.getElementById('Text1').value;
            var Image = 'image';
            var str = "{Name:'" + name + "',Link:'" + Link + "',islink:'" + islink + "',Type:'" + Type + "',Image:'" + Image + "'}";
            alert(str);
            $.ajax({
                
                type: "GET",
                url: "AjaxBanner.aspx/AddBanner",
                data: { banneritems: JSON.stringify(str) },
                dataType: "JSON",
                async: false,
                contentType: "application/json;charset=utf-8",
                success: function (data) {
                    var jData = JSON.parse(data.d);
                }
            });
        }

          
    </script>
   
    <style type="text/css">
        .style2
        {
            height: 30px;
        }
        .style3
        {
            height: 27px;
        }
        .style6
        {
            height: 30px;
            width: 135px;
        }
        .style7
        {
            height: 38px;
            width: 18px;
        }
        .style8
        {
        }
        .style9
        {
            height: 27px;
            width: 135px;
        }
        .style10
        {
            height: 38px;
            width: 135px;
        }
        .style14
        {
            height: 38px;
            }
        .style15
        {
            height: 27px;
            width: 150px;
        }
        .style16
        {
            height: 30px;
            width: 150px;
        }
        .style19
        {
            height: 38px;
            width: 107px;
        }
        .style25
        {
            height: 27px;
            width: 70px;
        }
        .style26
        {
            height: 30px;
            width: 70px;
        }
        .style27
        {
            height: 27px;
            width: 107px;
        }
        .style30
        {
            height: 27px;
            width: 133px;
        }
        .style31
        {
            height: 30px;
            width: 133px;
        }
        .style32
        {
            height: 38px;
            width: 133px;
        }
        .style37
        {
            width: 135px;
        }
        .style38
        {
            width: 107px;
        }
        .style39
        {
            width: 133px;
        }
        </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="sidebar">
        <ul class="sideNav">
            <li>
                <asp:LinkButton ID="lbViewBanners" runat="server" Width="94px" 
                    onclick="lbViewBanners_Click">View Banners</asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="lbAddBanners" runat="server" Width="94px" Height="35px" 
                    onclick="lbAddBanners_Click">Add Banners</asp:LinkButton></li>
        </ul>
    </div>
    
            
    <div class="register">

    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
        <table>
            <tr>
                <td class="style8" colspan="6">
                      </td>
               
            </tr>
            
            <tr>
                <td class="style9">
                    </td>
                
                <td class="style30" colspan="2">
                    <asp:Label ID="lblbannername" runat="server" Text="Banner Name"></asp:Label>
                </td>
                <td class="style25">
                    <asp:TextBox ID="txtbannername" runat="server" Height="16px" Width="163px"></asp:TextBox>
                </td>
                <td class="style15">
                    <asp:Label ID="lblbannerlink" runat="server" Text="Banner Link"></asp:Label>
                </td>
                <td class="style3">
                    <asp:TextBox ID="txtbannerlink" runat="server"></asp:TextBox>
                </td>
                 <td rowspan="3">
                    <asp:Image ID="Image3" runat="server" Height="60px" Visible="False" 
                         Width="61px" />
                </td>
            </tr>
            
            <tr>
                <td class="style6">
                    </td>
                
                <td class="style31" colspan="2">
                    
                    <asp:Label  ID="lblbannerimage" runat="server" Text="Banner Image"></asp:Label>
                </td>
                <td class="style26">
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </td>
                <td class="style16">
                    <asp:Label ID="lblbannertype" runat="server" Text="Banner Type"></asp:Label>
                </td>
                <td class="style2">
                    <asp:DropDownList ID="drpdownbannertype" runat="server">
                        <asp:ListItem>Banner</asp:ListItem>
                        <asp:ListItem>OfferBanner</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            
            <tr>
                <td class="style37">
                    </td>
                <td class="style38">
                    </td>
                <td class="style39">
                    </td>
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="chkboxislink" runat="server" Text="Banner IsLink" />
                </td>
                <td>
                    </td>
            </tr>
            
            <tr>
                <td class="style10">
                    &nbsp;</td>
                <td class="style19">
                    &nbsp;</td>
                <td class="style32">
                    </td>
                <td class="style7" colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnbannersubmit" runat="server" Text="SUBMIT" 
                        onclick="btnbannersubmit_Click" />
                    <asp:Label ID="LblMessage" runat="server"></asp:Label>
                </td>
                <td class="style7">
                    </td>
                <td>
                </td>
            </tr>
            </table>
        </asp:View>
        <asp:View ID="View2" runat="server">
        <table>
        <tr>
                <td class="style10">
                    &nbsp;</td>
                <td class="style19">
                    &nbsp;</td>
                <td class="style32">
                    <asp:Label ID="lblbannertypeview" runat="server" Text="Banner Type"></asp:Label>
                    </td>
                <td class="style7" colspan="2">
                    <asp:DropDownList ID="drpdwnbannerview" runat="server" 
                        onselectedindexchanged="drpdwnbannerview_SelectedIndexChanged" 
                        AppendDataBoundItems="True" AutoPostBack="True">
                        <asp:ListItem>---Select---</asp:ListItem>
                        <asp:ListItem>Banner</asp:ListItem>
                        <asp:ListItem>OfferBanner</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="style7">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr>
                <td class="style10">
                    &nbsp;</td>
                <td class="style19">
                    &nbsp;</td>
                <td class="style14" colspan="3">
                    <asp:GridView ID="Grdbanner" runat="server" AutoGenerateColumns="False" 
                        HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" 
                        BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="BannerId" 
                        >
                        <Columns>
                            <asp:BoundField DataField="BannerName" HeaderText="Banner Name" >
                            <HeaderStyle Width="90px" />
                            <ItemStyle Width="90px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Banner Image">
                                <ItemTemplate>
                                    <asp:Image ID="Image1" runat="server" Height="100px" 
                                        ImageUrl='<%# Eval("BannerImage") %>' Width="150px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="BannerLink" HeaderText="Banner Link" />
                            <asp:BoundField DataField="BannerIsLink" HeaderText="Banner IsLink" />
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" 
                                        CommandArgument='<%# Eval("BannerId") %>' oncommand="LinkButton1_Command">Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton2" runat="server" 
                                        CommandArgument='<%# Eval("BannerId") %>' oncommand="LinkButton2_Command">Select</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" 
                            HorizontalAlign="Center" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Center" VerticalAlign="Middle" ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                    </asp:GridView>
                    </td>
                <td class="style7">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        </asp:View>

         
             
            </asp:MultiView>
               
           
        </div>
            
   
</asp:Content>
