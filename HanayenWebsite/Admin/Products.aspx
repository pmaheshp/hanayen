﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="Products.aspx.cs" Inherits="HanayenWebsite.Admin.Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Styles/form.css" rel="Stylesheet" type="text/css" />
    <link rel="Stylesheet" href="Styles/Category.css" type="text/css" />
    <script type="text/javascript" src="js/ProductJson.js"></script>
    <script src="js/jquery.MultiFile.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
//            $("#ContentPlaceHolder1_txtNewSize").focus(function () {
//                var id = $("#ContentPlaceHolder1_dbStanderd option:selected").val();
//                $("#ContentPlaceHolder1_HFStanderd").val(id);
//            });
        });
        $(document).on('focus', '#ContentPlaceHolder1_txtNewSize', function () {
            var id = $("#ContentPlaceHolder1_dbStanderd option:selected").val();
            $("#ContentPlaceHolder1_HFStanderd").val(id);
        });
        $(document).on('change', '#ContentPlaceHolder1_txtDesignId', function () {
            //getLastProductCode();
        });
        $(document).on('click', '#imgCategory', function () {
            $("#dvCategory").css("display", "block");
            $("#productView").css("display", "none");
            $("#dvSize").css("display", "none");
            

        });
        $(document).on('click', '#iBtnSize', function () {
            $("#dvCategory").css("display", "none");
            $("#productView").css("display", "none");
            $("#dvSize").css("display", "block");
            getSizeStanderd();

        });
        $(document).on('click', '#imgDesign', function () {

            $("#dvDesign").css("display", "block");
            $("#productView").css("display", "none");
        });
        $(document).on('click', ".back", function () {
            $("#dvCategory").css("display", "none");
            $("#productView").css("display", "block");
            $("#dvSize").css("display", "none");
            $("#dvDesign").css("display", "none");
        });
       
       
       
        //     
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="sidebar">
        <ul class="sideNav">
            <li>
                <asp:LinkButton ID="lbViewProduct" runat="server">View Products</asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="lbNewProduct" runat="server">New Products</asp:LinkButton></li>
        </ul>
    </div>
    <div class="register" id="productView">
    <form id="product" >
        <h1>
            Products</h1>
        <fieldset class="row1">
            <legend>Product Details</legend>
            <p>
                <label>
                    Product Name*</label>
                <input type="text" id="txtProduct" runat="server" />
                <label>
                    SKU</label><input id="txtsku" runat="server" readonly />
            </p>
            <p>
                <label>
                    Product Code</label>
                <input type="text" id="txtProductCode" runat="server"  />
                <label>
                    Design</label>-
                <select id="txtDesignId" runat="server">
                </select><img id="imgDesign" class="newImage" />
            </p>
        </fieldset>
        <fieldset class="row2">
            <legend>Price and Category</legend>
            <p>
                <label>
                    Category*</label>
                <select id="DDLCategory" runat="server" class="select">
                </select>
                <img id="imgCategory" class="newImage" alt="" /></p>
            <p>
                <label>
                    Price*</label>
                <input type="text" id="txtPrice" runat="server"  />
            </p>
            <p>
                <label>
                    Discount(%)</label>
                <input type="text" id="txtDiscount" runat="server" />
            </p>
            <p>
            </p>
           <p>
                <label>
                    Model Height</label><input id="txtHeight" runat="server" /></p>
            <p>
                <label>
                    color</label><input id="txtColor" runat="server" /></p>
            <p>
                <label>
                    Neck Type</label><input id="txtNeck" runat="server" /></p>
                
        </fieldset>
        <fieldset class="row3">
            <legend>Product Description</legend>
            <p>
                <label>
                    Quantity</label>
                <input type="text" id="txtQuantity" runat="server" class="short" />
            </p>
            <p>
                <label>
                    Size</label><table>
                        <tr>
                            <td>
                                <div class="checkDiv">
                                    <div id="tblSize" class="ListControl" >
                                    </div>
                                    
                                </div>
                            </td>
                            <td>
                                <img id="iBtnSize" class="newImage" alt="" />
                            </td>
                        </tr>
                    </table>
            </p>
            <p><label>Large File(762*1100)</label><asp:FileUpload ID="file_upload" class="multi" runat="server" /></p></p>
            
        </fieldset>
        <fieldset class="row5">
            <legend>More Details</legend>
            <p>
                <label>
                    Title*</label>
                <input type="text" id="txtTitle" class="long" runat="server"  />
                <label>
                    Material</label><input id="txtMaterial" class="long" runat="server" />
            </p>
            <p>
                <label>
                    Washing Instruction</label><input id="txtInstruction" runat="server" class="long" />
                <label>
                    Model Measurement</label><input id="txtMeasurement" runat="server" class="long" />
            </p>
            <p>
                <label>
                    Product Details</label>
                <textarea id="txtDetails" runat="server" cols="50" rows="10"></textarea>
            </p>
        </fieldset>
        <div>
            <asp:Button class="button" runat="server" Text="Save Product" ID="btnProduct" OnClick="btnProduct_Click" onclientclick="btnProduct_OnClientClick()"></asp:Button></div>
            </form>
    </div>
    <div class="register" id="dvCategory">s
        <h1>
            Product Category Master</h1>
        <fieldset class="row1">
            <legend>New Category</legend>
            <p>
                <label>
                    Category Name</label>
                <input id="txtNewCategory" runat="server" class="long" />
                <div id="dvBacktpProduct" class="back">
                    Back to Product</div>
            </p>
            <p>
                <asp:Button ID="btnNewCategory" runat="server" CssClass="btnSize" OnClick="btnNewCategory_Click"
                    Text="Save Category" ></asp:Button>
            </p>
            <p>
                <asp:GridView ID="gvCategory" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField HeaderText="Category ID" DataField="CategoryId" />
                        <asp:BoundField HeaderText="Category Name" DataField="CategoryName" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEditCategory" runat="server" value="" CssClass="delete" />
                                <asp:ImageButton ID="ibtnDeleteCategory" runat="server" value="" CssClass="delete" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </p>
        </fieldset>
    </div>
    <div class="register" id="dvSize">
        <h1>
            Size</h1>
        <fieldset class="row1">
            <legend>Create Size Master</legend>
            <p>
                <p>
                    <label>
                        Standerd</label><select id="dbStanderd" runat="server" onchange="dbStanderd_SelectedIndexChanged"></select></p>
                <label>
                    New Size</label>
                <input id="txtNewSize" runat="server" /><div id="sizeToProduct" class="back">
                    Back to Product</div>
            </p>
            <asp:Button ID="btnNewSize" onfocus="dbStanderd_SelectedIndexChanged" runat="server" Text="Save Size" OnClick="btnNewSize_Click"
                CssClass="btnSize" CausesValidation="false" />
        </fieldset>
    </div>
    <div class="register" id="dvDesign">
        <h1>
            Design</h1>
        <fieldset class="row1">
            <legend>Create Design Master</legend>
            <p>
                <label>
                    New Design</label>
                <input id="txtNweDesign" runat="server" class="long" /><div id="Div2" class="back">
                    Back to Product</div>
            </p>
            <asp:Button ID="btnDesign" runat="server" Text="Save Design" OnClick="btnDesign_Click"
                CssClass="btnSize" CausesValidation="false" />
        </fieldset>
       
    </div>
     <asp:HiddenField ID="HFStanderd" runat="server" />
     <asp:HiddenField ID="HFSize" runat="server" />
     <asp:HiddenField ID="HFCategory" runat="server" />
     <asp:HiddenField ID="HFDesign" runat="server" />
</asp:Content>
