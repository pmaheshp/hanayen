﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Collections.Specialized;
using HanayenEntity.SupportingModel;
using HanayenBussiness;

namespace HanayenWebsite
{
    /// <summary>
    /// Summary description for JQGridHandler
    /// </summary>
    public class JQBannerHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
            string strOperation = forms.Get("oper");
            HanayenEntity.Banner banner = new HanayenEntity.Banner();
            hnBannerHandler hnbannerhandler = new hnBannerHandler();
            List<HanayenEntity.Banner> collectionEmployee = hnbannerhandler.hnGetBannerView();
            string strResponse = string.Empty;

            if (strOperation == null)
            {
                //oper = null which means its first load.
                var jsonSerializer = new JavaScriptSerializer();
                context.Response.Write(jsonSerializer.Serialize(collectionEmployee.AsQueryable<HanayenEntity.Banner>().ToList<HanayenEntity.Banner>()));
            }
            else if (strOperation == "del")
            {
                banner.BannerId = Convert.ToInt32(forms.Get("BannerId").ToString());
                var result = hnbannerhandler.hnDeleteBanner(banner);
                if (result == "success")
                {
                    strResponse = "Banner Deleted Sucessfully";
                    context.Response.Write(strResponse);
                }
                else
                {
                    strResponse = "Banner Deletion Failed";
                    context.Response.Write(strResponse);
                }
            }
            }
           
        //    else if (strOperation == "edit")
        //    {
        //        int strCustId = 0;

        //        string strOut = string.Empty;
        //        AddEdit(forms, collectionEmployee, out strOut);
        //        context.Response.Write(strOut);
        //        var result = collectionEmployee.AsQueryable<CustomerViewJoin>().Select(c => c.CustomerId).Max();

        //        strCustId = (Convert.ToInt32(result) + 1);

        //        string strFirstName = forms.Get("FirstName").ToString();
        //        string strLastName = forms.Get("LastName").ToString();
        //        string strCountry = forms.Get("Country").ToString();
        //        string strState = forms.Get("State").ToString();
        //        string strCity = forms.Get("City").ToString();
        //        string strAddress1 = forms.Get("Address1").ToString();
        //        string strAddress2 = forms.Get("Address2").ToString();
        //        int strPostalCode = Convert.ToInt32(forms.Get("PostalCode").ToString());
        //        string strphone = forms.Get("Phone").ToString();
        //        long strReferal = Convert.ToInt64(forms.Get("Referal").ToString());
        //        string strCardNumber = forms.Get("CardNumber").ToString();
        //        HanayenEntity.SupportingModel.CustomerViewJoin cust = new HanayenEntity.SupportingModel.CustomerViewJoin();
        //        cust.CustomerId = strCustId;
        //        cust.FirstName = strFirstName;
        //        cust.LastName = strLastName;
        //        cust.CountryName = strCountry;
        //        cust.StateName = strState;
        //        cust.City = strCity;
        //        cust.Address1 = strAddress1;
        //        cust.Address2 = strAddress2;
        //        cust.PostalCode = strPostalCode;
        //        cust.Referal = strReferal;
        //        cust.CardNumber = strCardNumber;
        //        collectionEmployee.Save(objEmp);
        //        strResponse = "Employee record successfully updated";
        //    }
        //}

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        //private void AddEdit(NameValueCollection forms, CustomerViewJoin collectionEmployee, out string strResponse)
        //{
        //    string strOperation = forms.Get("oper");
        //    int strCustId = 0;
        //    if (strOperation == "edit")
        //    {
        //        strCustId = Convert.ToInt32(forms.Get("CustomerId").ToString());
        //    }
        //    else if (strOperation == "add")
        //    {
        //       // var result = collectionEmployee.AsQueryable<CustomerViewJoin>().Select(c => c.CustomerId).Max();
        //       // strCustId = (Convert.ToInt32(result) + 1);
        //    }

        //    string strFirstName = forms.Get("FirstName").ToString();
        //    string strLastName = forms.Get("LastName").ToString();
        //    string strLastSSN = forms.Get("LastSSN").ToString();
        //    string strDepartment = forms.Get("Department").ToString();
        //    string strAge = forms.Get("Age").ToString();
        //    int strSalary = Convert.ToInt32(forms.Get("Salary").ToString());
        //    string strAddress = forms.Get("Address").ToString();
        //    string strMaritalStatus = forms.Get("MaritalStatus").ToString();

        //    //Employee objEmp = new Employee();
        //    //objEmp._id = strEmpId;
        //    //objEmp.FirstName = strFirstName;
        //    //objEmp.LastName = strLastName;
        //    //objEmp.LastSSN = strLastSSN;
        //    //objEmp.Department = strDepartment;
        //    //objEmp.Age = Convert.ToInt32(strAge);
        //    //objEmp.Address = strAddress;
        //    //objEmp.MaritalStatus = strMaritalStatus;
        //    //objEmp.Salary = strSalary;
        //    //collectionEmployee.Save(objEmp);
        //    //strResponse = "Employee record successfully updated";
        }

    }
//}