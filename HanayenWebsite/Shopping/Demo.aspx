﻿
<%@ Page Language="C#" MasterPageFile="~/Shopping/ShoppingMaster.Master" AutoEventWireup="true" CodeBehind="Demo.aspx.cs" Inherits="HanayenWebsite.Demo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Image Slider Theme - Jssor Slider, Carousel, Slideshow with Javascript Source Code</title>
</head>
<body>
    <!-- Caption Style -->
    <script src="js/JSHomeView.js" type="text/javascript"></script>
	<link href="css/sliderTest.css" rel="stylesheet" type="text/css">
    <!-- it works the same with all jquery version from 1.x to 2.x -->
    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jssor.js"></script>
    <script type="text/javascript" src="js/jssor.slider.js"></script>
    <script type="text/javascript" src="js/slider.js"> </script>
    <script type="text/javascript">

</script>
   
   
    <!-- Jssor Slider Begin -->
    <!-- You can move inline styles to css file or css block. -->
    <div id="topsection">
    <div id="slider1_container" style="float:left; width: 79%;margin-left: 3%;position: relative; top: 0px; left: 0px; width: 70%; height:430px; overflow: hidden; ">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 960px; height: 430px; overflow: hidden;" id="bannerBox">
        
           
           <%-- <div>
              

        <img src="../Admin/BannerImages/Happy-new-year-card-message-new-2015.jpg" />
               
            </div>
            <div>
            <img src="../Admin/BannerImages/logo1.gif" />

            </div>
            <div>
               

                <img src="../Admin/BannerImages/Screenshot%20(1).png" />
                
            </div>
            <div>
                <img u="image" src="img/photography/007.jpg" />
                <!--  <div u=caption t="*" class="captionOrange"  style="position:absolute; left:20px; top: 30px; width:300px; height:30px;"> 
                finger catchable right to left
                </div>-->
            </div>
            <div>
                <img u="image" src="img/photography/008.jpg" />
                <!--  <div u=caption t="*" class="captionOrange"  style="position:absolute; left:20px; top: 30px; width:300px; height:30px;"> 
                responsive, scale smoothly
                </div>-->
            </div>
            <div>
                <img u="image" src="img/photography/009.jpg" />
                 <!-- <div u=caption t="*" class="captionOrange"  style="position:absolute; left:20px; top: 30px; width:300px; height:30px;"> 
                random caption transition
                </div>-->
            </div>
            <div>
                <img u="image" src="img/photography/010.jpg" />
                
            </div>
            <div>
                <img u="image" src="img/photography/011.jpg" />
               
            </div>
        </div>
        
        --%>
        </div>
        <!-- Bullet Navigator Skin Begin -->
        <!-- jssor slider bullet navigator skin 01 -->
       
        <!-- bullet navigator container -->
        <div u="navigator" class="jssorb01" style=" display:none;position: absolute; bottom: 16px; right: 10px;">
            <!-- bullet navigator item prototype -->
            <div u="prototype" style="POSITION: absolute; WIDTH: 12px; HEIGHT: 12px;border-radius:12px;"></div>
        </div>
        <!-- Bullet Navigator Skin End -->
        
        <!-- Arrow Navigator Skin Begin -->
       
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 189px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 189px; right: 8px">
        </span>
        <!-- Arrow Navigator Skin End -->
       
    </div>
    </div>
<div class="banner_right fr" style="float:left;margin-left: 8px;" id="bannerrightBox">
<%--<a href="/clearance.html" class="banner_r_top">

<img src="http://css.dresslily.com/imagecache/DRSLILY/images/pageimg/homebanner/clearance-2.jpg" class="js_lazy" width="100%">

</a>--%>
</div>
</div>
<%--<div class="sectionSub">
<h1>Hot Sales</h1>
<div class="section_content">
<div class="section_item" id="hotsaleview">
<div><img src="../Products/Small/Pic1_1.jpg" /></div>
<div class="des_item">This is sample des</div>
<div class="item_price">$18.33</div>
</div>
<div class="section_item">
<div><img src="../Products/Small/Pic1_10.jpg" /></div>
<div class="des_item">This is sample des</div>
<div class="item_price">$18.33</div>
</div>
<div class="section_item">
<div><img src="../Products/Small/Pic1_10.jpg" /></div>
<div class="des_item">This is sample des</div>
<div class="item_price">$18.33</div>
</div>
<div class="section_item">
<div><img src="../Products/Small/Pic1_10.jpg" /></div>
<div class="des_item">This is sample des</div>
<div class="item_price">$18.33</div>
</div>
<div class="section_item">
<div><img src="../Products/Small/Pic1_10.jpg" /></div>
<div class="des_item">This is sample des</div>
<div>$18.33</div>
</div>
</div>
</div>--%>
<div class="sectionSub" id="hotsale">
<h1>Hot Sales</h1>
</div>

<div class="sectionSub" id="NewArrival">
<h1>New Arrivals</h1>
</div>
<%--<div class="section_content">
<div class="section_item">
<div><img src="../Products/Small/Pic1_1.jpg" /></div>
<div class="des_item">This is sample des</div>
<div class="item_price">$18.33</div>
</div>
<div class="section_item">
<div><img src="../Products/Small/Pic1_10.jpg" /></div>
<div class="des_item">This is sample des</div>
<div class="item_price">$18.33</div>
</div>
<div class="section_item">
<div><img src="../Products/Small/Pic1_10.jpg" /></div>
<div class="des_item">This is sample des</div>
<div class="item_price">$18.33</div>
</div>
<div class="section_item">
<div><img src="../Products/Small/Pic1_10.jpg" /></div>
<div class="des_item">This is sample des</div>
<div class="item_price">$18.33</div>
</div>
<div class="section_item">
<div><img src="../Products/Small/Pic1_10.jpg" /></div>
<div class="des_item">This is sample des</div>
<div>$18.33</div>
</div>
</div>
</div>
    <!-- Jssor Slider End -->
--%></asp:Content>