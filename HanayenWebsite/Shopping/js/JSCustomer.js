﻿
function insertUserDetails() {
    
    var email = document.getElementById('email').value;
    var passw = document.getElementById('password').value;
    var confPass = document.getElementById('repassword').value;
    var str = "{email:'" + email + "',password:'" + passw + "'}";
    alert(str);
    validateallfields();
    if (checkalldiv()) {
        $.ajax({
            type: "GET",
            url: "AjaxCustomer.aspx/InsertCustomerDetails",
            data: { custData: JSON.stringify(str) },
            dataType: "JSON",
            async:false,
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                var jData = JSON.parse(data.d);
                $("#email").val(" ");
                $("#password").val("");
                $("#repassword").val("");
                $("#agreemsg").val(""); 

            }
        });

        window.location = "ValidateUser.aspx";
    }

}

$(document).ready(function () {
   

    $("#repassword").blur(function () {
        validatePassword();
    });

    $("#email").blur(function () {
       
        validateemail();
    });

    $("#password").blur(function () {
        var divtable = document.getElementById('passwmsg');
        divtable.innerHTML = ''; 
       
    });


});

    function validatePassword()
    {
     var email = $("#email").val();
     var password = $("#password").val();
     var confPas = $("#repassword").val();

     if (password != confPas) {
         var divtable = document.getElementById('passwrdmsg');
         divtable.innerHTML = 'Password Mismatch'
     }
     else {
         var divtable = document.getElementById('passwrdmsg');
         divtable.innerHTML = ''


     } 

 }

 function validateemail() {
     var email = $("#email").val();
     $.ajax({
         type: "GET",
         url: "AjaxLogin.aspx/CheckEmailValid",
         data: { email: JSON.stringify(email) },
         dataType: "JSON",
         contentType: "application/json;charset=utf-8",
         success: function (data) {
             var jData = data.d;
             if (jData === "false") {

                 var divtable = document.getElementById('emailmsg');
                 divtable.innerHTML = 'Email Already Registred';

             }
             else {

                 var divtable = document.getElementById('emailmsg');
                 divtable.innerHTML = '';
                 checkemailvalid();


             }

         }
     });


 }



 function checkemailvalid() {
     var sEmail = $("#email").val();
     if (sEmail != "") {
         var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
         if (filter.test(sEmail)) {
             var divtable = document.getElementById('emailmsg');
             divtable.innerHTML = '';
             
         }
         else {
             var divtable = document.getElementById('emailmsg');
             divtable.innerHTML = 'Invalid Email Format';
            
         }
     }

 }


 function validateallfields() {


     var email = $("#email").val();
     var password = $("#password").val();
     var confPas = $("#repassword").val();
     if (email == "") {
         var divtable = document.getElementById('emailmsg');
         divtable.innerHTML = 'Please enter a valid email address'; 

        
     }
     if (password == "") {
         var divtable = document.getElementById('passwmsg');
         divtable.innerHTML = 'Provide a password'; 
      
     }
     if (confPas == "") {
         var divtable = document.getElementById('passwrdmsg');
         divtable.innerHTML = 'Repeat your password';

     }

     if ($("#agreement").is(':checked')) {
         var divtable = document.getElementById('agreemsg');
         divtable.innerHTML = '';
     }
     else
     {
         var divtable = document.getElementById('agreemsg');
         divtable.innerHTML = 'To complete the registration, you must agree to hanayen Terms and Conditions.';

     }

 }

 function checkalldiv() {
     
     var email = $("#emailmsg").text();
     var password = $("#passwmsg").text();
     var confPas = $("#passwrdmsg").text();
     var agree = $("#agreemsg").text();
    if (email == "" && password == "" && confPas == "" && agree == "") {
        return true;
     }
     else {
     return false;
    }
 }