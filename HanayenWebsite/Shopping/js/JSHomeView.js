﻿$(document).ready(function () {
    
    getNewArrivals();
    selectHotsaleproduct();
    getBanners();
    getOfferBanners();
   
    
});
function getNewArrivals()
{

    $.ajax({
        type:"GET",
        url:"AjaxHome.aspx/GetNewArrivals",
        data:{},
        dataType:"JSON",
        contentType:"application/json;charset=utf-8",
        success:function(data){
        var jData=JSON.parse(data.d);            
        AddTableNewArrival(jData);
        },
    });
}

function selectHotsaleproduct() 
{
    
     $.ajax({
        type:"GET",
        url:"AjaxHome.aspx/GetHotsaleProduct",
        data: "{}",
        dataType:"JSON",
        async: false,
        contentType:"application/json;charset=utf-8",
        success:function(data){
        var jData=JSON.parse(data.d);
        addTable(jData);
        
       
        },
    });
}
function addTable(jData) 
{
$.each(jData,function(key,value){

SetHotSaleItems(key,value);

});
}

function AddTableNewArrival(jData) 
{
$.each(jData,function(key,value){
SetNewArrivals(key,value);

});
}
function SetHotSaleItems(key,value)
{


var divtable=document.getElementById('hotsale');
var  content='<div class="section_item">'+
               '<div><a href="ItemView.aspx?id='+value.ProductId+'"><img src="../Products/Thumb/'+value.ImageId.File+'"></a></div>'+
               '<div class="des_item"><a href="ItemView.aspx?id='+value.ProductId+'">'+value.ProductName+'</a></div>'+
               '<div class="item_price">'+value.Price+'</div>'+
               '</div>';

divtable.innerHTML=divtable.innerHTML+content;
}

function SetNewArrivals(key,value)
{
var divtable=document.getElementById('NewArrival');
var  content='<div class="section_item">'+
               '<div><a href="ItemView.aspx?id='+value.ProductId+'"><img src="../Products/Thumb/'+value.ImageId.File+'"></a></div>'+
               '<div class="des_item"><a href="ItemView.aspx?id='+value.ProductId+'">'+value.ProductName+'</a></div>'+
               '<div class="item_price">'+value.Price+'</div>'+
               '</div>';

divtable.innerHTML=divtable.innerHTML+content;
//console.log("jjjj");

}
function getBanners()
{

    $.ajax({
        type:"GET",
        url:"AjaxHome.aspx/GetBanners",
        data:{},
        dataType:"JSON",
        async: false,
        contentType:"application/json;charset=utf-8",
        success:function(data){
        //console.log("success");
        var jData=JSON.parse(data.d);            
        AddTableBanners(jData);
        },
    });
}
function AddTableBanners(jData) 
{
$.each(jData,function(key,value){

SetBannerItems(key,value);

});
}
function SetBannerItems(key,value)
{
var str='<div><img src="'+value.BannerImage+'"></div>';
$("#bannerBox").append(str);
//console.log("value:"+str);
//console.log($("#bannerBox").html());


}



function getOfferBanners()
{

    $.ajax({
        type:"GET",
        url:"AjaxHome.aspx/GetOfferBanners",
        data:{},
        dataType:"JSON",
        async: false,
        contentType:"application/json;charset=utf-8",
        success:function(data){
        //console.log("success");
        var jData=JSON.parse(data.d);            
        AddTableOfferBanners(jData);
        },
    });
}

function AddTableOfferBanners(jData) 
{
$.each(jData,function(key,value){
SetofferBannerItems(key,value);
});
}
function SetofferBannerItems(key,value)
{
var str='<div><img src="'+value.BannerImage+'"></div>';
$("#bannerrightBox").append(str);
}


