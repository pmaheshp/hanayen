﻿$(document).ready(function () {
    $("#email").blur(function () {
        validateemail();
    });
    $("#password").blur(function () {
        var divtable = document.getElementById('passwmsg');
        divtable.innerHTML = '';

    });
});
function checkvaliduser() {
    var email = $("#email").val();
    var password = $("#password").val();
    var str = '{email:"' + email + '",password:"' + password + '"}';
    validateallfields();
    if (checkalldiv()) {
        $.ajax({
            type: "GET",
            url: "AjaxLogin.aspx/CheckUserIsValid",
            data: { user: JSON.stringify(str) },
            dataType: "JSON",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                var jData = JSON.parse(data.d);
                $.each(jData, function (key, value) {
                    window.location = "Gallery.aspx";

                });
            }
        });
    }
    
}   
function validateemail() {
    var email = $("#email").val();
    var divtable = document.getElementById('emailmsg');
    divtable.innerHTML = '';
    checkemailvalid();         
        }
   function checkemailvalid() {
    var sEmail = $("#email").val();
    if (sEmail != "") {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            var divtable = document.getElementById('emailmsg');
            divtable.innerHTML = '';

        }
        else {
            var divtable = document.getElementById('emailmsg');
            divtable.innerHTML = 'Invalid Email Format';

        }
    }

}


    function validateallfields() {
    var email = $("#email").val();
    var password = $("#password").val();
   
    if (email == "") {
        var divtable = document.getElementById('emailmsg');
        divtable.innerHTML = 'Please enter a valid email address';
    }
    if (password == "") {
        var divtable = document.getElementById('passwmsg');
        divtable.innerHTML = 'Provide a password';

    }
  }

function checkalldiv() {
   
    var email = $("#emailmsg").text();
    var password = $("#passwmsg").text();
    
    if (email == "" && password == "") {   
        return true;
    }
    else {    
        return false;
    }
}