﻿var start=0;
var end=30;
var categoryId=1;
var total=0;    
var inc=30;
function GetCollection(category) {
    
    var categoryId = category;
    var par ={start:start,end:end,category:categoryId};
    $.ajax({
        type:"GET",
        url:"AjaxGallery.aspx/GetGallaryProducts",
        data:par,
        dataType:"JSON",
        contentType:"application/json;charset=utf-8",
        success:function(data){
        var jData=JSON.parse(data.d);
        setCollection(jData);
           
        },
        fail:function(){$("#catalog_listings").html("Sorry, There is not Item Available for Now. ");},
    });
}
function GetProductCountByCategory(category) {

    var categoryId = category;
   
    $.ajax({
        type:"GET",
        async:false,
        url:"AjaxGallery.aspx/GetProductsCountByCategory",
        data:{Category:categoryId},
        dataType:"JSON",
        contentType:"application/json;charset=utf-8"
       
    }).done(function(data){
            setTotal(data.d);
      
    });
}
$(document).on("click","#next_page",function(){
    start=start+inc;
    end=start+inc;
    GetCollection(categoryId);
});
$(document).on("click",".image_container",function(e){
var pId=$(this).attr('data');
  
            GetZoomImage(pId);
            GetDescription(pId);
           
            window.location="ItemView.aspx?id="+pId;
         
});
   $(document).on("click", "#back_page", function () {
           
            clearZoom();
        });
$(document).on("click","#prev_page",function(){
    start=start-inc;
    end=end-inc;
    GetCollection(categoryId);
});
$(document).on("dblclick",".simpleLens-lens-image",function(e){
});
 $(document).on('click', '#catalog_category_option li ', function (e) {
        start=0;
        end=inc;
    
         categoryId = $(this).attr('data-index');
         $("#hf_category").val(categoryId);
         //getTotalProducts(categoryId);
          GetCollection(categoryId);
          GetProductCountByCategory(categoryId);
      //  $('#pagination-demo').empty();
        $('#page_navigation').html('');
    $('#page_navigation').html('<ul id="pagination-demo" class="pagination-sm"></ul>')
       
       // categoryClick();
    $('#pagination-demo').twbsPagination({
            totalPages: Math.ceil(total/inc),
            visiblePages: 3,
            onPageClick: function (event, page) {
                getCollectionPage(page);
                //goto galley.js
            }
        });

    });
function GetZoomImage(id)
{
    
   
    $.ajax({
        type:"GET",
        url:"AjaxGallery.aspx/GetProductsDetails",
        data:{productId:JSON.stringify(id)},
        dataType:"JSON",
        contentType:"application/json;charset=utf-8",
        success:function(data){
        var jData=JSON.parse(data.d);
            setZoomImage(jData);
            setDescription(jData);
        },
    });
}
function GetProductSize(id)
{
    $.ajax({
        type:"GET",
        url:"AjaxGallery.aspx/GetProductSizes",
        data:{productId:JSON.stringify(id)},
        dataType:"JSON",
        contentType:"application/json;charset=utf-8",
        success:function(data){
            var jsData=JSON.parse(data.d);
            setSizeDetails(data);
        },
    });
}
function setSizeDetails(data)
{

}
function GetDescription(id)
{
    
   
    $.ajax({
        type:"GET",
        url:"/AjaxCallGallery.aspx/GetDetails",
        data:{itemId:JSON.stringify(id)},
        dataType:"JSON",
        contentType:"application/json;charset=utf-8",
        success:function(data){
        var jData=JSON.parse(data.d);
            setDescription(jData);
                    },
    });
}
function setDescription(data)
{
$.each(data,function(key,value){
    $(".product-name").text(value.ProductName);
            
            $("#txtDesc").val(value.Description);
});

}
function setCollection(data)
{

var path="../Products/Small/";
var pId=0,temp=-1;
var prev,next;
if(data.length>0) $("#catalog_listings").empty();
    $.each(data,function(key,value)
    {
    if(temp==-1 || temp!=value.ProductId)
    {
         var image=value.ImageId.File;
         pId=value.ProductId;
        if(pId!=temp)
        {
         var ogPrice="";
        if(value.Discount!=value.Price)
        {
            ogPrice="AED"+value.Price;
        }
        var li=$("<li data-nm-hover-toggle-notouch='' class='listing closed' data-index='"+value.ProductId+"'></li>");
        var img=("<div class='image_container' data="+value.ProductId+"><img class='Loaded lazy' src='"+path+image+"' /><div class='preloader'></div> </div>");
        var desc=("<div class='details_container'><p class='brand'>"+value.Title+"</p><p class='price'><span class='pre_reduction'>"+ogPrice+"</span><span class='reduction'>"+value.Discount+"AED</span></p></div>");
        $("#catalog_listings").append
        ($(li).append($("<a href='#'></a>").append(img).append(desc)));
        temp=pId;
        
        }
      }
    });
   
    if(data.length<1)
    {
        $("#catalog_listings").append("<li>/li>").html("Sorry, There is not Item Available for Now. ");
        $("#next_page").css("display","none");
    }
   
   
}
function clearZoom()
{
$(".simpleLens-thumbnails-container").empty();
 
}
function setZoomImage(data)
{


var lc=0,sc=0,tc=0,tem=0;
var lImg,sImg,tImg;

    $.each(data,function(key,value)
    {
    var imgList=this.imageList;
    $.each(imgList,function(key,value){
        tc=value.ProductId;
       
           
                $(".simpleLens-lens-image").attr("data-lens-image","../Products/Large/"+value.File);
              
          
          
       
        
                $(".simpleLens-big-image").attr("src","../Products/Small/"+value.File);
           
      
       
            
               $(".simpleLens-thumbnails-container").append($("<ul id='carousel_pager'></ul>").append($("<li></li>").append($("<a></a>").attr("href","#").attr("class","simpleLens-thumbnail-wrapper").attr("data-lens-image","../Products/Large/"+value.File).attr("data-big-image","../Products/Small/"+value.File).append($("<img></img>").attr("src","../Products/Thumb/"+value.File).attr("alt","")))));
                //$("#carousel_pager>").append($("<li></li>").append($("<a></a>").attr("href","#").attr("class","simpleLens-thumbnail-wrapper").attr("data-lens-image","../Products/Large/"+value.File).attr("data-big-image","../Products/Small/"+value.File).append($("<img></img>").attr("src","../Products/Thumb/"+value.File).attr("alt",""))));
//               var str="<li><a class='zoomThumbActive' href='javascript:void(0);' rel='{gallery: 'gal1', smallimage: 'demos/imgProd/triumph_small1.jpg',largeimage: 'demos/imgProd/triumph_big1.jpg'}'><img src='demos/imgProd/thumbs/triumph_thumb1.jpg'></a></li>";
//                $(".thumblist").append(str);
         
            
        
        });
     });

}
function getTotalProducts(cId)
{
   $.ajax({
        type:"GET",
        url:"../Gallery.aspx/GetTotalProducts",
        data:{category:cId},
        dataType:"JSON",
        contentType:"application/json;charset=utf-8",
        async:false,
   
   }).done(function(data){
   
        var data=JSON.parse(data.d);
        var tot=data;
        total=tot;
        $("#hdtotal").attr('value',tot);
          
        return data;
    
   });
}
function getCategory(page)
{

    $.ajax({
       
         url: "../Admin/AjaxCall.aspx/GetCategory",
        data: "{}",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        success:function(data){
            var jsDate=JSON.parse(data.d);
            if(page=="order")
            {
            $.each(jsDate,function(key,value){
                SetNewItemCategory(key,value);
            });
            }
            else if(page=="product"){
                 $.each(jsDate,function(key,value){
                SetProductCategory(key,value);
            });
            }
            else
            {
             $.each(jsDate,function(key,value){
                SetCollectionCategory(key,value);
                });
            
            }
        },
        
    });
}
function setTotal(data)
{
    total=data;
}
function getCollectionPage(page)
{
    var p_page=$("#previous_page").val();
    start=(page-1)*inc;
    
    end=start+inc;
    var cat= $("#hf_category").val();
    GetCollection(cat);
}
function SetCollectionCategory(key,value)
{
    //$("#catalog_category_option").append($("<ul></ul>").append($("<li></li>")).append("<a></a>").html(value.ItemName).attr("href",value.ItemId);
    $("#catalog_category_option ul").append($("<li></li>").append($("<a></a>").html(value.CategoryName).attr("href","#").attr("class",".catgory")).attr("data-index",value.CategoryId));
}
 $(document).on("click", ".simpleLens-thumbnail-wrapper", function (e) {
            var lens = $(this).attr("data-lens-image");
            $(".simpleLens-lens-image").attr("data-lens-image", lens);
            var big = $(this).attr("data-big-image");
            $(".simpleLens-big-image").attr("src", big);
        });

        $(document).on("click", "#back_page", function () {
            var cid = GetQueryStringParams("cId");
            $(location).attr("href", "Gallery.aspx?cId='" + cid + "'");
        });
        $(document).on("click", ".simpleLens-thumbnail-wrapper", function (e) {
            var lens = $(this).attr("data-lens-image");
            $(".simpleLens-lens-image").attr("data-lens-image", lens);
            var big = $(this).attr("data-big-image");
            $(".simpleLens-big-image").attr("src", big);
        });

        $(document).on("click", "#back_page", function () {
            var cid = GetQueryStringParams("cId");
            $(location).attr("href", "Gallery.aspx?cId='" + cid + "'");
        });
        function GetQueryStringParams(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
    }
//FOR GETTING QUERYSTRING
     function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }



   