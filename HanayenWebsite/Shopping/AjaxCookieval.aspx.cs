﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

namespace HanayenWebsite.Shopping
{
    public partial class AjaxCookieval : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        private static string GetCookieGenrated(string msg)
        {
            String cookieID = SetCookie(12);
            return cookieID;
        }

        private static Random random = new Random((int)DateTime.Now.Ticks);

        private static string SetCookie(int Size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < Size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            builder.ToString();
            return builder.ToString();
        }
    }
}