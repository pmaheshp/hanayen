﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shopping/ShoppingMaster.Master"
    AutoEventWireup="true" CodeBehind="ItemView.aspx.cs" Inherits="HanayenWebsite.Shopping.ItemView" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <%--
    <link rel="stylesheet" type="text/css" href="Style/jquery.simpleLens.css" />
    <link rel="stylesheet" type="text/css" href="Style/jquery.simpleGallery.css" />
    <link rel="Stylesheet" type="text/css" href="Style/Gallery.css" />--%>
    <link rel="Stylesheet" type="text/css" href="Style/Products.css" />
    <link rel="Stylesheet" type="text/css" href="Style/itemcart.css" />
        <script src="js/JSGallery.js" type="text/javascript"></script>
  <%--  <link href="Style/Master.css" rel="Stylesheet" type="text/css" />--%>
    <%--<link rel="stylesheet" type="text/css" href="Style/jquery.confirm.css" />
  --%>  
  <%--<script type="text/javascript" src="js/jsGallery.js"></script>--%>
    <script type="text/javascript" src="js/jquery.confirm.js"></script>
    <script src="js/JSItemView.js" type="text/javascript"></script>
    <%--<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">--%>
    
    <script type="text/javascript">
        $(document).ready(function () {
 
            var itemId = GetQueryStringParams("id");
            var cId = GetQueryStringParams("cId");
            GetZoomImage(itemId);
            GetProductSize(itemId);
            // GetDescription(itemId);
            clearZoom();
        });
        $(document).on("click", ".simpleLens-thumbnail-wrapper", function (e) {
            var lens = $(this).attr("data-lens-image");
            $(".simpleLens-lens-image").attr("data-lens-image", lens);
            var big = $(this).attr("data-big-image");
            $(".simpleLens-big-image").attr("src", big);
        });

        $(document).on("click", "#back_page", function () {
            var cid = GetQueryStringParams("cId");
            $(location).attr("href", "Gallery.aspx?cId='" + cid + "'");
        });
        $(document).on("click","#new_addcart",function()
        {
            addcart();
        });
        $(document).on("click", ".simpleLens-thumbnail-wrapper", function (e) {
            var lens = $(this).attr("data-lens-image");
            $(".simpleLens-lens-image").attr("data-lens-image", lens);
            var big = $(this).attr("data-big-image");
            $(".simpleLens-big-image").attr("src", big);
        });

        $(document).on("click", "#back_page", function () {
            var cid = GetQueryStringParams("cId");
            $(location).attr("href", "Gallery.aspx?cId='" + cid + "'");
        });
        $(document).on("click", ".pr_details", function () {
            var a = $(".pr_details1").css("display");
            var ele = $(".pr_details")
            if (a == "block") {
                $(".pr_details1").hide(1000);
                $('html, body').animate({ scrollTop: ele.offset().top }, 450);
            }
            else {
                $(".pr_details1").show(1000);
                $('html, body').animate({ scrollTop: ele.offset().top }, 450);
            }
        });
        $(document).on("click", ".pr_head2", function () {
            var a = $(".pr_details2").css("display");
            var ele=$(".pr_details2")
            if (a == "block") {
                $(".pr_details2").hide(1000);
                $('html, body').animate({ scrollTop: ele.offset().top }, 1000);
            }
            else {
                $(".pr_details2").show(1000);
                $('html, body').animate({ scrollTop: ele.offset().top }, 1000);
            }
        });
 
       
 function ConfirmMessage(count,pId,cookieId)
 {
       //-----------------Confirmation Box----------------------
        $.confirm({
            'title': 'Already In Cart',
            'message': 'Do you want to Add. <br />',
            'buttons': {
                'Yes': {
                    'class': 'blue',
                    'action': function () {
                     count=count+1;

                        UpdateQuantity(pId,cookieId,count)
                        window.location="CartItem.aspx";

                    }
                },
                'No': {
                    'class': 'gray',
                    'action': function () { } // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });
 }
       //--------------------------------------
        function getCookie(name) {  
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }



       function addcart() {
       var pdtItem=0;
       var pId = getParameterByName('id');
       var cookieId = getCookie('HanayenCookie');
       alert(cookieId);
       var str='{productId:"'+pId+'",cookieId:"'+cookieId+'"}';
       var countpdt=0;
       $.ajax({
        type:"GET",
        url:"AjaxItemView.aspx/CheckProductId",
        data:{product:JSON.stringify(str)},
        dataType:"JSON",
        async:false,
        contentType:"application/json;charset=utf-8",
        success:function(data){
        var jData=JSON.parse(data.d);
        countpdt=jData;
        pdtItem=data.d;         
        if(pdtItem=='0')
        {
       inserttoCartitem();
        window.location="CartItem.aspx";
//     $.post("CartItem.aspx",
//        {
//        id:pId});
//        window.location="CartItem.aspx";
     }
        else
        {

        ConfirmMessage(countpdt,pId,cookieId);
        }
             
        }

    });
  
     }
        
   function UpdateQuantity(pdtId,cookieId,qty)
   {
   var str='{productId:"'+pdtId+'",cookieId:"'+cookieId+'",qty:"'+qty+'"}';
   $.ajax({
        type:"GET",
        url:"AjaxItemView.aspx/UpdateQty",
        data:{cartItem:JSON.stringify(str)},
        dataType:"JSON",
        async:false,
        contentType:"application/json;charset=utf-8",
        success:function(data){
        var jData=JSON.parse(data.d);
        
        
        },
    });

}




 </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <article>
    <div> <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" />
<asp:SiteMapPath ID="SiteMap1" runat="server"></asp:SiteMapPath></div>
    <div class="simpleLens-gallery-container" id="demo-1">
   <div class="column_grid large_gutter">
    <div class="row">
        <div class="simpleLens-container">
        <table><tr><td style="vertical-align:top">
         <div class="simpleLens-thumbnails-container">
           
        </div>
        </td><td style="vertical-align:top">
            <div class="simpleLens-big-image-container">
                <a class="simpleLens-lens-image" data-lens-image="">
                    <img src="../img/loader.gif" class="simpleLens-big-image" alt="" />
                </a>

            </div></td><td style="padding-top:0px" vertical-align="top" class="cont">
           <div class="product-detail-container product-detail pull-right">
           <header>
           <h1 class="product-name h6" itemprop="name">HANAYEN ABAYA</h1>
          <div id="product-content">
           <div class="product-price h6" itemtype="http://schema.org/Offer" itemscope="" itemprop="offers">
<span class="price-sales black price" itemprop="price">$75.00</span>
<span class="pre_reduction price" itemprop="
">$175.00</span>
</div>
<div id="product-number" class="product-number" data-style="301953_25">
Product Code
<span itemprop="productID">301953-25</span><br />
Design Name
<span itemprop="productID">CREP</span>
</div>


</div>
</header>
<div class="option_select">
<p class="faux_label">
Select Size</p>
<div class="size_help">
<div id="size_guide_container">
<a id="size_guide_trigger" data-ng-click="ux.toggle('showSizeGuide')" href="" title="Size Guide">Size Guide</a>
</div>
</div>
<ul id="product_size">
<li><label class="available" data-ng-class="{selected: ctrl.selectedSku === 'EL464AT14SAN-520295'}">
<span class="size">S</span>

</label></li>
<li><label class="available" data-ng-class="{selected: ctrl.selectedSku === 'EL464AT14SAN-520295'}">
<span class="size">XL</span>

</label></li>
<li><label class="available" data-ng-class="{selected: ctrl.selectedSku === 'EL464AT14SAN-520295'}">
<span class="size">L</span>

</label></li>
<li><label class="available" data-ng-class="{selected: ctrl.selectedSku === 'EL464AT14SAN-520295'}">
<span class="size">XXL</span>

</label></li>
<li><label class="available" data-ng-class="{selected: ctrl.selectedSku === 'EL464AT14SAN-520295'}">
<span class="size">M</span>

</label></li>
<div id="product_size_mapper" data-nm-size-mapping="">
<div class="select_container" data-ng-show="sizeMap">
<span>
<select class="ng-pristine ng-valid" data-ng-change="changeSizes()" data-ng-options="size_run for size_run in sizeMap.size_runs" data-ng-model="selectedRun" name="size_map" onblur="return size_map_onblur()" onblur="return size_map_onblur()">
<option value="0" selected="selected">International</option>
<option value="1">UK</option>
</select>
</span>
</div>
</div>
</ul>
<div class="order_btn clearfix cart_handle">
<p id="add_cart_msg587837" class="fl">
<p id="P1" class="fl">
<a id="new_addcart" class="redBtn" srcpage="2" rel="nofollow" ref="1"  attrchage="7|8" num="1" gid="587837" href="javascript:void(0)">
<span>ADD TO BAG</span>
</a>
</p>
<a class="addToFavBtn fl" data-src="/fun/index.php?act=drop_to_collect&id=587837" rel="nofollow" href="javascript:void(0)">
<i class="c_tagbg"></i>
Add to Favorites
</a>
</div>

</div>
<div id="product_info">
<div class="toggle_section">
<div class="toggle_link_container pr_details">
<a class="open" href="#" title="Details">Details</a>
</div>
<div class="toggle_content_container pr_details1" style="display: block;">
<div class="info_content">
<ul>
<li>Sheer polyester fabric with soft lining</li>
<li>Cowl neck reverse and full cuffed sleeves with button closure</li>
<li>Tie back belt embellished with beads and stone accents</li>
<li>Concealed zipper to side for easy wear</li>
<li>Flared hem with small kick slit</li>
</ul>
</div>
</div>
</div>
<div class="toggle_content_container">
<div class="toggle_link_container pr_head2 ">
<a class="open" href="#" title="Details">INSTRUCTION</a>
</div>
<div class="toggle_content_container pr_details2" style="display: block;">
<div class="info_content">

<table class="product_details">
<tr>
<th>SKU</th><td>SASASASA001</td></tr><tr>
<th>Color</th><td>Black</td></tr><tr>
<th>Washing Instruction</th><td>Machine wash 30c. Do not bleach. Low iron. Do not tumble dry. Any dry cleaning solvent other than trichloroethylene may be safely used.</td>
<tr><th>Neck Type</th><td>v Neck</td></tr>
<tr>
<th>Model Height</th><td>100</td></tr>
</tr>
<tr>
<th>Material</th><td>CREP Nida</td></tr>
<tr>
<th>Model Measurement</th><td>100 Single</td></tr>
</table>
</div></div>
        </td></tr></table>
        </div>
       </div>
       
        </div>  
    </div>
     
</article>
    
    <script type="text/javascript" src="js/jquery.simpleGallery.js"></script>
    <script type="text/javascript" src="js/jquery.simpleLens.js"></script>
    <script>
        $(document).ready(function () {


            $(' .simpleLens-big-image').simpleLens({
                loading_image: '../img/loader.gif'
            });
        });
    </script>
    <script>
        $(document).ready(function () {

            $(' .simpleLens-thumbnails-container img').simpleGallery({
                loading_image: '../img/loader.gif',
                show_event: 'click'
            });

            $('.simpleLens-big-image').simpleLens({
                loading_image: '../img/loader.gif',
                open_lens_event: 'click'
            });
            $(".simpleLens-thumbnail-wrapper").click(function (e) {

                $(".simpleLens-lens-image").attr("data-lens-image", $(this).attr("data-lens-image"));
                $(".simpleLens-big-image").attr("src", $(this).attr("data-big-image"));
            });



        });

        function size_map_onblur() {
        }
    </script>
</asp:Content>
