﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using HanayenBussiness;

namespace HanayenWebsite.Shopping
{
    public partial class AjaxGallery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet=true)]
        public static string GetGallaryProducts(int category,int start,int end)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            hnProductHandler productHandler = new hnProductHandler();
            return jScript.Serialize( productHandler.GetGalleryProducts(category,start,end));
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetProductsDetails(long productId)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            hnProductHandler productHandler = new hnProductHandler();
            return jScript.Serialize(productHandler.GetProductDescription(productId));
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetProductsCountByCategory(int category)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            hnProductHandler productHandler = new hnProductHandler();
            return jScript.Serialize(productHandler.GetTotalProductsCount(category));
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetProductSizes(int productId)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            hnProductHandler productHandler = new hnProductHandler();
            return jScript.Serialize(productHandler.hnGetProductSize(productId));
        }
    }
}