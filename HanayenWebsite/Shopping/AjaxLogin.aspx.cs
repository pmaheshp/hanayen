﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using HanayenEntity;
using HanayenBussiness;
using HanayenWebsite.Account;

namespace HanayenWebsite.Shopping
{
    public partial class AjaxLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string CheckUserIsValid(string user)
        {
           
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            LoginChk log = new LoginChk();
            HanayenEntity.Login lstLogin = new HanayenEntity.Login();
            hnLoginHandler loginHandler = new hnLoginHandler();
            HanayenEntity.Login login = new HanayenEntity.Login();
            log = jScript.Deserialize<LoginChk>(user);
            lstLogin=loginHandler.CheckUser(log.email.Trim(), log.password.Trim());
            if (lstLogin!=null)
            {
                SessionHandler.Email = lstLogin.Email;
                SessionHandler.LoginId = lstLogin.LoginId;
                return jScript.Serialize(lstLogin);       
            }
            return "false";
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string CheckEmailValid(string email)
        {

            JavaScriptSerializer jScript = new JavaScriptSerializer();
             hnLoginHandler loginHandler = new hnLoginHandler();
             HanayenEntity.Login lstLogin = new HanayenEntity.Login();
            lstLogin=   loginHandler.CheckEmail(email);
           
            if (lstLogin != null)

            {
                return "false";
            }
            return "true";
        }
    }
    public class LoginChk
    {
        public string email { get; set; }
        public string password { get; set; }

    }
}