﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shopping/ShoppingMaster.Master" AutoEventWireup="true" CodeBehind="ValidateUser.aspx.cs" Inherits="HanayenWebsite.Shopping.ValidateUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
Your account has been 
successfully created, 
Please activate it now!
An activation email has been sent to you. 
Please follow the prompts to activate your customer account.
Confirming your email allows you to:
1.Manage your points and coupons
3.Get the biggest discounts
5. Manage your online profile
2.View billing and shipping information
4.Check your order status and order history
Resend Activation Email
Resent successfully. Please check your email box to activate your account.
After 60 seconds, you can resend it again.  
</asp:Content>
