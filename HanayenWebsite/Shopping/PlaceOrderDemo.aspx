﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shopping/ShoppingMaster.Master" AutoEventWireup="true" CodeBehind="PlaceOrderDemo.aspx.cs" Inherits="HanayenWebsite.Shopping.PlaceOrderDemo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/placeorder.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="inner_content">
<h4>Shipping Information</h4>
<div id="div_shippinginformation" class="inner_box_padd">
<ul>
<li>Name:<span class="b">RemyaOlavanna</span></li>
<li>Address Line1:<span class="b">OLAVANNA</span> </li>
<li>Address:<span class="b">KOZHIKODWE KERALA India</span></li>
<li>ZIP/Postal Code:<span class="b"> 673019</span></li>
<li>Phone:<span class="b"> 9633392671</span></li>
<li>E-mail address: <span class="b">remya@nexegen.net</span></li>
</ul>
</div>
<h4>Shipping Methods</h4>
<div id="div_shippingmethods">
<ul>
<li class="hori li_col1">
<input name="shipping" class="shipping_method" value="1" checked="true" id="shipping1" type="radio">
<label><span class="lb">FlatRateShipping</span></label>
</li>
<li class="hori li_col2"><span class="lb">10 - 20 business days</span> </li>
<li class="hori li_col3"><span class="lb">$0.00 </span></li>
</ul>
<ul>
<li>
<input name="Need_Traking_number" value="2" id="Need_Traking_number1"  class="Need_Traking_number" type="checkbox">
<label class="track">Tracking Number</label></li>
<span>10 - 20 business days </span>
<span>$0.00 </span>
</ul>
<ul><li class="hori li_col1">
<input name="shipping" class="shipping_method" value="1" checked="true" id="Radio1" type="radio">
<label ><span  class="lb">Standard Shipping</span></label></li>
<li class="hori li_col2"><span  class="lb">10 - 20 business days </span></li>
<li class="hori li_col3"><span  class="lb">$0.00</span> </li>
</ul>
<ul>
<li class="hori li_col1">
<input name="Need_Traking_number" value="2" id="Checkbox1"  class="Need_Traking_number" type="checkbox">
<label><span class="lb">Add Shipping Insurance To Your Order</span></label>

</li>
<li class="hori li_col2"></li>
<li class="hori li_col3"><span class="lb">$0.00 </span></li>
</ul>
<div style="margin-left: 4%;">
<div class="cart_time"></div>
<p class="red">What's the Total Delivery Time? (Please use this formula to determine when your order will arrive) </p>
<p class="fb" style="padding-left: 37px;">Total Delivery Time = Processing Time + Packaging Time + Shipping Time</p>
</div>
</div>
<h4>Payment Method</h4>
<div id="div_paymentmethod">
<ul>
<li class="hori"><input name="shipping" class="shipping_method" value="1" checked="true" id="Radio2" type="radio"></li>
<li class="hori"><div><img src="images/pic_2.jpg" /></div></li>
<li class="hori"><label>Easier Way TO pay</label></li>
</ul>
<ul>
<li class="hori"><input name="shipping" class="shipping_method" value="1" checked="true" id="Radio3" type="radio"></li>
<li class="hori"><div><img src="images/paypal.gif" /></div></li>
<li class="hori">test description</li>
</ul>
<ul>
<li class="hori"><input name="shipping" class="shipping_method" value="1" checked="true" id="Radio4" type="radio"></li>
<li class="hori"><div><img src="images/creditcard.gif" /></div></li>
<li class="hori">test description</li>
</ul>
</div>
<h4>Order Review</h4>
<div id="div_orderreview">
<div><span>If you have special instructions for your order such as dropshipping order, gift order etc, please let us know!</span></div>
<div><textarea rows="6" cols="150"></textarea></div>
</div>
</div>
</asp:Content>
