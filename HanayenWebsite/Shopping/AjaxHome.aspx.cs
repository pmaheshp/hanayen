﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Script.Serialization;
using HanayenBussiness;
using HanayenEntity;
using HanayenEntity.SupportingModel;

namespace HanayenWebsite.Shopping
{
    public partial class AjaxHome : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetNewArrivals()
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            hnProductHandler productHandler = new hnProductHandler();
            List<Products> productlist = new List<Products>();
            productlist = productHandler.hnGetNewArrivals();
            int count = productlist.Count;
            return jScript.Serialize(productlist);
           
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetHotsaleProduct()
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            hnProductHandler productHandler = new hnProductHandler();
            List<Products> productlist = new List<Products>();
            productlist = productHandler.GetHotsaleProduct();
            int count = productlist.Count;
            return jScript.Serialize(productlist);

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetBanners()
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            hnBannerHandler bannerHandler = new hnBannerHandler();
            Banner banner = new Banner();
            List<Banner> bannerlist = new List<Banner>();
            bannerlist = bannerHandler.GetBannerlist(banner.BannerType);
            int count = bannerlist.Count;
            return jScript.Serialize(bannerlist);

        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetOfferBanners()
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            hnBannerHandler bannerHandler = new hnBannerHandler();
            Banner banner = new Banner();
            List<Banner> bannerlist = new List<Banner>();
            bannerlist = bannerHandler.GetOfferBannerlist(banner.BannerType);
            int count = bannerlist.Count;
            return jScript.Serialize(bannerlist);

        }
    }
}