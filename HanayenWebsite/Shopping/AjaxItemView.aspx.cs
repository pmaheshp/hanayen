﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using HanayenBussiness;
using System.Web.Services;
using System.Web.Script.Services;
using HanayenEntity;
using HanayenEntity.SupportingModel;

namespace HanayenWebsite.Shopping
{
    public partial class AjaxItemView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetCartItems(string cookieid)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            hnCartItemHandler cartitemHandler = new hnCartItemHandler();
            List<CartItemList> cartlist = new List<CartItemList>();
            cartlist = cartitemHandler.GetCartItems(cookieid);
            int count = cartlist.Count;
            return jScript.Serialize(cartlist);
           // return jScript.Serialize(cartitemHandler.GetCartItems(cookieid));
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetProductDescriptionById(string productId)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            hnCartItemHandler cartHandler = new hnCartItemHandler();
            List<Products> cartlist = new List<Products>();
            CartItemList cart = new CartItemList();
           // long pdtId = Convert.ToInt32(productId);
            cart = cartHandler.GetProductDetailsByCartId(productId);
            int count = cartlist.Count;
            return jScript.Serialize(cart);
            // return jScript.Serialize(cartitemHandler.GetCartItems(cookieid));
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string DeleteCartItems(String cartItemId)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            hnCartItemHandler cartitemHandler = new hnCartItemHandler();
            CartItems item = new CartItems();
            item.CartId = Convert.ToInt32(cartItemId.Trim());
            return jScript.Serialize(cartitemHandler.hnDeleteCartItems(item));
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string UpdateCartItems(String cartItem)
        {
           JavaScriptSerializer jScript = new JavaScriptSerializer();
           hnCartItemHandler cartitemHandler = new hnCartItemHandler();
           CartItems item = new CartItems();
           CartUpdation cartUp = new CartUpdation();
           cartUp = jScript.Deserialize<CartUpdation>(cartItem);
           item.CartId = Convert.ToInt32(cartUp.cartId);
           item.Quantity = Convert.ToInt32(cartUp.qty);
           return jScript.Serialize(cartitemHandler.hnEditCartItems(item));
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string CheckProductId(string product)
        {
            
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            CheckProduct chkproduct= new CheckProduct();
            CartItems cartitem = new CartItems();
            hnCartItemHandler cartitemHandler = new hnCartItemHandler();
            chkproduct = jScript.Deserialize<CheckProduct>(product);
            long pdt = cartitemHandler.CheckProductId(Convert.ToInt64(chkproduct.productId), chkproduct.cookieId);
            return jScript.Serialize(pdt);
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string UpdateQty(String cartItem)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            hnCartItemHandler cartitemHandler = new hnCartItemHandler();
            CartItems item = new CartItems();
            UpdateQuantity qtyup = new UpdateQuantity();
            qtyup = jScript.Deserialize<UpdateQuantity>(cartItem);
            item.ProductId= Convert.ToInt32(qtyup.productId);
            item.CookieId =qtyup.cookieId;
            item.Quantity = Convert.ToInt32(qtyup.qty);
            return jScript.Serialize(cartitemHandler.hnEditQuantity(item));
        }

        public class CartUpdation
        {
            public string cartId { get; set; }
            public string qty { get; set; }
        }
        public class CheckProduct
        {
            public string productId { get; set; }
            public string cookieId { get; set; }
        }
        public class UpdateQuantity
        {
            public string productId { get; set; }
            public string cookieId { get; set; }
            public string qty { get; set; }
        }
    }
}