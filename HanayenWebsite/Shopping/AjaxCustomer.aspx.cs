﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using HanayenEntity;
using HanayenBussiness;
using HanayenWebsite.Account;
using System.Text;

namespace HanayenWebsite.Shopping
{
    public partial class AjaxCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        private static string GetActivationKeyGenrated()
        {
            String activationkey = SetActivationKey(12);
            return activationkey;
        }
        private static Random random = new Random((int)DateTime.Now.Ticks);

        private static string SetActivationKey(int Size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < Size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            builder.ToString();
            return builder.ToString();
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static void InsertCustomerDetails(string custData)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            CustomerIns cust = new CustomerIns();
            Customers customers = new Customers();
            string  ActivationKey= GetActivationKeyGenrated();
            cust = jScript.Deserialize<CustomerIns>(custData);
            customers.Email = cust.email;
            HanayenEntity.Login log = new HanayenEntity.Login();
            log.Password = cust.password;
            log.Email = cust.email;
            log.ActivationKey = ActivationKey;
            log.IsActivate = "false";
            log.Status = "0";
            hnLoginHandler LoginHandler = new hnLoginHandler();
            int success = LoginHandler.hnInsert(log);
            int logid = LoginHandler.hnGetMaxLoginId();
            customers.LoginId = logid;
            hnCustomerHandler CustomerHandler = new hnCustomerHandler();
            CustomerHandler.hnInsertCustomer(customers);
            SessionHandler.Email = log.Email;
            SessionHandler.Activationkey = log.ActivationKey;
            EmailService emailServ = new EmailService();
            string MailSubject = "Thank You for Confirming Your Account on Hanayen";
            //string MailBody ="<html><div><img src="../logo1.gif"><p>Welcome to Hanayen,you are one step away from confirming your account.Please click the following link to complete the process:</p></div></html>";
            string MailBody = "<html><body><h1>haii</h1></body></html>";
            string toaddress = SessionHandler.Email;
            string activationkey = SessionHandler.Activationkey;
            EmailService.SendEmail(toaddress, MailSubject, MailBody,activationkey);
        }



    }

    class CustomerIns
    {
        public string email{get;set;}
        public string password { get; set; }
    
    }
}