﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using HanayenEntity;
using HanayenBussiness;
using HanayenWebsite.Account;
using System.Text;

namespace HanayenWebsite.Shopping
{
    public partial class AJAXCartItem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string InsertIntoCart(string cartItem)
        {
            JavaScriptSerializer jScript = new JavaScriptSerializer();
            CartDetails cartItm = new CartDetails();
            cartItm = jScript.Deserialize<CartDetails>(cartItem);
            string cookieID = cartItm.cookieid;

            if (cartItm.cookieid == "undefined")
            {
                cookieID = SetCookie(12);
            }
            SaveToCart(Convert.ToInt64(cartItm.cartId), cookieID);
            return cookieID;
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetCookieGenrated(string msgVal)
        {
            String cookieID = SetCookie(12);
            JavaScriptSerializer jScript = new JavaScriptSerializer();            
            return jScript.Serialize(cookieID);
        }
        private static Random random = new Random((int)DateTime.Now.Ticks);
        private static string 
            SetCookie(int Size)
        { 
        StringBuilder builder = new StringBuilder();
        char ch;
        for (int i = 0; i < Size; i++)
        {
            ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
            builder.Append(ch);
        }
         builder.ToString();
         return builder.ToString();
        }
      
        private static void SaveToCart(long proid, string cookie)
        {
            try
            {
                hnCartItemHandler CartHandler = new hnCartItemHandler();
                HanayenEntity.CartItems cartEntity = new HanayenEntity.CartItems();
                cartEntity.Quantity = 1;
                cartEntity.CookieId = cookie;
                cartEntity.CartStatus = 0;
                if (SessionHandler.Email != "")
                {
                    cartEntity.IsGuest = "No";
                }
                else
                {
                    cartEntity.IsGuest = "Yes";
                }
                cartEntity.ProductId = proid;
                cartEntity.CartDate = DateTime.Now;
                CartHandler.hnAddCartItems(cartEntity);
            }
            catch (Exception ex)
            {

            }
        }
       
    }
    public class CartDetails
    {
        public string cartId;
        public string cookieid;
    }
}