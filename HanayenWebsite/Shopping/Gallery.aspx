﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Shopping/ShoppingMaster.Master" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="HanayenWebsite.Shopping.Gallery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/jscript" src="js/JSGallery.js"></script>
<link rel="Stylesheet" href="Style/Gallery.css" />
    <script src="js/jquery.twbsPagination.js" type="text/javascript"></script>
    <link href="Style/simplePagination.css" rel="stylesheet" type="text/css" />
    <link href="../Pages/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/jscript">
     $(document).ready(function ()
     {
       getCategory("gallery");
       GetCollection(1);
       
       GetProductCountByCategory(1);
   
        $('#pagination-demo').twbsPagination({
            totalPages: Math.ceil(total / inc),
            visiblePages: 3,
            onPageClick: function (event, page) {
                getCollectionPage(page);
                //goto galley.js
            }
        });

        $('#header')
	.css({ 'top': -50 })
	.delay(1000)
	.animate({ 'top': 0 }, 800);
    });


</script>
<input type='hidden' id='current_page' />
<input type='hidden' id='previous_page' />
	<input type='hidden' id='show_per_page' />
    <input type='hidden' id='hf_category' value="1" />
<div class="widthcontainer">
<div> <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" />

<asp:SiteMapPath ID="SiteMap1" runat="server"></asp:SiteMapPath></div>
<section id="catalog_container">
<div id="totaldiv" style="display:block"></div>
<div class="row">
    <div class="column one_fifth">
    <div id="catalog_category_option">
        <ul>
        
        </ul>
        </div>
    </div>
    <div class="column four_fifths">
    <div id="catalog_content">
    <div>
    <ul id="catalog_listings">
    
     
    </ul>
    </div>
     <div class="text-center">
   <%--  <table style="margin:0 auto"><tr><td>

     <a class="arrow" title="Previous" href="#" id="prev_page">Previous</a></td><td>
       </td><td>
            <a id="next_page" class="arrow" style="display:none" title="Next" href="#">Next</a></td>
            </tr></table>--%>
            
        <div id='page_navigation'> <ul id="pagination-demo" class="pagination-sm"></ul>
          
        </div></div>
    </div>
    </div>
   
 </div>
</section>

</div>

<input type="hidden" id="hdTotal" />


</asp:Content>
