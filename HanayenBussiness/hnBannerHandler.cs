﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HanayenEntity;
using System.Data.SqlClient;
using HanayenDataAccess;

namespace HanayenBussiness
{
 public   class hnBannerHandler
    {
     
        HnBanner hnBannerItem = null;
        public hnBannerHandler()
        {
            hnBannerItem = new HnBanner();
        }
        public string hnInsertBanner(Banner banner)
        {
            return hnBannerItem.AddBannerImages(banner);
        }
        public List<Banner>GetBannerlist(string BannerType)
        {
            return hnBannerItem.GetBanners(BannerType);
        }
        public List<Banner> GetOfferBannerlist(string BannerType)
        {
            return hnBannerItem.GetOfferBanners(BannerType);
        }
        public string hnDeleteBanner(Banner BannerItem)
        {
            return hnBannerItem.DeleteBannerImages(BannerItem);
        }
        public List<Banner> SelectBannerlist(string BannerId)
        {
            return hnBannerItem.SelectBanner(BannerId);
        }
        public string hnUpdateBanner(Banner banner)
        {
            return hnBannerItem.UpdateBannerImages(banner);
        }
        public List<Banner> hnGetBannerView()
        {
            return hnBannerItem.GetBannerList();
        }

    }
}
