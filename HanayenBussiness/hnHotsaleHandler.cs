﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HanayenDataAccess;
using HanayenEntity;
using HanayenEntity.SupportingModel;


namespace HanayenBussiness
{
    public class hnHotsaleHandler
    {
        hnHotsale hnhotsale  = null;
        public hnHotsaleHandler()
        {
            hnhotsale = new hnHotsale();
        }
        public string hnAddHotsale(HotSale hotsale)
        {
            return hnhotsale.AddHotsale(hotsale);
        }
        public List<HotSaleJoin> hnGetHotSaleJoin()
        {
            return hnhotsale.GetHotSaleisBy();
        }
        public string hnDeleteHotsale(HotSale hotsale)
        {
            return hnhotsale.DeleteHotsaleById(hotsale);
        }
    }
}
