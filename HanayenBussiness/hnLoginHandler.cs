﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HanayenEntity;
using HanayenDataAccess;

namespace HanayenBussiness
{
   public class hnLoginHandler
    {
        hnLogin hnLoginItem = null;
        public hnLoginHandler()
        {
            hnLoginItem = new hnLogin();
        }
        public int hnInsert(Login login)
        {
            return hnLoginItem.Insert(login);
        }
       //Check Email&Password
        public Login CheckUser(string email, string password)
        {
            return hnLoginItem.CheckUser(email, password);
        }
        public string hnEditPassword(Login login)
        {
            return hnLoginItem.EditPassword(login);
        }
        public string hnEditActivationKey(Login login)
        {
            return hnLoginItem.EditActivationKey(login);
        }
        //GetMaxLoginId
        public int hnGetMaxLoginId()
        {
            return hnLoginItem.GetMaxLoginId();
        }

        public Login CheckEmail(string email)
        {
            return hnLoginItem.CheckEmail(email);
        }
       //Activate User
        public string hnActivateUser(Login login)
        {
            return hnLoginItem.ActivateUser(login);
        }
    }
}
