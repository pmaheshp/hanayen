﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HanayenDataAccess;
using HanayenEntity;
using System.Transactions;
using HanayenEntity.SupportingModel;
namespace HanayenBussiness
{
    public class hnProductHandler
    {
        hnProducts hnProducts = null;
        public hnProductHandler()
        {
            hnProducts = new hnProducts();
        }
        public string hnAddCategory(Category category)
        {
           return hnProducts.AddCategory(category);
        }
        public List<Category> hnGetCategory()
        {
            return hnProducts.GetCategory();
        }
        
        // Check product code valid/not

        public Products CheckProductCode(string productcode)
        {
            return hnProducts.CheckProductCode(productcode);
        }

        public string hnAddSize(Size size)
        {
            return hnProducts.AddSize(size);
        }
        public List<Size> hnGetSize()
        {
            List<Size> sizeList = new List<Size>();
            sizeList = hnProducts.GetSize();
            int count=0;
            foreach (Size siz in sizeList)
            {
                sizeList[count].SizeDesc = hnProducts.GetSizeDesc((int)sizeList[count].SizID);
                count++;
                
            }
            return sizeList;
        }
        public string hnAddDesign(Design design)
        {
            return hnProducts.AddDesign(design);
        }
        public List<Design> hnGetDesign()
        {
            return hnProducts.GetDesign();
        }
        public ErrorCode hnAddProduct(Products product)
        {
            ErrorCode error = ErrorCode.FAILED; 
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    hnProducts.AddProducts(product);
                    foreach (xRefProductSize ps in product.ProductSize)
                    {
                        hnProducts.AddProductSize(ps);
                    }
                    if (product.ProductSize.Count > 0)
                    {
                        scope.Complete();
                        error = ErrorCode.SUCCESS;
                    }
                }
            }
            catch(Exception ex)
            {
                error = ErrorCode.FAILED;
                throw ex;
            }
            return error;
        }
        public ErrorCode hnAddImage(Images images)
        {
            return hnProducts.AddImage(images);
        }
        public string hnGetLastProductCode()
        {
            return hnProducts.GetLastProductCode();
        }
        public List<Products> GetGalleryProducts(int category,int start,int end)
        {
            return hnProducts.GetGalleryProducts(category,start,end);
        }
        public List<Products> GetProductDescription(long productId)
        {
            return hnProducts.GetProductDescription(productId);
        }
        //Get Newarrivals
        public List<Products> hnGetNewArrivals()
        {
            return hnProducts.GetNewArrivals();
        }

        //Get Hotsales

        public List<Products> GetHotsaleProduct()
        {
            return hnProducts.GetHotsaleProduct();
        }

        public int GetTotalProductsCount(int category)
        {
            return int.Parse(hnProducts.GetTotalProductsCount(category));
        }
        public List<xRefProductSize> hnGetProductSize(long productId)
        {
            return hnProducts.GetProductSize(productId);
        }

        //View products
        public List<ProductViewJoin> hnGetProductViewJoin()
        {
            return hnProducts.GetProductList();
        }

        //Delete products
        public string hnDeleteProduct(Products ProductItem)
        {
            return hnProducts.DeleteProduct(ProductItem);
        }
    }
}
