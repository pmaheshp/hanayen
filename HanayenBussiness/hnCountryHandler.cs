﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HanayenEntity;
using HanayenDataAccess;
using HanayenEntity.SupportingModel;

namespace HanayenBussiness
{
    public class hnCountryHandler
    {
        hnCountry hnCountryItem = null;
        public hnCountryHandler()
        {
            hnCountryItem = new hnCountry();
        }
       public string hnInsertCountry(Country country)
       {
           return hnCountryItem.Insert(country);
       }
    }
}
