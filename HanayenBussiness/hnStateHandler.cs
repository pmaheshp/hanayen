﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HanayenEntity;
using HanayenDataAccess;
using HanayenEntity.SupportingModel;

namespace HanayenBussiness
{
   public class hnStateHandler
    {
       hnState hnStateItem = null;
       public hnStateHandler()
        {
            hnStateItem = new hnState();
        }
       public string hnInsertState(State state)
       {
           return hnStateItem.Insert(state);
       }
       public List<Country> hnGetCountry()
       {
           return hnStateItem.GetCountry();
       }
    }
}
