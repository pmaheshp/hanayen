﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HanayenEntity;
using HanayenDataAccess;
using HanayenEntity.SupportingModel;

namespace HanayenBussiness
{
    public class hnCustomerHandler
    {
      
        hnCustomer hnCustomerItem = null;
        public hnCustomerHandler()
        {
            hnCustomerItem = new hnCustomer();
        }
        public string hnInsertCustomer(Customers customer)
        {
            return hnCustomerItem.Insert(customer);
        }
        public List<CustomerViewJoin> hnGetCustomerView()
        {
            return hnCustomerItem.GetCustomerList();
        }
        public string hnDeleteCustomer(Customers customer)
        {
            return hnCustomerItem.DeleteCustomerById(customer);
        }
        public string hnUpdateCustomer(Customers customer)
        {
            return hnCustomerItem.UpdateCustomerDetails(customer);
        }
    }
}
