﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HanayenEntity;
using HanayenDataAccess;
using HanayenEntity.SupportingModel;

namespace HanayenBussiness
{
   public class hnCartItemHandler
    {

       hnCart hnCartItem = null;
       public hnCartItemHandler()
       {
           hnCartItem = new hnCart();
       }

       public string hnAddCartItems(CartItems cartItem)
       {
           return hnCartItem.AddCartItems(cartItem);
       }

       public long CheckProductId(long ProductId,string CookieId)
       {
           return hnCartItem.CheckProductId(ProductId,CookieId);
       }
       public List<CartItems> GetCookie(string CookieId)
       {
           return hnCartItem.GetCookie(CookieId);
       }
       public string hnDeleteCartItems(CartItems cartItem)
       {
           return hnCartItem.DeleteCartItems(cartItem);
       }
       public string hnEditCartItems(CartItems cartItem)
       {
           return hnCartItem.EditCartItems(cartItem);
       }
       public string hnEditQuantity(CartItems cartItem)
       {
           return hnCartItem.UpdateQuantity(cartItem);
       }
       public List<CartItemList>GetCartItems(string CookieId)
       {
           return hnCartItem.GetCartItems(CookieId);
       }
       public CartItemList GetProductDetailsByCartId(string cartId)
       {
           return hnCartItem.GetProductDetailsByCartId(cartId);
       }
    }
}
